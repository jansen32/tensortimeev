#include"itensor/all.h"
#include "unittest/test.h"
#include<vector>
#include"../src/holstein.hpp"
#include"../src/owntime.hpp"
#include"../src/tebd.hpp"
#include"../src/vidalnot.hpp"
#include"../src/timeevclass.hpp"
using namespace itensor;
using namespace std;
const double err=1E-5;
TEST_CASE("vidal notation")
{
  
    int  N=4;
    auto tstep = 0.0001;
  auto cutoff=1E-13;
  using Gate = BondGate<IQTensor>;
auto sites = SpinHalf(N);
 auto ampo = AutoMPO(sites);
auto ttotal = 3.0;
 
for(int j = 1; j < N; ++j)
    {
    ampo += 0.5,"S+",j,"S-",j+1;
    ampo += 0.5,"S-",j,"S+",j+1;
    ampo +=     "Sz",j,"Sz",j+1;
    }
 for(int j = 1; j < N; ++j)
    {
      ampo +=     0.5,"Sz",j,"Sz",j+1;
    }
 
 auto state = InitState(sites);
for(auto j : range1(N))
    {
    state.set(j,j%2==1?"Up":"Dn");
    }

auto psi = IQMPS(state);
auto H = IQMPO(ampo);
  psi.position(1);
    // Print(psi.A(1));
     psi.normalize();
auto gates = vector<Gate>();

for(int b = 1; b <= N-1; ++b)
    {
    auto hterm = sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += 0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += 0.5*sites.op("S-",b)*sites.op("S+",b+1);

    auto g = Gate(sites,b,b+1,Gate::tReal,tstep/2.,hterm);
    gates.push_back(g);
    }
//Create the gates exp(-i*tstep/2*hterm) in reverse
//order (to get a second order Trotter breakup which
//does a time step of "tstep") and add them to gates
for(int b = N-1; b >= 1; --b)
    {
    auto hterm = sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += 0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += 0.5*sites.op("S-",b)*sites.op("S+",b+1);

    auto g = Gate(sites,b,b+1,Gate::tReal,tstep/2.,hterm);
    gates.push_back(g);
    }
  auto ampo2 = AutoMPO(sites);
  ampo2 +=     1,"Sz",2;
  auto O = IQMPO(ampo2);

     auto O2=IQMPO(ampo);
     auto psi0=psi;
    auto psir = psi;
    auto psiv=vMPS<IQTensor>(psi);
    // print(psiv);
    
    //Print(psiv.psi);
	   print(psiv);
     double avl2 = overlap(psi, O, psi);
     std::cout<< 0<<"  "<<maxM(psi)<< "  "<<avl2 <<std::endl;
      auto args = Args("Cutoff=",cutoff,"Maxm=",5000, "Normalize",true);
      int i=0;
        TimeEvolve<IQTensor, vMPS<IQTensor>> TE(gates, psiv, args);
	 TE.TEBDStep();
//   avl2 = overlap(psi, O, psi);
//  auto l=psi;
//   l.position(1);
	std::cout<< "round 2 "<<std::endl;
//   std::cout<< i*tstep<< "  " << norm(l) <<"  "<<maxM(psi)<< "  "<<avl2 <<std::endl;
	   print(psiv);
TE.TEBDStep();
// 	  avl2 = overlap(psi, O, psi);
// 	    std::cout<< i*tstep<< "  " << norm(l) <<"  "<<maxM(psi)<< "  "<<avl2 <<std::endl;
    while(i<=10)
      {
	TE.TEBDStep();
         double avl2 = overlap(psi, O, psi);

// //    // 	std::cout<< avl2 << "  "
  auto l=psi;
  l.position(1);
  std::cout<< i*tstep<< "  " << norm(l) <<"  "<<maxM(psi)<< "  "<<avl2 <<std::endl;
    	i+=1;
      }

//     Print(psiv.psi);
//	   print(psiv);
  
  }
