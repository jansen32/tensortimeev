LIBRARY_DIR=$(ITENSOR)
include $(LIBRARY_DIR)/this_dir.mk
include $(LIBRARY_DIR)/options.mk
INC+=-I${ITENSOR}
INC+=-I${TENSOR}/TDVP/
INC+=-I${TENSOR}/parallel_tdvp_lbo
INC+=-I${TENSORTOOLS}/include
INC+=-I${MANYBODY}/src
INC+=-I$(PWD)/src
INC+=-I${EIGEN}
INC+=-I${EINC}

LIBSLINK+=-L${MANYBODY}/libs
LIBSLINK+=-L${ELIBS}
LIBSPATH=-L$(ITENSOR)/lib
LIBSPATH+=$(LIBSLINK)
LIBS=-litensor -lboost_program_options
LIBSDB=-litensor-g -lboost_program_options

CCFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(OPTIMIZATIONS) -Wno-unused-variable -std=c++17 -O2 -std=gnu++1z
CCGFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(DEBUGFLAGS) 

LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -lboost_program_options -lboost_filesystem 
LIBGFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBGFLAGS) -lboost_program_options -lboost_filesystem 
#LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -Wl,--start-group /home/jansen32/usr/lib2/lib/libboost_program_options.a /home/jansen32/usr/lib2/lib/libboost_filesystem.a -Wl,--end-group 

CPPFLAGS_EXTRA += -O2 -std=c++17
#MPILINK= -lboost_serialization -lboost_mpi  -I$MPI_INCLUDE -L$MPI_LIB 
MPICOM=mpicxx -m64 -std=c++17 -fconcepts -fPIC
DB=-g
CXX=g++
ND=-DNDEBUG

#-fopenmp -DOMPPAR
#-DTBBPAR
#
BINS=main  timecheckspinnorm timecheckspintebd timecheckholnorm timecheckholtebd timecheckholtebdlbo
main: main.cpp 
	g++ main.cpp  -o main -Wall $(INC) $(CPPFLAGS_EXTRA) $(LIBSPATH) $(LIBFLAGS)
# mpic++
# time evolution tests
timecheckholtebdpara: sample/timecheckholtebdpara.cpp 
	$(CXX) -std=c++17 sample/timecheckholtebdpara.cpp  -o timecheckholtebdpara -Wall  $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) $(CCFLAGS) 

timecheckholtdmrg: sample/timecheckholtdmrg.cpp 
	$(CXX) -std=c++17 sample/timecheckholtdmrg.cpp  -o timecheckholtdmrg -Wall $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)
timecheckholtdmrglbo: sample/timecheckholtdmrglbo.cpp  src/lboclass.hpp src/lbo.hpp
	$(CXX) -std=c++17 sample/timecheckholtdmrglbo.cpp  -o timecheckholtdmrglbo -Wall -Wall $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)

timecheckspintebd: sample/timecheckspintebd.cpp 
	$(CXX) -std=c++17 sample/timecheckspintebd.cpp  -o timecheckspintebd -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) 
timecheckspintebdpara: sample/timecheckspintebdpara.cpp 
	$(CXX) -std=c++17 sample/timecheckspintebdpara.cpp  -o timecheckspintebdpara $(OMPF) -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) 
timecheckholtebd: sample/timecheckholtebd.cpp 
	$(CXX) -std=c++17 sample/timecheckholtebd.cpp  -o timecheckholtebd -Wall -DDEBUG $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)
timecheckholtdvp: sample/timecheckholtdvp.cpp src/tdvp.hpp
	$(CXX) -std=c++17 sample/timecheckholtdvp.cpp  -o timecheckholtdvp -Wall  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND) 


timecheckholtdvp_lbo2: sample/timecheckholtdvp_lbo2.cpp src/tdvp_lbo2.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) 


timecheckholtdvp_lbo: sample/timecheckholtdvp_lbo.cpp src/tdvp_lbo.hpp
	$(CXX) -std=c++17 $< -o $@ -Wall  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)


timecheckholtdvpmix: sample/timecheckholtdvpmix.cpp src/tdvp.hpp
	$(CXX) -std=c++17 sample/timecheckholtdvpmix.cpp  -o $@ -Wall -DDEBUG $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND) 

timecheckholtdvp_bath: sample/timecheckholtdvp_bath.cpp src/tdvp.hpp
	$(CXX) -std=c++17 sample/timecheckholtdvp_bath.cpp  -o timecheckholtdvp_bath -Wall -DDEBUG $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)

timecheckholtdvp_bathbasisexp: sample/timecheckholtdvp_bathbasisexp.cpp src/tdvp.hpp
	$(CXX) -std=c++17 sample/timecheckholtdvp_bathbasisexp.cpp  -o timecheckholtdvp_bathbasisexp -Wall -DDEBUG $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND) 
timecheckholtdvpss: sample/timecheckholtdvpss.cpp src/tdvp.hpp
	$(CXX) -std=c++17 sample/timecheckholtdvpss.cpp  -o timecheckholtdvpss -Wall -DDEBUG $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND) 



timecheckholtdvp_lbomix: sample/timecheckholtdvp_lbomix.cpp src/tdvp_lbo.hpp
	$(CXX) -std=c++17 $< -o $@ -Wall  $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)


timecheckholtdvp_lboss: sample/timecheckholtdvp_lboss.cpp src/tdvp_lbo.hpp
	$(CXX) -std=c++17 $< -o $@ -Wall  $(CCGFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)

timecheckholexapp: sample/timecheckholexapp.cpp 
	$(CXX) -std=c++17 sample/timecheckholexapp.cpp  -o timecheckholexapp -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) 


timecheckholtebdlbo: sample/timecheckholtebdlbo.cpp 
	$(CXX) -std=c++17 sample/timecheckholtebdlbo.cpp  -o timecheckholtebdlbo -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

timecheckspintdmrg: sample/timecheckspintdmrg.cpp src/holstein.hpp
	$(CXX) -std=c++17 sample/timecheckspintdmrg.cpp  -o timecheckspintdmrg -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)

timecheckholtebdlbopara: sample/timecheckholtebdlbopara.cpp 
	$(CXX) -std=c++17 sample/timecheckholtebdlbopara.cpp  -o timecheckholtebdlbopara $(OMPF) -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)
##################################################
###### htstruct ###############################
### struct no leads but V ##
timeholtebdlbowV: sample/timeholtebdlbowV.cpp
	$(CXX) -std=c++17 $<  -o $@  -Wall  $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) $(CCFLAGS) 

timeholtdmrglbowV: sample/timeholtdmrglbowV.cpp
	$(CXX) -std=c++17 $<  -o $@  -Wall  $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) $(CCFLAGS) 

###############
htstruct: sample/htstruct.cpp 
	$(CXX)  sample/htstruct.cpp  -o htstruct -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
htstructpara: sample/htstructpara.cpp 
	$(CXX)  sample/htstructpara.cpp  -o htstructpara -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
htstructparalbo: sample/htstructparalbo.cpp 
	$(CXX)  sample/htstructparalbo.cpp  -o htstructparalbo -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
htstructparalborev: sample/htstructparalborev.cpp 
	$(CXX)  sample/htstructparalborev.cpp  -o htstructparalborev -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
holdotGS: sample/holdotGS.cpp 
	$(CXX)  sample/holdotGS.cpp  -o holdotGS -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)


holdotGSV: sample/holdotGSV.cpp 
	$(CXX)  sample/holdotGSV.cpp  -o holdotGSV -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holCDWstructGS: sample/holCDWstructGS.cpp 
	$(CXX)  $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holdotGSrfilled: sample/holdotGSrfilled.cpp 
	$(CXX)  $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holdotGS_bath: sample/holdotGS_bath.cpp 
	$(CXX)  sample/holdotGS_bath.cpp  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
holdotparalbo: sample/holdotparalbo.cpp 
	$(CXX)  sample/holdotparalbo.cpp  -o holdotparalbo -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holdottdmrglbo: sample/holdottdmrglbo.cpp 
	$(CXX)  $< -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
holcdwparalbo: sample/holcdwparalbo.cpp 
	$(CXX)  $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
holdot_bathtdvp: sample/holdot_bathtdvp.cpp 
	$(CXX)  sample/holdot_bathtdvp.cpp  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 

holdottdvp: sample/holdottdvp.cpp 
	$(CXX)  sample/holdottdvp.cpp  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
intparalbo: sample/intparalbo.cpp 
	$(CXX)  sample/intparalbo.cpp  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
intholparalbo: sample/intholparalbo.cpp 
	$(CXX)  sample/intholparalbo.cpp  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
intholtdvplbo: sample/intholtdvplbo.cpp 
	$(CXX)  sample/intholtdvplbo.cpp  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 

int_bathtdvp: sample/int_bathtdvp.cpp 
	$(CXX)  $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
intparalborev: sample/intparalborev.cpp 
	$(CXX)  sample/intparalborev.cpp  -o intparalborev -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
intparalboCon: sample/intparalboCon.cpp 
	$(CXX)  sample/intparalboCon.cpp  -o intparalboCon -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
intGS: sample/intGS.cpp 
	$(CXX)  sample/intGS.cpp  -o intGS -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holintGS: sample/holintGS.cpp 
	$(CXX)  $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

intGS_bath: sample/intGS_bath.cpp 
	$(CXX)  $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

intpara: sample/intpara.cpp 
	$(CXX)  sample/intpara.cpp  -o intpara -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) 
int: sample/int.cpp 
	$(CXX)  sample/int.cpp  -o int -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)  
intlbo: sample/intlbo.cpp 
	$(CXX)  sample/intlbo.cpp  -o intlbo -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND) $(OMPF) 
htstructFer: sample/htstructFer.cpp 
	$(CXX)  sample/htstructFer.cpp  -o htstructFer -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)
htstructFerpara: sample/htstructFerpara.cpp 
	$(CXX)  sample/htstructFerpara.cpp  -o htstructFerpara -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND)  $(OMPF)
################################################################################ greens functions ###########################################
GFtdmrglboCCDAG: sample/spfunctdmrglboCCDAG.cpp
	$(CXX) -std=c++17 sample/spfunctdmrglboCCDAG.cpp  -o spfunctdmrglboCCDAG -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBGFLAGS) $(OMPF) $(ND)
spfunctdmrglboCDAGC: sample/spfunctdmrglboCDAGC.cpp
	$(CXX) -std=c++17 sample/spfunctdmrglboCDAGC.cpp  -o spfunctdmrglboCDAGC -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboAUXCCDAG: sample/GFtdmrglboAUXCCDAG.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAG.cpp  -o GFtdmrglboAUXCCDAG -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)  $(ND)

GFtdmrglboAUXCCDAGnoNorm: sample/GFtdmrglboAUXCCDAGnoNorm.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGnoNorm.cpp  -o GFtdmrglboAUXCCDAGnoNorm -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)  $(ND)
GFtdmrglboAUXCCDAGnoNormKar: sample/GFtdmrglboAUXCCDAGnoNormKar.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGnoNormKar.cpp  -o GFtdmrglboAUXCCDAGnoNormKar -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND) 

GFtdmrglboAUXXXDAGnoNormKar: sample/GFtdmrglboAUXXXDAGnoNormKar.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXXXDAGnoNormKar.cpp  -o GFtdmrglboAUXXXDAGnoNormKar -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND) 
GFtdmrglboAUXCDAGCnoNormKar: sample/GFtdmrglboAUXCDAGCnoNormKar.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCDAGCnoNormKar.cpp  -o GFtdmrglboAUXCDAGCnoNormKar -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND)
GFtdmrglboAUXCDAGCTRIVnoNormKar: sample/GFtdmrglboAUXCDAGCTRIVnoNormKar.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCDAGCTRIVnoNormKar.cpp  -o GFtdmrglboAUXCDAGCTRIVnoNormKar -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND) 
GFtdmrglboAUXCCDAGtnoNormKar: sample/GFtdmrglboAUXCCDAGtnoNormKar.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGtnoNormKar.cpp  -o GFtdmrglboAUXCCDAGtnoNormKar -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND) 
GFtdmrglboAUXCCDAGtnoNormKarBra: sample/GFtdmrglboAUXCCDAGtnoNormKarBra.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGtnoNormKarBra.cpp  -o GFtdmrglboAUXCCDAGtnoNormKarBra -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND) 
GFtdmrglboAUXCCDAGtnoNormKarKet: sample/GFtdmrglboAUXCCDAGtnoNormKarKet.cpp 
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGtnoNormKarKet.cpp  -o GFtdmrglboAUXCCDAGtnoNormKarKet -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)   $(ND) 
GFtdmrglboAUXCDAGCbacknoNorm: sample/GFtdmrglboAUXCDAGCbacknoNorm.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCDAGCbacknoNorm.cpp  -o GFtdmrglboAUXCDAGCbacknoNorm -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)  $(ND)
GFtdmrglboAUXCCDAGt: sample/GFtdmrglboAUXCCDAGt.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGt.cpp  -o GFtdmrglboAUXCCDAGt -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)  $(ND)

GFtdmrglboAUXCCDAGNormt: sample/GFtdmrglboAUXCCDAGNormt.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGNormt.cpp  -o GFtdmrglboAUXCCDAGNormt -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)  $(ND)
GFtdmrglboAUXCCDAGnoNormt: sample/GFtdmrglboAUXCCDAGnoNormt.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCCDAGnoNormt.cpp  -o GFtdmrglboAUXCCDAGnoNormt -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)  $(ND)
GFtdmrglboAUXCCDAGpara: sample/GFtdmrglboAUXCCDAGpara.cpp src/GF.hpp
	mpic++ -std=c++17 sample/GFtdmrglboAUXCCDAGpara.cpp  -o GFtdmrglboAUXCCDAGpara -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(ND) $(MPILINK)


GFtdmrglboAUXCDAGC: sample/GFtdmrglboAUXCDAGC.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCDAGC.cpp  -o GFtdmrglboAUXCDAGC -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboAUXCDAGCNorm: sample/GFtdmrglboAUXCDAGCNorm.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCDAGCNorm.cpp  -o GFtdmrglboAUXCDAGCNorm -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboAUXCDAGCnoNorm: sample/GFtdmrglboAUXCDAGCnoNorm.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXCDAGCnoNorm.cpp  -o GFtdmrglboAUXCDAGCnoNorm -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)

GFtdmrglboAUXXDAGX: sample/GFtdmrglboAUXXDAGX.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXXDAGX.cpp  -o GFtdmrglboAUXXDAGX -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboAUXXDAGXt: sample/GFtdmrglboAUXXDAGXt.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXXDAGXt.cpp  -o GFtdmrglboAUXXDAGXt -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboAUXXDAGXnoNormt: sample/GFtdmrglboAUXXDAGXnoNormt.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXXDAGXnoNormt.cpp  -o GFtdmrglboAUXXDAGXnoNormt -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboAUXXXDAG: sample/GFtdmrglboAUXXXDAG.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXXXDAG.cpp  -o GFtdmrglboAUXXXDAG -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)

GFtdmrglboAUXXXDAGnoNorm: sample/GFtdmrglboAUXXXDAGnoNorm.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboAUXXXDAGnoNorm.cpp  -o GFtdmrglboAUXXXDAGnoNorm -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboXXDAGnoNormGS: sample/GFtdmrglboXXDAGnoNormGS.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboXXDAGnoNormGS.cpp  -o GFtdmrglboXXDAGnoNormGS -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboCDAGCnoNormGS: sample/GFtdmrglboCDAGCnoNormGS.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboCDAGCnoNormGS.cpp  -o GFtdmrglboCDAGCnoNormGS -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
GFtdmrglboCCDAGtnoNormGS: sample/GFtdmrglboCCDAGtnoNormGS.cpp src/GF.hpp
	$(CXX) -std=c++17 sample/GFtdmrglboCCDAGtnoNormGS.cpp  -o GFtdmrglboCCDAGtnoNormGS -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
###################### finite T JJ ###################
GFtdmrglboAuxJJnoNorm: sample/GFtdmrglboAuxJJnoNorm.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
GFtdvplboJJnoNorm: sample/GFtdvplboJJnoNorm.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
GFtdvplboJJnoNormMix: sample/GFtdvplboJJnoNormMix.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

GFtdvplboJJnoNormAux: sample/GFtdvplboJJnoNormAux.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

GFtdvplboEJJnoNormAux: sample/GFtdvplboEJJnoNormAux.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
FT_test: sample/FT_test.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $^  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)



## GS JJ ###############################
GS_test: sample/GS_test.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $^  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
GFtdvplboJJnoNormGS: sample/GFtdvplboJJnoNormGS.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

GFtdvplboEJJnoNormGS: sample/GFtdvplboEJJnoNormGS.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

GFtdvpJJnoNormGS: sample/GFtdvpJJnoNormGS.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
GFtdmrgJJnoNormGS: sample/GFtdmrgJJnoNormGS.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
GFtdmrglboJJnoNormGS: sample/GFtdmrglboJJnoNormGS.cpp src/GFopt.hpp
	$(CXX) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

spfree2: sample/spfree2.cpp
	$(CXX) -std=c++17 sample/spfree2.cpp  -o spfree2 -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF) $(ND)
spfreeferm: sample/spfreeferm.cpp
	$(CXX) -std=c++17 sample/spfreeferm.cpp  -o spfreeferm -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMP



##########################################################
### probe puls#############
JJprobetdvplboGS: sample/JJprobetdvplboGS.cpp
	$(CXX) -std=c++17 $^  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
###############################
# parallell programs
JJpara: sample/JJpara.cpp 
	$(MPICOM) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJpara_timeCont: sample/JJpara_timeCont.cpp 
	$(MPICOM) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJFTpara_timeCont: sample/JJFTpara_timeCont.cpp 
	$(MPICOM) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

JJFTpara: sample/JJFTpara.cpp 
	$(MPICOM) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)


CCDAGFTpara: sample/CCDAGFTpara.cpp 
	$(MPICOM) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
CDAGCFTpara: sample/CDAGCFTpara.cpp 
	$(MPICOM) -std=c++17 $<  -o $@ -Wall $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
######################


getlbosvals: sample/getlbosvals.cpp src/holstein.hpp
	$(CXX) -std=c++17 sample/getlbosvals.cpp  -o getlbosvals -Wall $(CPPFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS) $(OMPF)
clean:
	rm $(BINS) 

