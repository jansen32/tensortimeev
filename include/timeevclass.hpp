

#ifndef TIMEEVCLASS_H
#define TIMEEVCLASS_H
#include"itensor/all.h"
#include"vidalnot.hpp"
#include"para_opt.hpp"

namespace itensor{
 
 
  template<typename MPSType, bool FT>
  class TimeEvolve{

   
  public:
         using Gate = BondGate;
    TimeEvolve(  std::vector<Gate>& gatelist, MPSType& state, size_t L, Args const& args = Args::global()) : gatelist(gatelist),  psi(state), N(length(psi)), L(L),  args(args){};
    void tDmrgStep(); // works with all, depends on gatelist
    void TEBDStepO2();
    void TEBDStepO2para(); // only works with 2 ord tebd
    void paraNormalize();
    void Act(const Gate& gate);
    void Insert(int mn, int mx, ITensor& AA);
    void InsertFT(int mn, int mx, ITensor& AA);
    void InsertTime(int mn, int mx, ITensor& AA);

    bool PARA{0};
    std::vector<Gate>& gatelist;
    MPSType& psi;
    // lenght of MPS
    size_t N;
    // physical lenght of system
    int L;
    const bool use_lbo=false;
   const Args& args;
  };
  template<typename MPSType,bool FT>
  void  TimeEvolve< MPSType, FT>::paraNormalize()
  {
     psi.position(N);
    
     if constexpr(is_vidalNot<MPSType>::value)
		   {
 psi.psi.normalize();
     psi.makevMPS();
		   }

}
  template<typename MPSType,bool FT>
  void  TimeEvolve< MPSType, FT>::tDmrgStep()
  {
   
    psi.position(gatelist.front().i1());

    const bool do_normalize = args.getBool("Normalize",true);
    Real tot_norm = norm(psi);
        auto g = gatelist.begin();
        while(g != gatelist.end())
            {
            auto i1 = g->i1();
            auto i2 = g->i2();

            auto AA = psi.A(i1)*psi.A(i2)*g->gate();
	    AA.replaceTags("Site,1","Site,0");

            ++g;
            if(g != gatelist.end())
                {
                //Look ahead to next gate position
		 
                auto ni1 = g->i1();
                auto ni2 = g->i2();
		
                //SVD AA to restore MPS form
                //before applying current gate
                if(ni1 >= i2)
                    {

                    psi.svdBond(i1,AA,Fromleft,args);
                    psi.position(ni1); //does no work if position already ni1
                    }
                else
                    {
                    psi.svdBond(i1,AA,Fromright,args);
                    psi.position(ni2); //does no work if position already ni2
                    }
                }
            else
                {
                //No next gate to analyze, just restore MPS formb
                psi.svdBond(i1,AA,Fromright,args);
                }
            }

        if(do_normalize)
            {
            tot_norm *= psi.normalize();
            }

    return ;
  };
 
  template<typename MPSType,bool FT>
  void TimeEvolve< MPSType, FT>::TEBDStepO2()
  {

     auto g = gatelist.begin();
        while(g != gatelist.end())
            {
 	   Act(*g);

  	    g++;

	       
  	    }


    return;
  };
   template<typename MPSType, bool FT>
   void TimeEvolve< MPSType, FT>::Insert(int mn, int mx, ITensor& AA)
     {
       if constexpr(FT)
		     {
		       InsertFT(mn, mx, AA);
		     }
       else{
	 InsertTime(mn, mx, AA);
       }

	
     }
  template<typename MPSType, bool FT>
  void TimeEvolve< MPSType, FT>::InsertTime(int mn, int mx, ITensor& AA)
     {
const bool do_normalize = args.getBool("Normalize",true);

	 if(mx==L)
                    {
		 
 			  ITensor D;
 		       svd(AA,psi.Aref(mn),D,psi.Aref(mx) ,args);
		       auto Nom=1./itensor::norm(D);
		       auto newT=dag(psi.A(mn))*AA;
 	    	          if(do_normalize )
            {

	        if(!PARA)
		 {
		   D *= Nom;
		       newT*=Nom;
		 } 
			  	    }
			  
			  //std::cout<< "NORMXXX "<< itensor::norm(D)<<std::endl;
 			  psi.Ls[mn]=D;
			     
 		      psi.setA(mx, newT); }
 		      else{
     
 
  		      	ITensor D;
  		        svd(AA*psi.Ls[mx],psi.Aref(mn),D,psi.Aref(mx), args);
			auto Nom=1./itensor::norm(D);
			auto newT=dag(psi.A(mn))*AA;
			if(do_normalize )
             {
	       //std::cout<< "NORM222 "<< itensor::norm(D)<<std::endl;
	        if(!PARA)
		 {
		   D *= Nom;
	       newT*=Nom;
		 }
			    }
			//	std::cout<< "NORMYY "<< itensor::norm(D)<<std::endl;
		       psi.Ls[mn]=std::move(D);
    psi.setA(mx, newT);
    
 		    }

	
     }
   template<typename MPSType, bool FT>
   void TimeEvolve< MPSType, FT>::InsertFT(int mn, int mx, ITensor& AA)
     {
const bool do_normalize = args.getBool("Normalize",true);
  		      	ITensor D;
  		        svd(AA*psi.Ls[mx],psi.Aref(mn),D,psi.Aref(mx), args);
 	     	       if(do_normalize)
             {
			D *= 1./itensor::norm(D);
			    }
		       psi.Ls[mn]=std::move(D);
    psi.setA(mx, dag(psi.A(mn))*AA);
     if(mx==N-1)
			 {

			   AA=psi.A(mx)*psi.A(mx+1);
			   svd(AA,psi.Aref(mx),D,psi.Aref(mx+1), args);
			   if(do_normalize)
				{
			   D *= 1./itensor::norm(D);
				}
			   psi.Ls[mx]=std::move(D);
			   psi.setA(mx+1, dag(psi.A(mn+1))*AA);
			 }
    
 

	
     }
  template<typename MPSType, bool FT>
  void TimeEvolve< MPSType, FT>::Act(const Gate& gate)
     {


  	 auto i1 = gate.i1();
	 auto i2 = gate.i2();
	 int mx=std::max(i1, i2);
	 int mn=std::min(i1, i2);
	  auto phibar = psi.A(i1)*psi.A(i2);

	

	 phibar=phibar*gate;
	 phibar.replaceTags("1, Site", "0, Site");
	 Insert(mn, mx, phibar);

	
     }
  template<typename MPSType, bool FT>
  void TimeEvolve< MPSType, FT>::TEBDStepO2para()
  {
    // functioniert noch nicht für FT
    PARA=true;
const bool do_normalize = args.getBool("Normalize",true);
    size_t mod=(L+1)%2;
    size_t one=gatelist.size()/(3);
        para apply([this](size_t i){Act(gatelist[i]);});
    apply.act_para(one, 2*one+mod, gatelist.size());
 
     if(do_normalize)
       {
	 paraNormalize();
       }
    return ;
  };

}
#endif /* TIMEEVCLASS_H */
