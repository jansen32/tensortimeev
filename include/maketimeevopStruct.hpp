#ifndef MAKETIMEEVOPSTRUCT_H
#define MAKETIMEEVOPSTRUCT_H
#include"holstein.hpp"
#include<algorithm>
//#include"htham.hpp"
#include"itensor/mps/sites/electron.h"
#include<map>
namespace itensor{

 
   template<typename Lattice, typename Iterator>
 struct holstDotGates{
   using Latticet=Lattice;
     holstDotGates(Lattice& sites, Iterator& gates, double tstep, std::map<std::string, double> param, int Llead1, int Llead2, int Lchain):sites(sites),  N(length(sites)), Llead1(Llead1), Llead2(Llead2), Lchain(Lchain),   t0(param["t0"]), tl(param["tl"]), tint(param["tint"]), V(param["V"]),omega(param["omega"]), gamma(param["gamma"]), eps0(param["eps0"]) ,tstep(tstep), gates(gates) {makeGates();};
   
   void makeGates();
//             void set_current_time(const double& time)
//        {
// 	 current_time=time;
// }
   void makeGates(std::vector<int>);
 using Gate = BondGate;
     Gate oneGate(int, double);
     Lattice& sites;
  
     int N;
     
     int Llead1;
     int Llead2;
     int Lchain;
     double t0{};
     double tl{};
     double tint{};
    
     double V{};
     double omega{};
     double gamma{};
     double eps0{};
      double tstep{};
     Iterator& gates;
     // double current_time{0};
  
 };
   
template<typename Lattice, typename Iterator>
void holstDotGates< Lattice, Iterator>::makeGates()
{

    gates.clear();



   for( int b = 2; b <= N-1; b+=2)
           {
	     
	     gates.push_back(oneGate(b,tstep/2));
    }
     Iterator even=gates;
     for(int b = 1; b <= N-1; b+=2)
    {
 gates.push_back(oneGate(b,tstep));
    }
gates.insert(gates.end(), even.begin(), even.end());
 
}
 template<typename Lattice, typename Iterator>
auto holstDotGates< Lattice, Iterator>::oneGate(int b, double dt)
  ->holstDotGates< Lattice, Iterator>::Gate
{

 int L_x=Llead1+(Lchain+1)/2;
 double E=0;
 if(Lchain>1)
   {
E=V/(Lchain+1);  
   }
  if(b<Llead1)
    {
      
      auto hterm=(-tl)*sites.op("Cdag", b)*(sites.op("C",b+1));
            hterm+=-tl*sites.op("Cdag", b+1)*(sites.op("C",b));
	    hterm+=-V*0.5*sites.op("N", b)*(sites.op("Id",b+1));
	    //itensor::step_func(current_time)*
	    //std::cout<< "b "<< b << " val "<< -V*0.5<<std::endl;
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
      return g;
     }
     else if(b==Llead1)
    {
            
      auto hterm=-tint*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tint*sites.op("Cdag", b+1)*(sites.op("C",b));
       hterm+=-V*0.5*sites.op("N", b)*(sites.op("Id",b+1));
       //       std::cout<< "b "<< b << " val "<< -V*0.5<<std::endl;
       //itensor::step_func(current_time)*
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
    }

else if(b> Llead1 && b<(Llead1+Lchain))
      {

   	auto hterm=-t0*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-t0*sites.op("Cdag", b+1)*(sites.op("C",b));
      hterm += gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      hterm += gamma*sites.op("NB",b)*sites.op("Id", b+1);
      hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+1));
      hterm += eps0*(sites.op("n",b)*sites.op("Id", b+1));
                  hterm+=(b-L_x)*E*sites.op("N", b)*(sites.op("Id",b+1));
		  //itensor::step_func(current_time)*
		  //	    std::cout<< "b "<< b << " val "<< (b-L_x)*E<<std::endl;
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
      }
     else if(b==Llead1+Lchain)
     {


       auto hterm=-tint*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tint*sites.op("Cdag", b+1)*(sites.op("C",b));
       hterm += gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
       hterm += gamma*sites.op("NB",b)*sites.op("Id", b+1);
       hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+1));
       hterm += eps0*(sites.op("n",b)*sites.op("Id", b+1));
        hterm+=(b-L_x)*E*sites.op("N", b)*(sites.op("Id",b+1));
       auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
       //itensor::step_func(current_time)*
   return g;
     }
  else {
          
           auto hterm=-tl*sites.op("Cdag", b)*(sites.op("C",b+1));
       hterm+=-tl*sites.op("Cdag", b+1)*(sites.op("C",b));
       hterm+=0.5*V*sites.op("N", b)*(sites.op("Id",b+1));
       //      std::cout<< "b "<< b << " val "<< V*0.5<<std::endl;
      if(b==Llead1+Llead2+Lchain-1)
   	{
  hterm+=0.5*V*sites.op("Id", b)*(sites.op("N",b+1));
  //itensor::step_func(current_time)*
  //       std::cout<< "b "<< b+1 << " val "<< V*0.5<<std::endl;
   	}
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
   }   
}

using IHGdot=holstDotGates<Holstein,std::vector<BondGate> >;

}

#endif /* MAKETIMEEVOPSTRUCT_H */
