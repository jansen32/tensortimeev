#ifndef HTHAM_H
#define HTHAM_H
#include "itensor/mps/siteset.h"
#include "itensor/all.h"
#include"extra.hpp"
namespace itensor{
  
  template <typename T>
std::vector<T> linspace(T a, T b, size_t N) {
    T h = (b - a) / static_cast<T>(N-1);
    std::vector<T> xs(N);
    typename std::vector<T>::iterator x;
    T val;
    for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
        *x = val;
    return xs;
}
    template<typename siteset>
  auto makeIntHam_bath(siteset sites, int Llead1,int LH, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
	  
	  double decay=1.;
	  // adding decaying hopping
	  //	  if(j<=4){

	    //decay=std::pow(Lambda, -double(Llead1-1-j)/2.);
	    //  decay=std::pow(Lambda, -double(5-j)/2.);
	    //  }
	  //std::cout<<"j " << j << " and "<< (5-j)<< " an "<< decay<<std::endl;	  
      	ampo += -tl*decay,"Cdag",j,"C",j+1;
	ampo+= -tl*decay, "Cdag",j+1,"C",j;

    }

   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;

  
  
 for(int j=Llead1+1; j  < Llead1+2*LH-2; j+=2)
        {
	  double decay=1;
	  // adding decaying hopping
// 	  if((Llead1+LH-j)<=4)
// 	    {
// 	      //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
// 	      decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

// 	    }
// std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;
	  ampo += -t0*decay,"Cdag",j,"C",j+2;
	ampo+= -t0*decay, "Cdag",j+2,"C",j;
	ampo += gamma,"NB",j,"Bdag_beta",j+1;
	ampo += gamma,"NBdag",j, "B_beta",j+1;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

 ampo += gamma,"NB",Llead1+2*LH-1,"Bdag_beta",Llead1+2*LH;
   ampo += gamma,"NBdag",Llead1+2*LH-1,"B_beta",Llead1+2*LH;
	    ampo += omega,"Nph",Llead1+2*LH-1;
	    ampo += eps0,"n",Llead1+2*LH-1;
	 
    
    return ampo;
  }
    template<typename siteset>
  auto makeIntHam(siteset sites, int Llead1,int LH, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
	  
	  double decay=1;
	  if(j<=4){

	    //decay=std::pow(Lambda, -double(Llead1-1-j)/2.);
	    //  decay=std::pow(Lambda, -double(5-j)/2.);
	  }
	  //std::cout<<"j " << j << " and "<< (5-j)<< " an "<< decay<<std::endl;	  
      	ampo += -tl*decay,"Cdag",j,"C",j+1;
	ampo+= -tl*decay, "Cdag",j+1,"C",j;

    }

   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   std::cout << "done 1 "<<std::endl;
  
  
 for(int j=Llead1+1; j  < Llead1+LH; j+=1)
        {
	  double decay=1;
// 	  if((Llead1+LH-j)<=4)
// 	    {
// 	      //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
// 	      decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

// 	    }
// std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;
 
	    
	  
      	ampo += -t0*decay,"Cdag",j,"C",j+1;
	ampo+= -t0*decay, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+LH;
	  ampo += gamma,"NBdag",Llead1+LH;
	    ampo += omega,"Nph",Llead1+LH;
	    ampo += eps0,"n",Llead1+LH;
	 
    
    return ampo;
  }

    template<typename siteset>
  auto makeIntHolHam(siteset sites, int Llead1,int LH, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
	  
	  double decay=1;
	  
	  
	  
      	ampo += -tl*decay,"Cdag",j,"C",j+1;
	ampo+= -tl*decay, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }
   ampo += gamma,"NB",Llead1;
   ampo += gamma,"NBdag",Llead1;
   ampo += omega,"Nph",Llead1;
   ampo += eps0,"n",Llead1;

   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   
  
  
 for(int j=Llead1+1; j  < Llead1+LH; j+=1)
        {
	  double decay=1;
// 	  if((Llead1+LH-j)<=4)
// 	    {
// 	      //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
// 	      decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

// 	    }
// std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;
 
	    
	  
      	ampo += -t0*decay,"Cdag",j,"C",j+1;
	ampo+= -t0*decay, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+LH;
	  ampo += gamma,"NBdag",Llead1+LH;
	    ampo += omega,"Nph",Llead1+LH;
	    ampo += eps0,"n",Llead1+LH;
	 
    
    return ampo;
  }

  
  // so far L_r=L_l =Lleads
template<typename siteset>
auto makeIntHamV(siteset sites, int Llead1, int LH, double V, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {


   
     auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V, "n",j;

    }
   ampo+= -V, "n",Llead1;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;

  
   for(int j=Llead1+1; j  < Llead1+LH; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;
			ampo+= +V, "n",j;

    }

   ampo += gamma,"NB",Llead1+LH;
	  ampo += gamma,"NBdag",Llead1+LH;
	    ampo += omega,"Nph",Llead1+LH;
	    ampo += eps0,"n",Llead1+LH;
	    ampo+= +V, "n", Llead1+LH;

    return ampo;
  }
template<typename siteset>
auto makeIntHamV_atd(siteset sites, int Llead1, int LH, double V, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0, double current_time=0.)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {


   
     auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V*step_func(current_time), "n",j;

    }
   ampo+= -V*step_func(current_time), "n",Llead1;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;

  
   for(int j=Llead1+1; j  < Llead1+LH; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;
	     ampo+= +V*step_func(current_time), "n",j;

    }
     
   ampo += gamma,"NB",Llead1+LH;
	  ampo += gamma,"NBdag",Llead1+LH;
	    ampo += omega,"Nph",Llead1+LH;
	    ampo += eps0,"n",Llead1+LH;
	    ampo+= +V*step_func(current_time), "n", Llead1+LH;

    return ampo;
  }
  template<typename siteset>
auto makeIntHamV_att_bath(siteset sites, int Llead1, int LH, double V, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0, double current_time=0.)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {


   
     auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V*step_func(current_time), "n",j;

    }
   ampo+= -V*step_func(current_time), "n",Llead1;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;

  
   for(int j=Llead1+1; j  < Llead1+2*LH-2; j+=2)
        {
      	ampo += -t0,"Cdag",j,"C",j+2;
	ampo+= -t0, "Cdag",j+2,"C",j;
	ampo += gamma,"NB",j,"Bdag_beta",j+1;
	ampo += gamma,"NBdag",j, "B_beta",j+1;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;
	ampo+= +V*step_func(current_time), "n",j;

    }
     
   ampo += gamma,"NB",Llead1+2*LH-1,"Bdag_beta",Llead1+2*LH;
   ampo += gamma,"NBdag",Llead1+2*LH-1,"B_beta",Llead1+2*LH;
	    ampo += omega,"Nph",Llead1+2*LH-1;
	    ampo += eps0,"n",Llead1+2*LH-1;
	    ampo+= +V*step_func(current_time), "n", Llead1+2*LH-1;

    return ampo;
  }
template<typename siteset>
auto makeHetHam(siteset sites, int Llead1,int Llead2, int Lchain, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {

    auto ampo = AutoMPO(sites);

   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
   	ampo+= -tl, "Cdag",j+1,"C",j;

    }
      for(int j=1+Llead1+Lchain; j  < Llead1+Llead2+Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
   	ampo+= -tl, "Cdag",j+1,"C",j;
	
    }


   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   ampo += -tint,"Cdag",Llead1+ Lchain,"C",Llead1+ Lchain+1;
    ampo+= -tint, "Cdag",Llead1+ Lchain+1,"C",Llead1+ Lchain;
  
    for(int j=Llead1+1; j  < Llead1+Lchain; j+=1)
         {
       	ampo += -t0,"Cdag",j,"C",j+1;
    	ampo+= -t0, "Cdag",j+1,"C",j;
   	ampo += gamma,"NB",j;
   	  ampo += gamma,"NBdag",j;
   	ampo += omega,"Nph",j;
   	ampo += eps0,"n",j;

     }

   ampo += gamma,"NB",Llead1+Lchain;
   	  ampo += gamma,"NBdag",Llead1+Lchain;
   	    ampo += omega,"Nph",Llead1+Lchain;
   	    ampo += eps0,"n",Llead1+Lchain;


    
    return ampo;
  }
  template<typename siteset>
auto makeHetHam_pp(siteset sites, int Llead1,int Llead2, int Lchain, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
    auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
   	ampo+= -tl, "Cdag",j+1,"C",j;

    }
      for(int j=Llead1+2*Lchain+1; j  < Llead1+Llead2+2*Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
   	ampo+= -tl, "Cdag",j+1,"C",j;
	
    }


          ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
      ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
          ampo += -tint,"Cdag",Llead1+ 2*Lchain-1,"C",Llead1+ 2*Lchain+1;
      ampo+= -tint, "Cdag",Llead1+ 2*Lchain+1,"C",Llead1+ 2*Lchain-1;
  
      for(int j=Llead1+1; j  < Llead1+2*Lchain-2; j+=2)
          {

         	ampo += -t0,"Cdag",j,"C",j+2;
      	ampo+= -t0, "Cdag",j+2,"C",j;
   	ampo += gamma,"NB",j, "Bdag_beta", j+1;
   	ampo += gamma,"NBdag",j, "B_beta", j+1;
    	ampo += omega,"Nph",j;
    	ampo += eps0,"n",j;


       }

    ampo += gamma,"NB",Llead1+2*Lchain-1, "Bdag_beta", Llead1+2*Lchain;
    ampo += gamma,"NBdag",Llead1+2*Lchain-1,"B_beta",Llead1+2*Lchain;
	  
    ampo += omega,"Nph",Llead1+2*Lchain-1;
   	    ampo += eps0,"n",Llead1+2*Lchain-1;
	 

    
    return ampo;
  }
  // so far L_r=L_l =Lleads
  template<typename siteset>
  auto makeHetHamV_pp_att(siteset sites, int Llead1, int Llead2, int Lchain,  double V, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0, double current_time=0.)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {


   
     auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V*step_func(current_time), "n",j;

    }
      for(int j=1+Llead1+2*Lchain; j  < Llead1+Llead2+2*Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= +V*step_func(current_time), "n",j;
    }

      ampo+= -V*step_func(current_time), "n",Llead1;
 ampo+= +V*step_func(current_time), "n", Llead1+Llead2+2*Lchain;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   ampo += -tint,"Cdag",Llead1+ 2*Lchain-1,"C",Llead1+ 2*Lchain+1;
   ampo+= -tint, "Cdag",Llead1+ 2*Lchain+1,"C",Llead1+ 2*Lchain-1;
  
   for(int j=Llead1+1; j<Llead1+2*Lchain-2; j+=2)
        {
      	ampo += -t0,"Cdag",j,"C",j+2;
	ampo+= -t0, "Cdag",j+2,"C",j;
	ampo += gamma,"NB",j, "Bdag_beta", j+1;
	ampo += gamma,"NBdag",j, "B_beta", j+1;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+2*Lchain-1,  "Bdag_beta", Llead1+2*Lchain;
   ampo += gamma,"NBdag",Llead1+2*Lchain-1, "B_beta", Llead1+2*Lchain;
	    ampo += omega,"Nph",Llead1+2*Lchain-1;
	    ampo += eps0,"n",Llead1+2*Lchain-1;
	 

    return ampo;
  }
template<typename siteset>
auto makeHetHamV(siteset sites, int Llead1, int Llead2, int Lchain,  double V, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {


   
     auto ampo = AutoMPO(sites);
 int L_x=Llead1+(Lchain+1)/2;
 double E=0;
 if(Lchain>1)
   {
E=V/(Lchain+1);  
   }
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V/2, "n",j;

    }
      for(int j=1+Llead1+Lchain; j  < Llead1+Llead2+Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= +V/2, "n",j;
    }

      ampo+= -V/2, "n",Llead1;
 ampo+= +V/2, "n", Llead1+Llead2+Lchain;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   ampo += -tint,"Cdag",Llead1+ Lchain,"C",Llead1+ Lchain+1;
   ampo+= -tint, "Cdag",Llead1+ Lchain+1,"C",Llead1+ Lchain;
  
   for(int j=Llead1+1; j  < Llead1+Lchain; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;
	ampo+=(j-L_x)*E,"n", j;
    }

   ampo += gamma,"NB",Llead1+Lchain;
	  ampo += gamma,"NBdag",Llead1+Lchain;
	    ampo += omega,"Nph",Llead1+Lchain;
	    ampo += eps0,"n",Llead1+Lchain;
	    ampo+=(Llead1+Lchain-L_x)*E,"n", Llead1+Lchain;	 

    return ampo;
  }
  template<typename siteset>
  auto makeHetHamV_pp(siteset sites, int Llead1,int Llead2, int Lchain, double V,double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {
 auto ampo = AutoMPO(sites);
 int L_x=Llead1+(Lchain+1)/2;
 double E=0;
 if(Lchain>1)
   {
E=V/(Lchain+1);  
   }
    
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
   	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V/2, "n",j;

    }
      for(int j=Llead1+2*Lchain+1; j  < Llead1+Llead2+2*Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
   	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= +V/2, "n",j;
    }


          ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
      ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
          ampo += -tint,"Cdag",Llead1+ 2*Lchain-1,"C",Llead1+ 2*Lchain+1;
      ampo+= -tint, "Cdag",Llead1+ 2*Lchain+1,"C",Llead1+ 2*Lchain-1;
        ampo+= -V/2, "n",Llead1;
 ampo+= +V/2, "n", Llead1+Llead2+2*Lchain;
 int i=Llead1+1;
      for(int j=Llead1+1; j  < Llead1+2*Lchain-2; j+=2)
          {

         	ampo += -t0,"Cdag",j,"C",j+2;
      	ampo+= -t0, "Cdag",j+2,"C",j;
   	ampo += gamma,"NB",j, "Bdag_beta", j+1;
   	ampo += gamma,"NBdag",j, "B_beta", j+1;
    	ampo += omega,"Nph",j;
    	ampo += eps0,"n",j;
	ampo+=(i-L_x)*E,"n", j;
	i++;

       }

    ampo += gamma,"NB",Llead1+2*Lchain-1, "Bdag_beta", Llead1+2*Lchain;
    ampo += gamma,"NBdag",Llead1+2*Lchain-1,"B_beta",Llead1+2*Lchain;
	  
    ampo += omega,"Nph",Llead1+2*Lchain-1;
   	    ampo += eps0,"n",Llead1+2*Lchain-1;
	  ampo+=(Llead1+Lchain-L_x)*E,"n", Llead1+2*Lchain-1;	 

    
    return ampo;
  }
template<typename siteset>
auto makeHetHamV_att(siteset sites, int Llead1, int Llead2, int Lchain,  double V, double tl,  double tint, double t0=1, double gamma=1, double omega=1, double eps0=0, double current_time=0)
->std::enable_if_t<!is_Mixed<siteset>::value, AutoMPO>
  {


   
     auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -step_func(current_time)*V, "n",j;
    }
      for(int j=1+Llead1+Lchain; j  < Llead1+Llead2+Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= +step_func(current_time)*V, "n",j;
    }

      ampo+= -step_func(current_time)*V, "n",Llead1;
 ampo+= +step_func(current_time)*V, "n", Llead1+Llead2+Lchain;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   ampo += -tint,"Cdag",Llead1+ Lchain,"C",Llead1+ Lchain+1;
   ampo+= -tint, "Cdag",Llead1+ Lchain+1,"C",Llead1+ Lchain;
  
   for(int j=Llead1+1; j  < Llead1+Lchain; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+Lchain;
	  ampo += gamma,"NBdag",Llead1+Lchain;
	    ampo += omega,"Nph",Llead1+Lchain;
	    ampo += eps0,"n",Llead1+Lchain;
	 

    return ampo;
  }
template<typename siteset>
auto makeHetFerHam(siteset sites, int Llead1, int Llead2, int Lchain, double tl,  double tint, double t0=1,  double ep0=1)
->AutoMPO
  {
   auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;

    }
      for(int j=1+Llead1+Lchain; j  < Llead1+ Llead2+Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;


    }


   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
   ampo += -tint,"Cdag",Llead1+ Lchain,"C",Llead1+ Lchain+1;
   ampo+= -tint, "Cdag",Llead1+ Lchain+1,"C",Llead1+ Lchain;
   for(int j=Llead1+1; j  < Llead1+Lchain; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;

    }
	 

    
    return ampo;
  }
  // so far L_r=L_l =Lleads
template<typename siteset>
auto makeHetFerHamV(siteset sites, int Llead1, int Llead2, int Lchain,  double V, double tl,  double tint, double t0=1, double U=1, double ep0=1)
->AutoMPO
  {
  auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V, "N",j;
	 
    }
     for(int j=1+Llead1+ Lchain;j  < Llead1+Llead2+ Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	 ampo+= V, "N",j;
	 
    }

	 ampo+= -V, "N",Llead1;
	 ampo+= V, "N",Llead1+ Lchain+Llead2;
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
    ampo += -tint,"Cdag",Llead1+ Lchain,"C",Llead1+ Lchain+1;
   ampo+= -tint, "Cdag",Llead1+ Lchain+1,"C",Llead1+ Lchain;
   for(int j=Llead1+1; j  < Llead1+Lchain; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;

    }
	 

    
    return ampo;
  }
  template<typename siteset>
  auto jFer(siteset sites, int Lleads1, int Lleads2, int Lchain, double tl)
->AutoMPO
  {
int N=length(sites);
    Cplx preFac=Cplx_i*tl;
        auto ampo = AutoMPO(sites);
	//     for(int j=1;j  < Lleads; j+=1)
        // {

      	ampo += preFac,"Cdag",Lleads1,"C",Lleads1+1;
	ampo+= -preFac, "Cdag",Lleads1+1,"C",Lleads1 ;
	
      	ampo += preFac,"Cdag",(Lchain +Lleads1),"C",(Lchain +Lleads1+1);
	ampo+= -preFac, "Cdag",(Lchain +Lleads1+1),"C",(Lchain +Lleads1);



	//    }

	return ampo;
  }
  template<typename siteset>
  auto jint(siteset sites, int Lleads1, double tint)
->MPO
  {
 auto ampo = AutoMPO(sites);
    Cplx preFac=Cplx_i*tint;
    ampo += preFac,"Cdag",Lleads1,"C",Lleads1+1; // one extra minus from -t_hyb, see derivation
	 ampo+= -preFac, "Cdag",Lleads1+1,"C",Lleads1 ;
	
;

    return ampo;
  }
    template<typename siteset>
auto Neint(siteset sites, int Lleads1)
->MPO
  {


        auto ampo = AutoMPO(sites);
	for(int i=Lleads1+1; i<=length(sites); i++)
	  {
      	ampo += 1,"N",i;
	  }
    return ampo;
  }

template<typename siteset>
auto dens_dens_correl(siteset sites, MPS&  psi, int Lleads1,int Lchain, std::string dirname)
->void
  {
    std::complex<double> val=0;
    for(int i=Lleads1+1; i<=Lleads1+Lchain; i++)
      {

	
psi.position(i);
	auto psidag=dag(psi);
	psidag.prime("Link");
    for(int j=i; j<=Lleads1+Lchain; j++)
      {
	int max_val=j;
	int min_val=i;
	

auto li_1=leftLinkIndex(psi, min_val);
	auto op_i=op(sites, "Cdag", min_val);
	auto op_j=op(sites, "C", max_val);
	auto C=prime(psi(min_val), li_1)*op_i;
	C*=prime(psidag(min_val), "Site");
	
	
	if(i!=j)
	  {
	    for(int k=min_val+1; k<max_val; ++k)
	      {
		C*=psi(k);
		C*=psidag(k);
	      }
 auto lj=rightLinkIndex(psi,max_val);
 	 C*=prime(psi(max_val),lj)*op_j;
 	 C*=prime(psidag(max_val),"Site");
	
	 val=eltC(C);
}
	else{

 	  auto ket = psi(j);
	  auto bra = dag(prime(ket, "Site"));
  auto nop = op(sites,"n",j);
  auto Nphop = op(sites,"Nph",j);
 auto Xop = op(sites,"X",j);
	  
 val=eltC(bra*nop*ket);

      double val_nph=real(eltC(bra*Nphop*ket));
   double val_x=real(eltC(bra*Xop*ket));
	std::vector<double> nphvec;
	std::vector<double> xvec;
	nphvec.push_back(val_nph);
	xvec.push_back(val_x);
auto vecSize_ph=nphvec.size();
itensor::ToFile(nphvec, dirname+"/Nphat"+std::to_string(j)+".dat",vecSize_ph);
itensor::ToFile(xvec, dirname+"/Xat"+std::to_string(j)+".dat",vecSize_ph);
}

 	std::vector<std::complex<double>> nevec2;
 	nevec2.push_back(val);
 	auto vecSize=nevec2.size();
 	itensor::ToFile(nevec2, dirname+"/CdagCi"+std::to_string(i)+"j"+std::to_string(j)+".dat",vecSize);
    
    

}


      }


    return;
  }


template<typename siteset>
auto dens_densities_pp(siteset sites, MPS&  psi, int Lleads1,int Lchain, std::string dirname)
->void
  {
    
    for(int i=Lleads1+1; i<=Lleads1+2*Lchain-1; i+=2)
      {

	


 	  auto ket = psi(i);
	  auto bra = dag(prime(ket, "Site"));
  auto nop = op(sites,"n",i);
  auto Nphop = op(sites,"Nph",i);
  //auto Xop = op(sites,"X",i);
	  
double val=real(eltC(bra*nop*ket));

      double val_nph=real(eltC(bra*Nphop*ket));
      //double val_x=real(eltC(bra*Xop*ket));
	std::vector<double> nphvec;
	//std::vector<double> xvec;
	nphvec.push_back(val_nph);
	//	xvec.push_back(val_x);
auto vecSize_ph=nphvec.size();
itensor::ToFile(nphvec, dirname+"/Nphat"+std::to_string(i)+".dat",vecSize_ph);
//itensor::ToFile(xvec, dirname+"/Xat"+std::to_string(i)+".dat",vecSize_ph);


 	std::vector<double> nevec2;
 	nevec2.push_back(val);

 	itensor::ToFile(nevec2, dirname+"/nat"+std::to_string(i)+".dat",vecSize_ph);
    
    

}


      


    return;
  }


template<typename siteset>
auto get_local_basis(siteset sites, MPS&  psi, int Lleads1,int Lchain, std::string dirname, int M,double t)
->void
  {
 

    for(int i=Lleads1+1; i<=Lleads1+Lchain; i++)
      {
 std::vector<double> reg0;
 std::vector<double> reg1;
 std::vector<double> op0;
 std::vector<double> op1;


 std::vector<std::complex<double>> lbovec1_0;
 std::vector<std::complex<double>> lbovec1_1;
 std::vector<std::complex<double>> lbovec2_0;
 std::vector<std::complex<double>> lbovec2_1;

     psi.position(i);



     auto ket=psi.A(i);
     auto bra=ket;  
   auto ind=findIndex(bra,"Site");
      bra.prime(ind);
      auto rho=ket*dag(bra);
      //      print(rho);
      auto rho_cp=rho;
      auto [U, D1] =diagPosSemiDef(rho_cp,{"RespectDegenerate" ,true, "Truncate", false,"ShowEigs", true});
      auto new_ind_set_1=rho.inds();
      auto new_ind_1=new_ind_set_1[0];

 auto new_ind_set_2=D1.inds();
      auto new_ind_2=new_ind_set_2[0];
   auto inds_U=inds((U));
      for(int j=1; j<=M+1; j++)
	{
	  reg0.push_back(real(rho.eltC(new_ind_1(j), new_ind_set_1[1](j))));
	  reg1.push_back(real(rho.eltC(new_ind_1(M+1+j), new_ind_set_1[1](M+1+j))));

	  op0.push_back(D1.elt(new_ind_2(j), new_ind_set_2[1](j)));
	  op1.push_back(D1.elt(new_ind_2(M+1+j), new_ind_set_2[1](M+1+j)));

	  // getting the first 2 optimal modes
	  	  	  lbovec1_0.push_back(U.eltC(inds_U[0](j), inds_U[1](1)));
   lbovec1_1.push_back(U.eltC(inds_U[0](M+1+j), inds_U[1](M+1+1)));
 lbovec2_0.push_back(U.eltC(inds_U[0](j), inds_U[1](2)));
   lbovec2_1.push_back(U.eltC(inds_U[0](M+1+j), inds_U[1](M+1+2)));



}

      auto vecSize=reg0.size();
      itensor::bin_write( dirname+"/locn0basis_site"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",reg0);      
      itensor::bin_write( dirname+"/locn1basis_site"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",reg1);      
            itensor::bin_write( dirname+"/optn0basis_site"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",op0);      
            itensor::bin_write( dirname+"/optn1basis_site"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",op1);      

            itensor::bin_write( dirname+"/opt_staten0_1at"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",lbovec1_0);      
            itensor::bin_write( dirname+"/opt_staten1_1at"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",lbovec1_1);      

            itensor::bin_write( dirname+"/opt_staten0_2at"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",lbovec2_0);      
            itensor::bin_write( dirname+"/opt_staten1_2at"+std::to_string(i)+"attime"+std::to_string(t).substr(0, 6)+".bin",lbovec2_1);      
     
	    
}


      


    return;
  }

template<typename siteset>
auto j(siteset sites, int Lleads1,int Lleads2, int Lchain, double tint)
->MPO
  {

    Cplx preFac=0.5*Cplx_i*tint;
        auto ampo = AutoMPO(sites);


      	ampo += preFac,"Cdag",Lleads1,"C",Lleads1+1;
	ampo+= -preFac, "Cdag",Lleads1+1,"C",Lleads1 ;
	
      	 ampo += preFac,"Cdag",(Lchain +Lleads1),"C",(Lchain +Lleads1+1);
	 ampo+= -preFac, "Cdag",(Lchain +Lleads1+1),"C",(Lchain +Lleads1);

    return ampo;
  }
  template<typename siteset>
  auto jEdot(siteset sites, int Lleads1,int Lleads2, int Lchain, double tl, double V)
->MPO
  {
    Cplx preFac=-0.5*Cplx_i;

        auto ampo = AutoMPO(sites);


	ampo+= preFac*tl*tl, "Cdag",Lleads1,"C",Lleads1-2 ;
      	ampo += -preFac*tl*tl,"Cdag",Lleads1-2,"C",Lleads1;

	ampo+= (1)*-1*V*preFac*tl*tl, "Cdag",Lleads1-3,"C",Lleads1-2 ;
	ampo += -1.*-1*(V)*preFac*tl*tl,"Cdag",Lleads1-2,"C",Lleads1-3;
	// second term
	
	ampo+= +preFac, "Cdag",(Lchain +Lleads1+3),"C",(Lchain +Lleads1+1);      	
	ampo += -preFac,"Cdag",(Lchain +Lleads1+1),"C",(Lchain +Lleads1+3);
	
	

	ampo+= +1.*V*preFac, "Cdag",(Lchain +Lleads1+1),"C",(Lchain +Lleads1+2);      	
	ampo += -1.*V*preFac,"Cdag",(Lchain +Lleads1+2),"C",(Lchain +Lleads1+1);

    return ampo;
  }
  
template<typename siteset>
auto j_pp(siteset sites, int Lleads1,int Lleads2, int Lchain, double tint)
->MPO
  {

    Cplx preFac=0.5*Cplx_i*tint;
        auto ampo = AutoMPO(sites);


      	ampo += preFac,"Cdag",Lleads1,"C",Lleads1+1;
	ampo+= -preFac, "Cdag",Lleads1+1,"C",Lleads1 ;
	
      	 ampo += preFac,"Cdag",(2*Lchain +Lleads1-1),"C",(2*Lchain +Lleads1+1);
	 ampo+= -preFac, "Cdag",(2*Lchain +Lleads1+1),"C",(2*Lchain +Lleads1-1);

    return ampo;
  }
    template<typename siteset>
    auto Edot_ppMPO(siteset sites, int Llead1,int Llead2, int Lchain, double t0, double gamma, double omega, double eps0 )
->MPO
  {
     auto ampo = AutoMPO(sites);

    for(int j=Llead1+1; j<Llead1+2*Lchain-2; j+=2)
        {
      	ampo += -t0,"Cdag",j,"C",j+2;
	ampo+= -t0, "Cdag",j+2,"C",j;
	ampo += gamma,"NB",j, "Bdag_beta", j+1;
	ampo += gamma,"NBdag",j, "B_beta", j+1;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+2*Lchain-1,  "Bdag_beta", Llead1+2*Lchain;
   ampo += gamma,"NBdag",Llead1+2*Lchain-1, "B_beta", Llead1+2*Lchain;
	    ampo += omega,"Nph",Llead1+2*Lchain-1;
	    ampo += eps0,"n",Llead1+2*Lchain-1;

    return ampo;
  }
     template<typename siteset>
   auto Xdot_ppMPO(siteset sites, int Llead1,int Llead2, int Lchain)
->MPO
  {

   auto ampo = AutoMPO(sites);

   ampo += 1,"B",Llead1+2*Lchain-1,  "Bdag_beta", Llead1+2*Lchain;
   ampo += 1,"Bdag",Llead1+2*Lchain-1, "B_beta", Llead1+2*Lchain;


    return ampo;
  }
   template<typename siteset>
   auto Pdot_ppMPO(siteset sites, int Llead1,int Llead2, int Lchain)
->MPO
  {
 auto ampo = AutoMPO(sites);

  

  
   ampo += itensor::Cplx_i,"Bdag",Llead1+2*Lchain-1, "B_beta", Llead1+2*Lchain;
    ampo += -itensor::Cplx_i,"B",Llead1+2*Lchain-1,  "Bdag_beta", Llead1+2*Lchain;


    return ampo;
  }
   template<typename siteset>
   auto ELdot(siteset sites, int Llead1,int Llead2, int Lchain, double tl, double V)
->MPO
  {

     auto ampo = AutoMPO(sites);
   for(int j=1; j< Llead1; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= -V, "n",j;

    }
ampo+= -V, "n",Llead1;

    return ampo;
  }
  template<typename siteset>
  auto ERdot(siteset sites, int Llead1,int Llead2, int Lchain, double tl, double V)
->MPO
  {

     auto ampo = AutoMPO(sites);
     for(int j=Llead1+Lchain+1; j < Llead1+Llead2+Lchain; j+=1)
        {
      	ampo += -tl,"Cdag",j,"C",j+1;
	ampo+= -tl, "Cdag",j+1,"C",j;
	ampo+= +V, "n",j;

    }
ampo+= +V, "n",Llead1+Llead2+Lchain;

    return ampo;
  }
  template<typename siteset>
  auto CDWOstruct(siteset sites, int Llead1,int Lchain)
->MPO
  {

     auto ampo = AutoMPO(sites);
     int i=-1;
     for(int j=Llead1+1; j <= Llead1+Lchain; j+=1)
        {
      	ampo += i,"N",j;
	i*=-1;


    }


     return toMPO(ampo);
  }

  template<typename siteset>
  auto CDWOstruct_pp(siteset sites, int Llead1,int Lchain)
->MPO
  {

     auto ampo = AutoMPO(sites);
     
     int i=-1;
     // making sure only calculate every second site

     for(int j=Llead1+1; j <= Llead1+2*Lchain; j+=2)
        {

      	ampo += i,"N",j;
	i*=-1;

}



     return toMPO(ampo);
  }
template<typename siteset>
  auto CDWDispstruct(siteset sites, int Llead1,int Lchain)
->MPO
  {

     auto ampo = AutoMPO(sites);
     int i=-1;
     for(int j=Llead1+1; j <= Llead1+Lchain; j+=1)
        {
	  ampo += i,"B",j;
  ampo += i,"Bdag",j;
	i*=-1;


    }


     return toMPO(ampo);
  }

template<typename siteset>
  auto CDWDispstruct_pp(siteset sites, int Llead1,int Lchain)
->MPO
  {

     auto ampo = AutoMPO(sites);
     int i=-1;
     for(int j=Llead1+1; j <= Llead1+2*Lchain; j+=2)
        {
	  ampo += i,"B",j,"Bdag_beta",j+1;
	  ampo += i,"Bdag",j,"B_beta",j+1;
	i*=-1;


    }


     return toMPO(ampo);
  }
  template<typename siteset>
  auto Ephstruct(siteset sites, int Llead1, int Lchain, double omega)
->MPO
  {

     auto ampo = AutoMPO(sites);
     for(int j=Llead1+1; j <= Llead1+Lchain; j+=1)
        {
      	ampo += omega,"Nph",j;
	


    }


     return toMPO(ampo);
  }


  template<typename siteset>
  auto Ecoupstruct(siteset sites, int Llead1, int Lchain, double gamma)
->MPO
  {

     auto ampo = AutoMPO(sites);
     for(int j=Llead1+1; j <= Llead1+Lchain; j+=1)
        {

  	ampo += gamma,"NB",j;
   	  ampo += gamma,"NBdag",j;
	


    }


     return toMPO(ampo);
  }
  template<typename siteset>
  auto Nstruct(siteset sites, int Llead1, int Lchain)
->MPO
  {

     auto ampo = AutoMPO(sites);
     for(int j=Llead1+1; j <= Llead1+Lchain; j+=1)
        {
	  ampo += 1.,"N",j;
	


    }


     return toMPO(ampo);
  }
  template<typename siteset>
  auto Ekinstruct(siteset sites, int Llead1, int Lchain, double t0)
->MPO
  {

     auto ampo = AutoMPO(sites);
     for(int j=Llead1+1; j < Llead1+Lchain; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
	ampo+= -t0, "Cdag",j+1,"C",j;


    }


    return ampo;
  }
  template<typename siteset>
  auto Estruct(siteset sites, int Llead1,int Llead2, int Lchain, double t0, double gamma, double omega, double eps0)
->MPO
  {

     auto ampo = AutoMPO(sites);
for(int j=Llead1+1; j  < Llead1+Lchain; j+=1)
        {
      	ampo += -t0,"Cdag",j,"C",j+1;
   	ampo+= -t0, "Cdag",j+1,"C",j;
   	ampo += gamma,"NB",j;
   	  ampo += gamma,"NBdag",j;
   	ampo += omega,"Nph",j;
   	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+Lchain;
   	  ampo += gamma,"NBdag",Llead1+Lchain;
   	    ampo += omega,"Nph",Llead1+Lchain;
   	    ampo += eps0,"n",Llead1+Lchain;
	 

    
    return ampo;
  }
     template<typename siteset>
  auto EHYBdot(siteset sites, int Llead1,int Llead2, int Lchain, double tint)
->MPO
  {

     auto ampo = AutoMPO(sites);
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;
    ampo += -tint,"Cdag",Llead1+ Lchain,"C",Llead1+ Lchain+1;
   ampo+= -tint, "Cdag",Llead1+ Lchain+1,"C",Llead1+ Lchain;

    return ampo;
  }
       template<typename siteset>
  auto EHYBint(siteset sites, int Llead1, double tint)
->MPO
  {

     auto ampo = AutoMPO(sites);
   ampo += -tint,"Cdag",Llead1,"C",Llead1+1;
   ampo+= -tint, "Cdag",Llead1+1,"C",Llead1;


    return ampo;
  }
         template<typename siteset>
	 auto ELint(siteset sites, int Llead1,int Llead2, double tl)
->MPO
  {

    auto ampo = AutoMPO(sites);
   for(int j=1;j  < Llead1; j+=1)
        {
	  
	  double decay=1;
	  // if(j<=4){

	  //   //decay=std::pow(Lambda, -double(Llead1-1-j)/2.);
	  //   decay=std::pow(Lambda, -double(5-j)/2.);
	  // }
	  //	  std::cout<<"j " << j << " and "<< (5-j)<< " an "<< decay<<std::endl;	  
      	ampo += -tl*decay,"Cdag",j,"C",j+1;
	ampo+= -tl*decay, "Cdag",j+1,"C",j;

    }

    return ampo;
  }
         template<typename siteset>
	 auto ERint(siteset sites, int Llead1,int LH,  double t0=1, double gamma=1, double omega=1, double eps0=0)
->MPO
  {

    

     auto ampo = AutoMPO(sites);
 for(int j=Llead1+1; j  < Llead1+LH; j+=1)
        {
	  double decay=1;
	  // if((Llead1+LH-j)<=4)
	  //   {
	  //     //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
	  //     decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

	  //   }
	  //std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;	   
	    
	  
      	ampo += -t0*decay,"Cdag",j,"C",j+1;
	ampo+= -t0*decay, "Cdag",j+1,"C",j;
	ampo += gamma,"NB",j;
	  ampo += gamma,"NBdag",j;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

   ampo += gamma,"NB",Llead1+LH;
	  ampo += gamma,"NBdag",Llead1+LH;
	    ampo += omega,"Nph",Llead1+LH;
	    ampo += eps0,"n",Llead1+LH;
    return ampo;
  }
  template<typename siteset>
	 auto ERint_pp(siteset sites, int Llead1,int LH,  double t0=1, double gamma=1, double omega=1, double eps0=0)
->MPO
  {
     auto ampo = AutoMPO(sites);
      double decay=1;
 for(int j=Llead1+1; j  < Llead1+2*LH-2; j+=2)
        {
	 
	  // if((Llead1+LH-j)<=4)
	  //   {
	  //     //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
	  //     decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

	  // }
 //std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;	   
	    
	  
      	ampo += -t0*decay,"Cdag",j,"C",j+2;
	ampo+= -t0*decay, "Cdag",j+2,"C",j;
	ampo += gamma,"NB",j,"Bdag_beta",j+1;
	ampo += gamma,"NBdag",j,"B_beta",j+1;
	ampo += omega,"Nph",j;
	ampo += eps0,"n",j;

    }

 ampo += gamma,"NB",Llead1+2*LH-1,"Bdag_beta",Llead1+2*LH;
 ampo += gamma,"NBdag",Llead1+2*LH-1,"B_beta",Llead1+2*LH;
	    ampo += omega,"Nph",Llead1+2*LH-1;
	    ampo += eps0,"n",Llead1+2*LH-1;
    return ampo;
  }
         template<typename siteset>
	 auto ERkin(siteset sites, int Llead1,int LH,  double t0=1)
->MPO
  {

    

     auto ampo = AutoMPO(sites);
 for(int j=Llead1+1; j  < Llead1+LH; j+=1)
        {
	  double decay=1;
	  // if((Llead1+LH-j)<=4)
	  //   {
	  //     //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
	  //     decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

	  //   }
	  //std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;	   
	    
	  
      	ampo += -t0*decay,"Cdag",j,"C",j+1;
	ampo+= -t0*decay, "Cdag",j+1,"C",j;

    }

    return ampo;
  }

    template<typename siteset>
	 auto ERkin_pp(siteset sites, int Llead1,int LH,  double t0=1)
->MPO
  {

    

     auto ampo = AutoMPO(sites);
 for(int j=Llead1+1; j  < Llead1+2*LH-2; j+=2)
        {
	  double decay=1;
	  // if((Llead1+LH-j)<=4)
	  //   {
	  //     //	   decay=std::pow(Lambda, -double(j-(Llead1+1))/2.);
	  //     decay=std::pow(Lambda, -double(5-(Llead1+LH-j))/2.);

	  //   }
// std::cout<< " j "<< j << " and "<<(j-(Llead1+1))<< " an "<< decay<<std::endl;	  
 
	    
	  
      	ampo += -t0*decay,"Cdag",j,"C",j+2;
	ampo+= -t0*decay, "Cdag",j+2,"C",j;

    }

    return ampo;
  }
  
         template<typename siteset>
	 auto Nphint(siteset sites, int Llead1,int LH,  double omega=1)
->MPO
  {

    

     auto ampo = AutoMPO(sites);
 for(int j=Llead1+1; j  <= Llead1+LH; j+=1)
        {

	    

	ampo += omega,"Nph",j;


    }

    return ampo;
  }
     template<typename siteset>
	 auto Nphint_pp(siteset sites, int Llead1,int LH,  double omega=1)
->MPO
  {

    

     auto ampo = AutoMPO(sites);
 for(int j=Llead1+1; j  <= Llead1+2*LH-1; j+=2)
        {

	    

	ampo += omega,"Nph",j;


    }

    return ampo;
  }
}
#endif /* HTHAM_H */
