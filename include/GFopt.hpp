#pragma once
#include"itensor/all.h"
#include <functional>
#include<omp.h>
#include"files.hpp"
#include "basisextension.h"
#include "extra.hpp"
namespace itensor{
  void write_data(MPS& psi1, MPS& psi2, std::string obsname,std::string DIR, double time)
  {
    double n2=norm(psi2);
    double n1=norm(psi1);
    std::complex<double> Oval= innerC(psi2, psi1);
    std::vector<std::complex<double>> o;
    std::vector<double> t;
    std::vector<double> avBdK;
    std::vector<double> maxBdK;
    std::vector<double> avBdB;
    std::vector<double> maxBdB;
    std::vector<double> normK;
    std::vector<double> normB;
 
    t.push_back(time);
    avBdK.push_back(averageLinkDim(psi1));
    maxBdK.push_back(maxLinkDim(psi1));
normK.push_back(n1);
avBdB.push_back(averageLinkDim(psi2));
    maxBdB.push_back(maxLinkDim(psi2));
normB.push_back(n2);
    o.push_back(Oval);
    auto vecSize=t.size();

 
    itensor::ToFile(o, DIR+"/"+obsname+".dat",vecSize);
    itensor::ToFile(t, DIR+"/time.dat", vecSize);
    itensor::ToFile(avBdB, DIR+"/avBdB.dat", vecSize);
    itensor::ToFile(maxBdB, DIR+"/maxBdB.dat", vecSize);
    itensor::ToFile(avBdK, DIR+"/avBdK.dat", vecSize);
    itensor::ToFile(maxBdK, DIR+"/maxBdK.dat", vecSize);
    itensor::ToFile(normB, DIR+"/normB.dat", vecSize);
    itensor::ToFile(normK, DIR+"/normK.dat", vecSize);
    std::cout<< time<<" overlap"+obsname<< Oval<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
  }

      // acting mixed tdvp , normal and lbo for also evolving auxillery
   template<typename TimeEvolver, typename SiteSet>
 void CFactMPStdvpAux(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, double dt, double tot, std::string filename, size_t M,std::string obsname)
  {
  
    
     int n=0;
     psi1.position(1);
     psi2.position(1);
     auto ne = AutoMPO(sites);
     for(int j = 1; j < length(psi1); j+=1)
    {
      
    ne+=1,"n",j;
    }

     auto Ne = toMPO(ne);
     auto NORM=itensor::norm(psi1);
     std::cout<< " start norm "<< NORM<<'\n';
 	   while(n*dt<tot)
      {

	psi1.position(1);
	psi2.position(1);
write_data(psi1, psi2, obsname, filename, n*dt);
if constexpr(use_lbo<TimeEvolver>::value)
	      {
		std::string DIR=obsname+"noNormCF/";
		std::vector<double> lbomx;
		lbomx.push_back(TE1.maxLboDim());
		size_t vecSize=lbomx.size();
 	    itensor::ToFile(lbomx, DIR+"lbomx"+obsname+filename, vecSize);
	      }


 n++;
 	TE1.step();
		TE2.step();
      }
    return;
   }
  //  acting mixed tdvp , normal and lbo for
 template<typename TimeEvolver, typename SiteSet>
 void CFactMPStdvp(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites,MPO& O,  double dt, double tot, std::string DIR, size_t M,std::string obsname, Args const& argsApply = Args::global())
  {
     auto y1 = applyMPO(O,psi1,argsApply);
     psi1=y1;
     psi1.noPrime();
    
     int n=0;
     // if the code shoul start in 2 site tdvp amd switch to single site tdvp after a ertain bond dim 
     bool mix_1=TE1.args.getBool("Mix",false);
     bool mix_2=TE2.args.getBool("Mix",false);
     int CBD=0;
     if(mix_1)
       {CBD=TE1.args.getInt("CBD",200);}
     if(mix_2)
       {CBD=TE2.args.getInt("CBD",200);}
     psi1.position(1);
     psi2.position(1);
     auto ne = AutoMPO(sites);
     for(int j = 1; j < length(psi1); j+=1)
    {
      
    ne+=1,"n",j;
    }

     auto Ne = toMPO(ne);
     auto NORM=itensor::norm(psi1);
     std::cout<< " start norm "<< NORM<<'\n';
 	   while(n*dt<tot)
      {

	psi1.position(1);
	psi2.position(1);
	auto psi2x=applyMPO(O,psi2,argsApply); 
	psi2x.position(1);
	psi2x.noPrime();
 	write_data(psi1, psi2x, obsname, DIR, n*dt);
if constexpr(use_lbo<TimeEvolver>::value)
	      {

		std::vector<double> lbomx;
		lbomx.push_back(TE1.maxLboDim());
		size_t vecSize=lbomx.size();
 	    itensor::ToFile(lbomx, DIR+"/lbomx.dat", vecSize);
	      }
 if(mix_1 and (CBD<minLinkDim(psi1)) )
   {
     TE1.args.add("NumCenter",1);
     mix_1=false;
   }
  if(mix_2 and (CBD<minLinkDim(psi2)) )
   {
     TE2.args.add("NumCenter",1);
     mix_2=false;
   }
  std::cout<< mix_1 << "  at "<< minLinkDim(psi1)<<std::endl;
 n++;
 	TE1.step();
		TE2.step();
      }
    return;
   }
  
    // acting normal tdmrg lbo
 template<typename TimeEvolver, typename SiteSet>
 void GFactMPSGStdmrglbo(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites,MPO& O,  double dt, double tot, std::string DIR, size_t M,std::string obsname, Args const& argsApply = Args::global())
  {

     auto y1 = applyMPO(O,psi1,argsApply);
   
     psi1=y1;
     psi1.noPrime();

      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();
 		      psi1.position(1);
 		      psi2.position(1);
 	std::vector<std::complex<double>> expval;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
 	 auto ne = AutoMPO(sites);

 	 for(int j = 1; j < length(psi1); j+=1)
    {

    ne+=1,"n",j;
    }

 
 	 auto NORM=1;

 	   std::cout<< " start norm "<< NORM<<'\n';
	  
 	   while(n*dt<tot)
      {

 	  psi1.position(1);
 	  psi2.position(1);
	  auto psixx=TE2.getBareMps();
	  auto psi1x=TE1.getBareMps();
	auto psi2x=applyMPO(O,psixx,argsApply); 
	  psi2x.position(1);
	  psi1x.position(1);
	psi2x.noPrime();
 
  	  double n2=norm(psi2x);
 	  double n1=norm(psi1x);
  	  std::complex<double> Oval= innerC(psi2x, psi1x);
 	std::vector<std::complex<double>> o;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
 	    t.push_back(double(n*dt));
 	    avBd.push_back(averageLinkDim(psi1));
 	    maxBd.push_back(maxLinkDim(psi1));
 	    lbomx.push_back(TE1.maxLboDim());
 	    o.push_back(Oval);


       auto vecSize=t.size();

       itensor::ToFile(o, DIR+obsname+".dat",vecSize);
       itensor::ToFile(t, DIR+"time.dat", vecSize);
        itensor::ToFile(lbomx, DIR+"lbomx.dat", vecSize);
       itensor::ToFile(avBd, DIR+"avBd.dat", vecSize);
       itensor::ToFile(maxBd, DIR+"maxBd.dat", vecSize);
	    	    
 	 

       std::cout<< n*dt<<" overlap"+obsname+"KarnoNorm "<< Oval<<"lbo max psi1 "<<TE1.maxLboDim()<< "   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<<TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 n++;



 	TE1.lbotDmrgStep();
 	TE2.lbotDmrgStep();
 
      }
    return;
   }
  // acting normal tdmrg
 template<typename TimeEvolver, typename SiteSet>
 void GFactMPStdmrg(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites,MPO& O,  double dt, double tot, std::string filename, size_t M,std::string obsname, Args const& argsApply = Args::global())
  {
     auto y1 = applyMPO(O,psi1,argsApply);
     psi1=y1;
     psi1.noPrime();
  
      int n=0;
  
 		      psi1.position(1);
 		      psi2.position(1);
 	std::vector<std::complex<double>> expval;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
 	 auto ne = AutoMPO(sites);
 	 for(int j = 1; j < length(psi1); j+=1)
    {

    ne+=1,"n",j;
    }

 	 auto Ne = toMPO(ne);
 
 	 auto NORM=itensor::norm(psi1);
 	   std::cout<< " start norm "<< NORM<<'\n';
	  
 	   while(n*dt<tot)
      {

 	  psi1.position(1);
 	  psi2.position(1);
	auto psi2x=applyMPO(O,psi2,argsApply); 
	  psi2x.position(1);
	psi2x.noPrime();
 
  	  double n2=norm(psi2x);
 	  double n1=norm(psi1);
  	  std::complex<double> Oval= innerC(psi2x, psi1);
 	std::vector<std::complex<double>> o;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
 	    t.push_back(double(n*dt));
 	    avBd.push_back(averageLinkDim(psi1));
 	    maxBd.push_back(maxLinkDim(psi1));
 	    //lbomx.push_back(TE1.maxLboDim());
 	    o.push_back(Oval);
 // 	    //	    if(n%100==0){
 // 	    auto E1=innerC(psi1,H, psi1);
 // 	    ///innerC(psi1x, psi1x);
 // 	    auto E2=innerC(psi2x,H, psi2x);
 // 	    ///innerC(psi2x, psi2x);;
 // std::cout<< "E_01= " << E_01<< " E02= "<< E_02<<std::endl;     
 // std::cout<< "energy difference1 |E_0 -E|/E_0= " << std::abs(E1-E_01)/std::abs(E_01)<<std::endl;     
 // std::cout<< "energy difference2 |E_0 -E|/E_0= " << std::abs(E2-E_02)/std::abs(E_02)<<std::endl;
 //      //}

       auto vecSize=t.size();
        std::string DIR=obsname+"noNormKarGF/";
       itensor::ToFile(o, DIR+obsname+"GF"+filename,vecSize);
       itensor::ToFile(t, DIR+"time"+obsname+"GF"+filename, vecSize);
       // itensor::ToFile(lbomx, DIR+"lbomx"+obsname+"GF"+filename, vecSize);
       itensor::ToFile(avBd, DIR+"avBd"+obsname+"GF"+filename, vecSize);
       itensor::ToFile(maxBd, DIR+"maxBd"+obsname+"GF"+filename, vecSize);
	    	    
 	 
 // 	    // /NORM
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/innerC(psi1x, psi1x)<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/innerC(psi2x, psi2x)<<'\n';
       std::cout<< n*dt<<std::endl;
 	       std::cout<< n*dt<<" overlap"+obsname+"KarnoNorm "<< Oval<<"lbo max psi1 "<< "   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 n++;
	TE1.tDmrgStep();
 	TE2.tDmrgStep();


      }
    return;
   }
   // acting with karsh method tdvp
  // acting with karsh method tdmrg
        template<typename TimeEvolver, typename SiteSet>
 	void GFactnoNormKar(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, double dt, double tot, std::string dirname, int M, std::string obsname)
  {


      int n=0;
      
 TE1.makeLbo();
       TE2.makeLbo();

		     
 		      psi1.position(1);
 		      psi2.position(1);

 	 auto ne = AutoMPO(sites);
 	 for(int j = 1; j < length(psi1); j+=2)
    {

    ne+=1,"n",j;
    }

 auto Ne = toMPO(ne);
  auto psi1x=TE1.getBareMps();
  psi1x.position(1);

       while(n*dt<tot)
      {

 	 auto psi2x=TE2.getBareMps();
 	  psi1x=TE1.getBareMps();
 	  psi1x.position(1);
 	  psi2x.position(1);
 	write_data(psi1x, psi2x, obsname, dirname, n*dt);

		std::vector<double> lbomxK;
		lbomxK.push_back(TE1.maxLboDim());
std::vector<double> lbomxB;
		lbomxB.push_back(TE2.maxLboDim());
		size_t vecSize=lbomxB.size();
 	    itensor::ToFile(lbomxB, dirname+"/lbomxB.dat", vecSize);
itensor::ToFile(lbomxK, dirname+"/lbomxK.dat", vecSize);
	    std::cout<< "max lbo 1 "<< TE1.maxLboDim()<< " max lbo 1 "<< TE2.maxLboDim()<<std::endl;
	    n++;

 	TE1.lbotDmrgStep();
 	TE2.lbotDmrgStep();
      }
       return;
  }
}
