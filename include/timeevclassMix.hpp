
#ifndef TIMEEVCLASS_H
#define TIMEEVCLASS_H
#include"itensor/all.h"
#include"vidalnot.hpp"
#include"omp.h"
namespace itensor{
  template<typename MPSType>
  class TimeEvolve{

    using Gate = BondGate;
  public:
    TimeEvolve(const  std::vector<Gate>& gatelist, MPSType& state, size_t N, Args const& args = Args::global()) : gatelist(gatelist),  psi(state), N(N),  args(args){};
    double tDmrgStep(); // works with all, depends on gatelist

    //double TEBDStep();
    double TEBDStepO2(); // only works with 2 ord tebd
    double TEBDStepO2para(); // only works with 2 ord tebd
  private:
    void paraAct(int i); 
      std::vector<Gate> gatelist;
    MPSType& psi;
    int N;
   const Args& args;
  };

  template<typename MPSType>
double  TimeEvolve< MPSType>::tDmrgStep()
  {
    //     const bool verbose = args.getBool("Verbose",false);
    const bool normalize = args.getBool("Normalize",true);

    psi.position(gatelist.front().i1());
    Real tot_norm = norm(psi);

   

        auto g = gatelist.begin();
        while(g != gatelist.end())
            {
            auto i1 = g->i1();
            auto i2 = g->i2();
            auto AA = psi.A(i1)*psi.A(i2)*g->gate();
            AA.replaceTags("1, Site", "0, Site");

            ++g;
            if(g != gatelist.end())
                {
                //Look ahead to next gate position
		 
                auto ni1 = g->i1();
                auto ni2 = g->i2();
		
                //SVD AA to restore MPS form
                //before applying current gate
                if(ni1 >= i2)
                    {

                    psi.svdBond(i1,AA,Fromleft,args);
                    psi.position(ni1); //does no work if position already ni1
                    }
                else
                    {
                    psi.svdBond(i1,AA,Fromright,args);
                    psi.position(ni2); //does no work if position already ni2
                    }
                }
            else
                {
                //No next gate to analyze, just restore MPS formb
                psi.svdBond(i1,AA,Fromright,args);
                }
            }

        if(normalize)
            {
	                  tot_norm *= psi.normalize();
            }

    return tot_norm;
  };
 
  template<typename MPSType>
   double TimeEvolve< MPSType>::TEBDStepO2()
  {
       //  const bool verbose = args.getBool("Verbose",false);
 //    const bool normalize = args.getBool("Normalize",true);


        auto g = gatelist.begin();
        while(g != gatelist.end())
            {
 	      auto i1 = g->i1();
            auto i2 = g->i2();
	    
           auto AA = psi.A(i1)*psi.A(i2);
	   // 	     AA.mapprime(1,0,Site);
 	      AA=AA*g->gate();
	      AA.replaceTags("1, Site", "0, Site");
 	    		int mx=std::max(i1, i2);
 		int mn=std::min(i1, i2);

  	    g++;
               if(mx==N)
                    {
		      


 	      	       	  ITensor D;
 	      	        svd(AA,psi.Aref(mn),D,psi.Aref(mx) ,args);
 	     // // 	          if(args.getBool("DoNormalize",false))
             // // {
 	     //              D *= 1./itensor::norm(D);
	     // 		  //	    }
		      	  psi.Ls[mn]=D;
 	      	      psi.setA(mx, dag(psi.A(mn))*AA);


		         }
 		      else{
     
			//	Print(psi.A(mx));
  		      	ITensor D;
  		        svd(AA*psi.Ls[mx],psi.Aref(mn),D,psi.Aref(mx), args);
			// 		       if(args.getBool("DoNormalize",false))
			//             {
 	                  D *= 1./itensor::norm(D);
		          //  }
 psi.Ls[mn]=D;
    psi.setA(mx, dag(psi.A(mn))*AA);
    //	Print(psi.A(mx));
 		    }
	       
  	    }
	//	psi.set_centerN();

    return 1.;
  };
     template<typename MPSType>
   void TimeEvolve< MPSType>::paraAct(int i)
     {
        //        while(g != gatelist.end())
 //            {

  	      auto i1 = gatelist[i].i1();
             auto i2 = gatelist[i].i2();
	     // std::cout<< i1 << " "<< i2 << std::endl;
            auto AA = psi.A(i1)*psi.A(i2);
 // 	   // 	     AA.mapprime(1,0,Site);
  	      AA=AA*gatelist[i].gate();
              AA.replaceTags("1, Site", "0, Site");
  	    		int mx=std::max(i1, i2);
  		int mn=std::min(i1, i2);

 //  	    g++;
               if(mx==N)
                    {
		 
 			  ITensor D;
 		       svd(AA,psi.Aref(mn),D,psi.Aref(mx) ,args);
 	    	          if(args.getBool("DoNormalize",false))
            {
 	                  D *= 1./itensor::norm(D);
 			    }
 			  psi.Ls[mn]=D;
 		      psi.setA(mx, dag(psi.A(mn))*AA);
		    
 		    }
 		      else{
     
 
  		      	ITensor D;
  		        svd(AA*psi.Ls[mx],psi.Aref(mn),D,psi.Aref(mx), args);
 		       if(args.getBool("DoNormalize",false))
             {
 	                  D *= 1./itensor::norm(D);
 		            }
		       psi.Ls[mn]=std::move(D);
    psi.setA(mx, dag(psi.A(mn))*AA);
    
 		    }
     }
       template<typename MPSType>
   double TimeEvolve< MPSType>::TEBDStepO2para()
  {
       //  const bool verbose = args.getBool("Verbose",false);
 //    const bool normalize = args.getBool("Normalize",true);


       //  auto g = gatelist.begin();
    // std::cout<< "start 1 "<<std::endl;
    #pragma omp parallel
    {
#pragma omp for 
    for(size_t i=0; i<int((N-1)/2); i++)
      {
    	paraAct(i);
	//    	   	  Print(gatelist[i]);
    	    }
    // std::cout<< "stop " << std::endl;
// #pragma omp critical
//     {}
      // for(int i=0; i< gatelist.size(); i++)
      // 	{
      // 	  std::cout<< i << std::endl;
      // 	  Print(gatelist[i]);
      // 	}
      //Print(gatelist[int((N-1)/2)]);
      // Print(gatelist[int((N)/2)+int((N-1)/2)-1]);
    // 	Print(gatelist[int((N)/2)+int((N-1)/2)-1]);
	
    
      // std::cout<< int((N-1)/2)<< std::endl;
      // Print(gatelist[int((N-1)/2)+1]);
          #pragma omp for
    for(size_t i=size_t((N-1)/2); i<size_t((N)/2)+size_t((N-1)/2); i++)
      {
		paraAct(i);
	//Prsize_t(gatelist[i]);
    	    }
#pragma omp for 
    for(size_t i=0; i<size_t((N-1)/2); i++)
      {
    	paraAct(i);
	//    	   	  Print(gatelist[i]);
    	    }
    }
    
 // 	psi.set_centerN();

    return 1.;
  };
  //  template<typename Tensor,typename MPSType>
  //  double TimeEvolve<Tensor, MPSType>::TEBDStep()
  // {
  //   std::cout<< "does not work "<< std::endl;
    //     const bool verbose = args.getBool("Verbose",false);
    // const bool normalize = args.getBool("Normalize",true);
    // // psi.position(gatelist.front().i1());
    // Real tot_norm =1;
    // //norm(psi);

    // Real tsofar = 0;
    // int tt=1;
           


    //     auto g = gatelist.begin();
    //     while(g != gatelist.end())
    //         {

	     
    // 	      auto i1 = g->i1();
    //         auto i2 = g->i2();
    // 	      	      std::cout<< " started "<< i1 << "  " << i2 << std::endl;	    
    //         auto AA = psi.A(i1)*psi.A(i2);
    // 	    //	    Print(AA);
    // 	    //  AA.mapprime(1,0,Site);
    // 	      AA=AA*g->gate();
    //         AA.mapprime(1,0,Site);
    // 	    		int mx=std::max(i1, i2);
    // 		int mn=std::min(i1, i2);
    // 	    double cut=1E-10;
    // 	    g++;
    // 	    if(g!=gatelist.end())
    // 	      {
    //             //Look ahead to next gate position
    //             auto ni1 = g->i1();
    //             auto ni2 = g->i2();
	
    // 	    		int nmx=std::max(ni1, ni2);
    // 		int nmn=std::min(ni1, ni2);
    //             //SVD AA to restore MPS form
    //             //before applying current gate

    // 		//			std::cout<< mx << "  "<< mn<< std::endl;
    //           if(mx==N)
    //                 {
    // 		      if(nmx==mx)
    // 		      	{
    // 			  //		std::cout<< " A Acted on "<< i1 << "  " << i2 << std::endl;
    // 			  //  std::cout<< "here 1 "<<std::endl;
    // 			  Tensor D;
    // 			  //			  std::cout<< "A  is empty "<< isEmpty(AA)<< std::endl;
    // 		       svd(AA,psi.Aref(mn),D,psi.Aref(mx) ,{"Cutoff",cut});
    // 		          if(args.getBool("DoNormalize",false))
    //         {
    // 	                  D *= 1./itensor::norm(D);
    //         }
    // 			  psi.Ls[mn]=D;
    // 		      psi.setA(mx, dag(psi.A(mn))*AA);
    // 		                            }
    // 		      else{
    // 			//std::cout<< " B Acted on "<< i1 << "  " << i2 << std::endl;
			

	    	
    // 			  auto x=psi.Ls[mn-1].inds()[0];
    // 			  auto y=psi.Ls[mn-1].inds()[1];
	    	
    // 	     		  Tensor D;
    // 	     		  Tensor F=dag(psi.Ls[mn-1])*AA;
    // 			  F=F*delta(x, y);
    // 			  // std::cout<< "B  is empty "<< isEmpty(F)<< std::endl;
    // 			  svd(F,psi.Aref(mn),D, psi.Aref(mx) ,{"Cutoff",cut});
    // 	    	          if(args.getBool("DoNormalize",false))
    //         {
    // 	                D *= 1./itensor::norm(D);
    //         }

    // 	       		  psi.Ls[mn]=dag(D);
    // 	     		  psi.setA(mn, AA*dag(psi.A(mx)));

    // 		                            }
    // 		      }
                
    //         else
    //             {
    // 		  if(nmx>=mx)
    // 		    {
    // 		      // 			std::cout<< " C Acted on "<< i1 << "  " << i2 << std::endl;
    // 		      //		      		std::cout<< "Acted on "<< i1 << "  " << i2 << std::endl;
		      
    // 		      // redefined to account for correct ni1 not updated g
    // 		      	Tensor D;
    // 			Print(AA);
			
    // 			Print(psi.Ls[mx]);
    // 			Print(AA*psi.Ls[mx]);
    // 			Print(psi.Aref(mn));
    // 			Print(psi.Aref(mx));
    // 			//			  std::cout<< "C  is empty "<< isEmpty(AA*psi.Ls[mx])<< std::endl;
    // 		        svd(AA*psi.Ls[mx],psi.Aref(mn),D,psi.Aref(mx), {"Cutoff",cut});
    // 		          if(args.getBool("DoNormalize",false))
    //         {
    // 	                  D *= 1./itensor::norm(D);
    //         }
    // 			  psi.Ls[mn]=(D);
    // 			    std::cout<< "now  "<<std::endl;
    // 			    std::cout<< mx << " "<< mn<< std::endl;
    // 			    Print(psi.A(mn));
    // 			    Print(AA);
    // psi.setA(mx, dag(psi.A(mn))*AA);
    // Print(psi.Aref(mn));
    // Print(psi.Aref(mx));
    // 	    std::cout<< "then  "<<std::endl;

    // 		    }
    // 		  else{
    // 		    //		std::cout<< " D Acted on "<< i1 << "  " << i2 << std::endl;
    // 			  auto x=psi.Ls[mn-1].inds()[0];
    // 			  auto y=psi.Ls[mn-1].inds()[1];
	
    // 	     		  Tensor D;
    // 	     		  Tensor F=dag(psi.Ls[mn-1])*AA;
    // 			  F=F*delta(x, y);
    // 			  //	  std::cout<< "D  is empty "<< isEmpty(F)<< std::endl;
    // 	     		  svd(F,psi.Aref(mn),D, psi.Aref(mx) ,{"Cutoff",cut});
    // 			  // std::cout<< " finished "<< std::endl;
    // 			          if(args.getBool("DoNormalize",false)){
    // 	                  D *= 1./itensor::norm(D);
    //         }
    // 				  psi.Ls[mn]=dag(D);

    // 	     		  psi.setA(mn, AA*dag(psi.A(mx)));


    // 		  }
    //             }
    //         }
    // 	    else{
    // 	            			std::cout<< " E Acted on "<< i1 << "  " << i2 << std::endl;
	       	        
    // 	      //  		std::cout<< "Acted on "<< i1 << "  " << i2 << std::endl;
    // 	      //std::cout<< " started "<< std::endl;
    // 	     		  Tensor D;
    // 	     		  Tensor F=AA;
    // 			  // F=F*delta(x, y);
    // 			  //	  std::cout<< "E  is empty "<< isEmpty(F)<< std::endl;
    // 	     		  svd(F,psi.Aref(mn),D, psi.Aref(mx) ,{"Cutoff",cut});
    // 			  		          if(args.getBool("DoNormalize",false)){
    // 	                  D *= 1./itensor::norm(D);
    //         }
    // 						  psi.Ls[mn]=dag(D);

    // 	     		  psi.setA(mn, AA*dag(psi.A(mx)));
    // 			  // Print(psi.A(mn));
    // 			  // Print(psi.A(mx));
	    
    // 	    }
    // 	    }
    // 	//Print(psi.psi);
	    
    // 	//	Print(psi.psi);
    // 	//	print(psi);
    // 	std::cout<< " finsiehd 1"<< std::endl;
    //     if(normalize)
    //         {
    // 	      //  tot_norm *= psi.psi.normalize();
    //         }
	
	
	//	Print(psi.psi);
  //   return 0;
  // };
}
#endif /* TIMEEVCLASS_H */
