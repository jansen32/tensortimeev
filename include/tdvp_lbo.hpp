#pragma once
#include "itensor/iterativesolvers.h"
#include "itensor/mps/localmposet.h"
#include "itensor/mps/sweeps.h"
#include "itensor/mps/DMRGObserver.h"
#include "itensor/util/cputime.h"
#include "lbo.hpp"
#include "extra.hpp"
#include"lbo_solvers.h"
#include<map>
namespace itensor {

  
  
      using TensorPair=std::tuple<ITensor, ITensor>;
  template<typename MPSType>
class TDVP_lbo{
  private:
        MPSType& psi;
     MPO& H;
      std::complex<double> dt{0};
       Args& args;
    Sweeps sweeps;
    size_t N;
    LocalMPO PH;

    double max_lbo_svd{0};
    double max_lbo_solv{0};
    double mean_lbo_solv{0};
    std::vector<double> lbodims;
public:
    double get_max_lbo_svd()const {
      return  max_lbo_svd;
    }
        double get_max_lbo_solv()const {
      return  max_lbo_solv;
    }
        double get_mean_lbo_solv()const {
      return  mean_lbo_solv;
    }
    std::vector<double> get_all_lbodims_solv()const {
      return  lbodims;
    }
    TDVP_lbo(MPSType& psi,  MPO& H, std::complex<double> dt, Sweeps Sweeps_New, Args& args = Args::global()): psi(psi), H(H), dt(dt),args(args),sweeps(Sweeps_New), N(length(psi)) ,PH( LocalMPO(H,args)){lbodims=std::vector<double>(length(psi), 0);};

    template <class LocalOpT>
Real TDVPWorker_lbo(MPS & psi,
           LocalOpT& PH,
           Cplx t,
           Sweeps const& sweeps,
		    Args args);
    template <class LocalOpT>
Real TDVPWorker_lbo_impl2(MPS & psi,
           LocalOpT& PH,
           Cplx t,
           Sweeps const& sweeps,
		    Args args);

    template <class LocalOpT>
Real TDVPWorker_lbo_impl3(MPS & psi,
           LocalOpT& PH,
           Cplx t,
           Sweeps const& sweeps,
		    Args args);


    const bool use_lbo=true;
  void step_new( Sweeps& sweeps);
    void step(); // solves local equation with regular Krylov without LBO
        void step_impl2(); // solves local equation with RK4
    void step_impl3(); // solves local equation with Krylov LBO
      
};


  template<typename MPSType>
 void TDVP_lbo<MPSType>::step_new(  Sweeps& sweeps)
  {
         LocalMPO PH(H,args);
     Real energy = TDVPWorker_lbo(psi,PH,dt,sweeps,args);
  }
    template<typename MPSType>
 void TDVP_lbo<MPSType>::step()
  {
    // LocalMPO PH(H,args);
     Real energy = TDVPWorker_lbo(psi,PH,dt,sweeps,args);
  }

      template<typename MPSType>
 void TDVP_lbo<MPSType>::step_impl2()
  {
    // LocalMPO PH(H,args);
     Real energy = TDVPWorker_lbo_impl2(psi,PH,dt,sweeps,args);
  }

      template<typename MPSType>
 void TDVP_lbo<MPSType>::step_impl3()
  {
    // LocalMPO PH(H,args);
     Real energy = TDVPWorker_lbo_impl3(psi,PH,dt,sweeps,args);
  }

    template <typename MPSType>
  template<class LocalOpT>
Real   TDVP_lbo<MPSType>::TDVPWorker_lbo(MPS & psi,
           LocalOpT& PH,
           Cplx t,
           Sweeps const& sweeps,
           Args args)
    { 
   // Truncate blocks of degenerate singular values (or not)
    args.add("RespectDegenerate",args.getBool("RespectDegenerate",true));
  
    const bool silent = args.getBool("Silent",true);
    if(silent)
        {
        args.add("Quiet",true);
        args.add("PrintEigs",false);
        args.add("NoMeasure",true);
        args.add("DebugLevel",-1);
        }
    const bool quiet = args.getBool("Quiet",false);
    const int debug_level = args.getInt("DebugLevel",(quiet ? -1 : 0));
    const int numCenter = args.getInt("NumCenter",2);
    if(numCenter != 1)
        args.add("Truncate",args.getBool("Truncate",true));
    else
        args.add("Truncate",args.getBool("Truncate",false));
    const int N = length(psi);
    Real energy = NAN;

    if((!isOrtho(psi)) || (psi.leftLim() != 0))
        psi.position(1);

    args.add("DebugLevel",debug_level);
    //args.add("DoNormalize",true);
    // initializing max_lbo_svd=0 to only have store the maximum  per sweep
max_lbo_svd=0;
    for(int sw = 1; sw <= sweeps.nsweep(); ++sw)
        {
        cpu_time sw_time;
        args.add("Sweep",sw);
        args.add("NSweep",sweeps.nsweep());
        args.add("Cutoff",sweeps.cutoff(sw));
        args.add("MinDim",sweeps.mindim(sw));
        args.add("MaxDim",sweeps.maxdim(sw));
        args.add("MaxIter",sweeps.niter(sw));
 
        if(!PH.doWrite()
           && args.defined("WriteDim")
           && sweeps.maxdim(sw) >= args.getInt("WriteDim"))
            {
            if(!quiet)
                {
                println("\nTurning on write to disk, write_dir = ",
                        args.getString("WriteDir","./"));
                }
  
            //psi.doWrite(true);
            PH.doWrite(true,args);
            }

        // 0, 1 and 2-site wavefunctions
        ITensor phi0,phi1;
        Spectrum spec;
        for(int b = 1, ha = 1; ha <= 2; sweepnext(b,ha,N,{"NumCenter=",numCenter}))
            {


            if(!quiet)
                printfln("Sweep=%d, HS=%d, Bond=%d/%d",sw,ha,b,(N-1));

            PH.numCenter(numCenter);
            PH.position(b,psi);

            if(numCenter == 2)
	      { phi1 = psi(b)*psi(b+1);}
            else if(numCenter == 1)
	      {

phi1 = psi(b);
		
}
	  
            applyExp(PH,phi1,t/2,args);

            if(args.getBool("DoNormalize",true))
                phi1 /= norm(phi1);

            if(numCenter == 2)
	      {
	
		auto indx1=findIndex(psi.A(b), "Site");
		  auto indx2=findIndex(psi.A(b+1), "Site");
		 
  int old_llim=psi.leftLim();
  int old_rlim=psi.rightLim();
   		  auto [T1, T2]=applyLbo(phi1, indx1, indx2,b, args);
	       auto id1=findInds(T1, "OM");
	       auto id2=findInds(T2, "OM");
	       lbodims[b-1]=dim(id1);
	       lbodims[b]=dim(id2);

	       double new_dim(std::max(static_cast<double>(dim(id2)), static_cast<double>(dim(id1))));
	       max_lbo_svd=std::max(new_dim, max_lbo_svd);
	       
   		    phi1*=T1;
  		      		  phi1*=T2;
 		     psi.Aref(b+1)*=T2;
  		    psi.set(b , psi.A(b)*T1);

  	     		    psi.leftLim(old_llim);
  		    psi.rightLim(old_rlim);
		  	  spec = psi.svdBond(b,phi1,(ha==1 ? Fromleft : Fromright),PH,args);
				   
   old_llim=psi.leftLim();
   old_rlim=psi.rightLim();
   		  	phi1*=dag(T1);
   			   		 phi1*=dag(T2);



    		 psi.Aref(b+1)*=dag(T2);

    		 psi.Aref(b)*=dag(T1);
    		 		  psi.leftLim(old_llim);
   		  psi.rightLim(old_rlim);
	     
	      }
	    else if(numCenter == 1)
	      { psi.ref(b) = phi1; }

           // Calculate energy

            // ITensor PH_phi1;
	   
            // PH.product(phi1,PH_phi1);
            // energy = real(eltC(dag(phi1)*PH_phi1));
 
            if((ha == 1 && b+numCenter-1 != N) || (ha == 2 && b != 1))
                {
                auto b1 = (ha == 1 ? b+1 : b);
 
                if(numCenter == 2)
                    {
		     
                    phi0 = psi(b1);
		     
                    }
                else if(numCenter == 1)
                    {

		                    Index l;
                    if(ha == 1) l = commonIndex(psi(b),psi(b+1));
                    else        l = commonIndex(psi(b-1),psi(b));
                    ITensor U,S,V(l);
                    spec = svd(phi1,U,S,V,args);
                    psi.ref(b) = U;
                    phi0 = S*V;

                    }

                PH.numCenter(numCenter-1);
                PH.position(b1,psi);

                applyExp(PH,phi0,-t/2,args);
	
                if(args.getBool("DoNormalize",true))
                    phi0 /= norm(phi0);
                
                if(numCenter == 2)
                    {
                    psi.ref(b1) = phi0;
                    }
                if(numCenter == 1)
                    {
                    if(ha == 1) psi.ref(b+1) *= phi0;
                    else        psi.ref(b-1) *= phi0;
                    }

                 // Calculate energy
                 ITensor PH_phi0;
                 PH.product(phi0,PH_phi0);
                 energy = real(eltC(dag(phi0)*PH_phi0));
                 }
            else
                {
                // Calculate energy
                ITensor H_phi1;
                PH.product(phi1,H_phi1);
                energy = real(eltC(dag(phi1)*H_phi1));
                }
 

             if(!quiet)
                 { 
                 printfln("    Truncated to Cutoff=%.1E, Min_dim=%d, Max_dim=%d",
                          sweeps.cutoff(sw),
                          sweeps.mindim(sw), 
                          sweeps.maxdim(sw) );
                 printfln("    Trunc. err=%.1E, States kept: %s",
                          spec.truncerr(),
                          showDim(linkIndex(psi,b)) );
                 }

    	     //obs.lastSpectrum(spec);

            args.add("AtBond",b);
            args.add("HalfSweep",ha);
            args.add("Energy",energy); 
            args.add("Truncerr",spec.truncerr()); 

    	    //            obs.measure(args);

	    } // for loop over b

        if(!silent)
            {
            auto sm = sw_time.sincemark();
            printfln("    Sweep %d/%d CPU time = %s (Wall time = %s)",
                     sw,sweeps.nsweep(),showtime(sm.time),showtime(sm.wall));
            }
        
    	//        if(obs.checkDone(args)) break;
  
	} //for loop over sw
  
psi.rightLim(psi.leftLim()+2);
    if(args.getBool("DoNormalize",true))
        psi.normalize();
       
    return energy;
    }

    template <typename MPSType>
    template<class LocalOpT>
Real   TDVP_lbo<MPSType>::TDVPWorker_lbo_impl2(MPS & psi,
           LocalOpT& PH,
           Cplx t,
           Sweeps const& sweeps,
           Args args)
    { 
   // Truncate blocks of degenerate singular values (or not)
    args.add("RespectDegenerate",args.getBool("RespectDegenerate",true));
  
    const bool silent = args.getBool("Silent",true);
    if(silent)
        {
        args.add("Quiet",true);
        args.add("PrintEigs",false);
        args.add("NoMeasure",true);
        args.add("DebugLevel",-1);
        }
    const bool quiet = args.getBool("Quiet",false);
    const int debug_level = args.getInt("DebugLevel",(quiet ? -1 : 0));
    const int numCenter = args.getInt("NumCenter",2);
    if(numCenter != 1)
        args.add("Truncate",args.getBool("Truncate",true));
    else
        args.add("Truncate",args.getBool("Truncate",false));
    const int N = length(psi);
    Real energy = NAN;
    //    double lbo_max=0;
    if((!isOrtho(psi)) || (psi.leftLim() != 0))
        psi.position(1);

    args.add("DebugLevel",debug_level);
    //args.add("DoNormalize",true);
    // initializing max_lbo_svd=0 to only have store the maximum  per sweep
max_lbo_svd=0;
max_lbo_solv=0;
 mean_lbo_solv=0;
    for(int sw = 1; sw <= sweeps.nsweep(); ++sw)
        {
        cpu_time sw_time;
        args.add("Sweep",sw);
        args.add("NSweep",sweeps.nsweep());
        args.add("Cutoff",sweeps.cutoff(sw));
        args.add("MinDim",sweeps.mindim(sw));
        args.add("MaxDim",sweeps.maxdim(sw));
        args.add("MaxIter",sweeps.niter(sw));
 
        if(!PH.doWrite()
           && args.defined("WriteDim")
           && sweeps.maxdim(sw) >= args.getInt("WriteDim"))
            {
            if(!quiet)
                {
                println("\nTurning on write to disk, write_dir = ",
                        args.getString("WriteDir","./"));
                }
  
            //psi.doWrite(true);
            PH.doWrite(true,args);
            }

        // 0, 1 and 2-site wavefunctions
        ITensor phi0,phi1;
        Spectrum spec;
        for(int b = 1, ha = 1; ha <= 2; sweepnext(b,ha,N,{"NumCenter=",numCenter}))
            {


            if(!quiet)
                printfln("Sweep=%d, HS=%d, Bond=%d/%d",sw,ha,b,(N-1));

            PH.numCenter(numCenter);
            PH.position(b,psi);
	auto indx1=findIndex(psi.A(b), "Site");
		  auto indx2=findIndex(psi.A(b+1), "Site");
            if(numCenter == 2)
	      { phi1 = psi(b)*psi(b+1);}
            else if(numCenter == 1)
	      {

phi1 = psi(b);
		
}
	


	    auto [max_solv,mean_solv]= Runge_Kutta_4_lbo(PH,phi1,t/2,indx1, indx2, b,H,args);

	      // applyExp_lbo(PH,phi1,t/2,indx1, indx2, b,H,args);
	    // std::cout<< "meth 2 "<<std::endl;
	    //	    std::cout<< "b " << b << "  b+1 = " << b+1 << "  max "<< max_lbo_solv << " and " << max_solv <<std::endl;
	    max_lbo_solv=std::max(max_solv,max_lbo_solv);
	    mean_lbo_solv=std::max(mean_solv,mean_lbo_solv);


	     
            if(args.getBool("DoNormalize",true))
                phi1 /= norm(phi1);

            if(numCenter == 2)
	      {
	

		 
  int old_llim=psi.leftLim();
  int old_rlim=psi.rightLim();
   		  auto [T1, T2]=applyLbo(phi1, indx1, indx2,b, args);
	       auto id1=findInds(T1, "OM");
	       auto id2=findInds(T2, "OM");
	       lbodims[b-1]=dim(id1);
	       lbodims[b]=dim(id2);
	       double new_dim(std::max(static_cast<double>(dim(id2)), static_cast<double>(dim(id1))));
	       max_lbo_svd=std::max(new_dim, max_lbo_svd);
   		    phi1*=T1;
  		      		  phi1*=T2;


 		     psi.Aref(b+1)*=T2;

  		    psi.set(b , psi.A(b)*T1);
		    psi.leftLim(old_llim);
  		    psi.rightLim(old_rlim);
		  	  spec = psi.svdBond(b,phi1,(ha==1 ? Fromleft : Fromright),PH,args);
				   
   old_llim=psi.leftLim();
   old_rlim=psi.rightLim();
   		  	phi1*=dag(T1);
			phi1*=dag(T2);



    		 psi.Aref(b+1)*=dag(T2);
    		 psi.Aref(b)*=dag(T1);
    		 		  psi.leftLim(old_llim);
   		  psi.rightLim(old_rlim);
	     
	      }
	    else if(numCenter == 1)
	      { psi.ref(b) = phi1; }

           // Calculate energy

            // ITensor PH_phi1;
	   
            // PH.product(phi1,PH_phi1);
            // energy = real(eltC(dag(phi1)*PH_phi1));
 
            if((ha == 1 && b+numCenter-1 != N) || (ha == 2 && b != 1))
                {
                auto b1 = (ha == 1 ? b+1 : b);
 
                if(numCenter == 2)
                    {
		     
                    phi0 = psi(b1);
		     
                    }
                else if(numCenter == 1)
                    {

		                    Index l;
                    if(ha == 1) l = commonIndex(psi(b),psi(b+1));
                    else        l = commonIndex(psi(b-1),psi(b));
                    ITensor U,S,V(l);
                    spec = svd(phi1,U,S,V,args);
                    psi.ref(b) = U;
                    phi0 = S*V;

                    }

                PH.numCenter(numCenter-1);
                PH.position(b1,psi);
		                Runge_Kutta_4(PH,phi0,-t/2,args);
				//                applyExp(PH,phi0,-t/2,args);
	
                if(args.getBool("DoNormalize",true))
                    phi0 /= norm(phi0);
                
                if(numCenter == 2)
                    {
                    psi.ref(b1) = phi0;
                    }
                if(numCenter == 1)
                    {
                    if(ha == 1) psi.ref(b+1) *= phi0;
                    else        psi.ref(b-1) *= phi0;
                    }

                 // Calculate energy
                 ITensor PH_phi0;
                 PH.product(phi0,PH_phi0);
                 energy = real(eltC(dag(phi0)*PH_phi0));
                 }
            else
                {
                // Calculate energy
                ITensor H_phi1;
                PH.product(phi1,H_phi1);
                energy = real(eltC(dag(phi1)*H_phi1));
                }
 

             if(!quiet)
                 { 
                 printfln("    Truncated to Cutoff=%.1E, Min_dim=%d, Max_dim=%d",
                          sweeps.cutoff(sw),
                          sweeps.mindim(sw), 
                          sweeps.maxdim(sw) );
                 printfln("    Trunc. err=%.1E, States kept: %s",
                          spec.truncerr(),
                          showDim(linkIndex(psi,b)) );
                 }

    	     //obs.lastSpectrum(spec);

            args.add("AtBond",b);
            args.add("HalfSweep",ha);
            args.add("Energy",energy); 
            args.add("Truncerr",spec.truncerr()); 

    	    //            obs.measure(args);

	    } // for loop over b

        if(!silent)
            {
            auto sm = sw_time.sincemark();
            printfln("    Sweep %d/%d CPU time = %s (Wall time = %s)",
                     sw,sweeps.nsweep(),showtime(sm.time),showtime(sm.wall));
            }
        
    	//        if(obs.checkDone(args)) break;
  
	} //for loop over sw
  
psi.rightLim(psi.leftLim()+2);
    if(args.getBool("DoNormalize",true))
        psi.normalize();

    return energy;
    }

    template <typename MPSType>
    template<class LocalOpT>
Real   TDVP_lbo<MPSType>::TDVPWorker_lbo_impl3(MPS & psi,
           LocalOpT& PH,
           Cplx t,
           Sweeps const& sweeps,
           Args args)
    { 
   // Truncate blocks of degenerate singular values (or not)
    args.add("RespectDegenerate",args.getBool("RespectDegenerate",true));
  
    const bool silent = args.getBool("Silent",true);
    if(silent)
        {
        args.add("Quiet",true);
        args.add("PrintEigs",false);
        args.add("NoMeasure",true);
        args.add("DebugLevel",-1);
        }
    const bool quiet = args.getBool("Quiet",false);
    const int debug_level = args.getInt("DebugLevel",(quiet ? -1 : 0));
    const int numCenter = args.getInt("NumCenter",2);
    if(numCenter != 1)
        args.add("Truncate",args.getBool("Truncate",true));
    else
        args.add("Truncate",args.getBool("Truncate",false));
    const int N = length(psi);
    Real energy = NAN;
    //    double lbo_max=0;
    if((!isOrtho(psi)) || (psi.leftLim() != 0))
        psi.position(1);

    args.add("DebugLevel",debug_level);
    //args.add("DoNormalize",true);
    // initializing max_lbo_svd=0 to only have store the maximum  per sweep
    // afer proof 6
max_lbo_svd=0;
 max_lbo_solv=0;
 mean_lbo_solv=0;
    for(int sw = 1; sw <= sweeps.nsweep(); ++sw)
        {
        cpu_time sw_time;
        args.add("Sweep",sw);
        args.add("NSweep",sweeps.nsweep());
        args.add("Cutoff",sweeps.cutoff(sw));
        args.add("MinDim",sweeps.mindim(sw));
        args.add("MaxDim",sweeps.maxdim(sw));
        args.add("MaxIter",sweeps.niter(sw));
 
        if(!PH.doWrite()
           && args.defined("WriteDim")
           && sweeps.maxdim(sw) >= args.getInt("WriteDim"))
            {
            if(!quiet)
                {
                println("\nTurning on write to disk, write_dir = ",
                        args.getString("WriteDir","./"));
                }
  
            //psi.doWrite(true);
            PH.doWrite(true,args);
            }

        // 0, 1 and 2-site wavefunctions
        ITensor phi0,phi1;
        Spectrum spec;
        for(int b = 1, ha = 1; ha <= 2; sweepnext(b,ha,N,{"NumCenter=",numCenter}))
            {


            if(!quiet)
                printfln("Sweep=%d, HS=%d, Bond=%d/%d",sw,ha,b,(N-1));

            PH.numCenter(numCenter);
            PH.position(b,psi);
	auto indx1=findIndex(psi.A(b), "Site");
		  auto indx2=findIndex(psi.A(b+1), "Site");
            if(numCenter == 2)
	      { phi1 = psi(b)*psi(b+1);}
            else if(numCenter == 1)
	      {

phi1 = psi(b);
		
}
	


	    auto [max_solv,mean_solv]=applyExp_lbo(PH,phi1,t/2,indx1, indx2, b,H,args);

	    //	    std::cout<< "meth 2 "<<std::endl;
	    //	    std::cout<< "b " << b << "  b+1 = " << b+1 << "  max "<< max_lbo_solv << " and " << max_solv <<std::endl;
	    max_lbo_solv=std::max(max_solv,max_lbo_solv);
	    mean_lbo_solv=std::max(mean_solv,mean_lbo_solv);


	     
            if(args.getBool("DoNormalize",true))
                phi1 /= norm(phi1);

            if(numCenter == 2)
	      {
	

		 
  int old_llim=psi.leftLim();
  int old_rlim=psi.rightLim();
   		  auto [T1, T2]=applyLbo(phi1, indx1, indx2,b, args);
	       auto id1=findInds(T1, "OM");
	       auto id2=findInds(T2, "OM");
	       lbodims[b-1]=dim(id1);
	       lbodims[b]=dim(id2);
	       double new_dim(std::max(static_cast<double>(dim(id2)), static_cast<double>(dim(id1))));
	       max_lbo_svd=std::max(new_dim, max_lbo_svd);
   		    phi1*=T1;
  		      		  phi1*=T2;


 		     psi.Aref(b+1)*=T2;

  		    psi.set(b , psi.A(b)*T1);
		    psi.leftLim(old_llim);
  		    psi.rightLim(old_rlim);
		  	  spec = psi.svdBond(b,phi1,(ha==1 ? Fromleft : Fromright),PH,args);
				   
   old_llim=psi.leftLim();
   old_rlim=psi.rightLim();
   		  	phi1*=dag(T1);
			phi1*=dag(T2);



    		 psi.Aref(b+1)*=dag(T2);
    		 psi.Aref(b)*=dag(T1);
    		 		  psi.leftLim(old_llim);
   		  psi.rightLim(old_rlim);
	     
	      }
	    else if(numCenter == 1)
	      { psi.ref(b) = phi1; }

           // Calculate energy

            // ITensor PH_phi1;
	   
            // PH.product(phi1,PH_phi1);
            // energy = real(eltC(dag(phi1)*PH_phi1));
 
            if((ha == 1 && b+numCenter-1 != N) || (ha == 2 && b != 1))
                {
                auto b1 = (ha == 1 ? b+1 : b);
 
                if(numCenter == 2)
                    {
		     
                    phi0 = psi(b1);
		     
                    }
                else if(numCenter == 1)
                    {

		                    Index l;
                    if(ha == 1) l = commonIndex(psi(b),psi(b+1));
                    else        l = commonIndex(psi(b-1),psi(b));
                    ITensor U,S,V(l);
                    spec = svd(phi1,U,S,V,args);
                    psi.ref(b) = U;
                    phi0 = S*V;

                    }

                PH.numCenter(numCenter-1);
                PH.position(b1,psi);

	                applyExp(PH,phi0,-t/2,args);
	
                if(args.getBool("DoNormalize",true))
                    phi0 /= norm(phi0);
                
                if(numCenter == 2)
                    {
                    psi.ref(b1) = phi0;
                    }
                if(numCenter == 1)
                    {
                    if(ha == 1) psi.ref(b+1) *= phi0;
                    else        psi.ref(b-1) *= phi0;
                    }

                 // Calculate energy
                 ITensor PH_phi0;
                 PH.product(phi0,PH_phi0);
                 energy = real(eltC(dag(phi0)*PH_phi0));
                 }
            else
                {
                // Calculate energy
                ITensor H_phi1;
                PH.product(phi1,H_phi1);
                energy = real(eltC(dag(phi1)*H_phi1));
                }
 

             if(!quiet)
                 { 
                 printfln("    Truncated to Cutoff=%.1E, Min_dim=%d, Max_dim=%d",
                          sweeps.cutoff(sw),
                          sweeps.mindim(sw), 
                          sweeps.maxdim(sw) );
                 printfln("    Trunc. err=%.1E, States kept: %s",
                          spec.truncerr(),
                          showDim(linkIndex(psi,b)) );
                 }

    	     //obs.lastSpectrum(spec);

            args.add("AtBond",b);
            args.add("HalfSweep",ha);
            args.add("Energy",energy); 
            args.add("Truncerr",spec.truncerr()); 

    	    //            obs.measure(args);

	    } // for loop over b

        if(!silent)
            {
            auto sm = sw_time.sincemark();
            printfln("    Sweep %d/%d CPU time = %s (Wall time = %s)",
                     sw,sweeps.nsweep(),showtime(sm.time),showtime(sm.wall));
            }
        
    	//        if(obs.checkDone(args)) break;
  
	} //for loop over sw
  
psi.rightLim(psi.leftLim()+2);
    if(args.getBool("DoNormalize",true))
        psi.normalize();

    return energy;
    }
  template<>
  struct use_lbo<TDVP_lbo<MPS>>: std::true_type { 
  };
}
