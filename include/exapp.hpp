 #ifndef EXAPP_H
#define EXAPP_H
#include"itensor/all.h"
namespace itensor{
  template<typename T>
struct exApp{
  exApp(MPS& psi, T dt,
	 AutoMPO ampo,Args const& args = Args::global()): psi(psi), args(args)
  {
    auto taua = dt/2.*(1.+1._i);
      auto taub = dt/2.*(1.-1._i);
     expHb = toExpH(ampo, taub);
    expHa = toExpH(ampo,taua);
  }

  void new_ham(AutoMPO ampo, T dt)
  {
auto taua = dt/2.*(1.+1._i);
      auto taub = dt/2.*(1.-1._i);
     expHb = toExpH(ampo, taub);
    expHa = toExpH(ampo,taua);}
  MPS& psi;
  MPO expHa;
  MPO expHb;
   const Args& args;
  double dt;
  void apply();
};
  template<typename T>
  void exApp<T>::apply()
{
  //  Print(args);
  psi=applyMPO(expHa,psi, args);
   psi.noPrime();

   psi=applyMPO(expHb,psi, args);
   psi.noPrime();
   //.normalize();
 }
}
#endif /* EXAPP_H */
