 #ifndef KRYLOV_H
#define KRYLOV_H
#include "itensor/all.h"
#include "tridi.hpp"
#include<vector>

 

namespace itensor{
  double eps=1E-10;
  double cutoff=1E-13;
  int maxm=1000;
  // should I instead just save the norms and apply them every time?
template<typename OP, typename MPS>
MPS OrthoNorm(MPS& W, std::vector<MPS>& subSpace, OP& H)
{
 auto subSpace2=subSpace;

 std::for_each(subSpace2.begin(), subSpace2.end(), [W, H](MPS& p)
 {
   auto Value=innerC(W,    p);
       Value*=-1;
       p*=Value;
 });
 subSpace2.push_back(W);
auto Wprime=sum(subSpace2);

  if(norm(Wprime)<eps){std::cout<< "inv subspace "<< std::endl;}
  Wprime.normalize();
  return Wprime;
}
  template<typename OP, typename Container>
  auto ApplyAndOrthoNorm( Container& subSpace, OP& H)
    ->typename Container::value_type
  {
    auto W=subSpace[subSpace.size()-1];

    W=applyMPO(H, W, {"Maxm=",maxm,"Cutoff=",cutoff});
    W.noPrime();
    return OrthoNorm( W, subSpace,  H);
  }
  template<typename Container, typename Op >
  void updateTri(TriDiagMat<Real>& T, const Container& subSpace, const Op& H)
  {
    size_t dim=subSpace.size();
    //    std::cout<< dim << std::endl;
    T.addElement(real(innerC(subSpace[dim-1], H, subSpace[dim-1])), real(innerC(subSpace[dim-1],H,  subSpace[dim-2])));
  
  }
 
  template<typename  MPS, typename MPO, typename Type>
void krylov_timeStep(MPS& iniState, const MPO& H, Type dt)
{
  MPS v0=iniState;
  normalize(v0);
  int nos=12;
  std::vector<MPS> subSpace{v0};

  TriDiagMat<Real> T;
  double Norm=norm(iniState);
  T.addDiagElement(real(innerC(v0, H, v0)));
  for(int i=1; i<=nos; i++)
    {

       MPS v=ApplyAndOrthoNorm(subSpace, H);
       subSpace.push_back(v);

       updateTri(T, subSpace, H);
 
	 //	Cs.push_back(getCs(TE, dt));  
}
  //  std::cout<< "hei "<< std::endl;
          auto TE=T.make_tensor();
	auto mind=TE.inds()[1];
	auto mind2=TE.inds()[0];
	 auto NewM=expHermitian(TE, dt);
	 auto NEUET=ITensor(mind);
	 NEUET.set(mind(1), 1);
	 //std::cout<< NEUET.real(mind(2))<< std::endl;
	 auto MATT=NewM*NEUET;
	 v0*=norm(iniState);
	 iniState=subSpace[0]*MATT.cplx(mind2(1));
	
	   for(int i=1; i<subSpace.size()-1; i++)
	   {
	     iniState = sum(iniState, Norm*MATT.cplx(mind2(i+1))*subSpace[i], {"Maxm=",maxm,"Cutoff=",cutoff});	     
	   }
  
}
}
#endif /* KRYLOV_H */
