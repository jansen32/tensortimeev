#ifndef MAKETIMEEVOPHOLST_H
#define MAKETIMEEVOPHOLST_H
#include"holstein.hpp"
#include<algorithm>
#include"itensor/mps/sites/electron.h"
#include<map>
namespace itensor{



 template<typename Lattice, typename Iterator>
 struct holstGates{
   using Latticet=Lattice;
   holstGates(Lattice& sites, Iterator& gates, double tstep, std::map<std::string, double> param):sites(sites), 
N(itensor::length(sites)), gates(gates), tstep(tstep), t0(param["t0"]), omega(param["omega"]), gamma(param["gamma"]),  secLast(N-1){

     if(param.count("omegap")!=0)
       {
	 omegap= param["omegap"];
       }
     if(param.count("Const")!=0)
       {
	 Const= param["Const"];
       }
     makeGates();
};
   using Gate = BondGate;
   void makeGates();
    void makeGates_V(double);
   void makeFTGates(Gate::Type type=Gate::tImag, int sign=+1);
   void makeFTGatesAux();
   void makeFTGatesPar(Gate::Type type=Gate::tImag);
   void makeGates(std::vector<int>);
   void FTcomb(int b, ITensor& first, ITensor& sec, double dt);
   void FTcomb_noAux(int b, ITensor& First, double dt, Gate::Type type);
   Gate oneGate(int, double);
   Gate oneGate_V(int, double, double);
      Gate oneGateIm2(int, double);
      ITensor oneGateIm1(int);
   Lattice& sites;
  
   int N;
   Iterator& gates;
   double tstep{};
   double t0;
   double omega;
   double gamma;
   double omegap{0.0};
   
   // energy offset for FT calculations
   double Const{0};
   int secLast{0};
  
 };
 
template<typename Lattice, typename Iterator>
auto holstGates< Lattice, Iterator>::oneGate(int b, double dt)
  ->holstGates< Lattice, Iterator>::Gate
{

        auto hterm = -t0*sites.op("Cdag",b+1)*sites.op("C",b);
      hterm += -t0*sites.op("Cdag",b)*sites.op("C",b+1);

       hterm +=omegap*sites.op("Bdag",b+1)*sites.op("B",b);
      hterm += omegap*sites.op("Bdag",b)*sites.op("B",b+1);
  
      

      hterm += gamma*sites.op("NX",b)*sites.op("Id", b+1);
      hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+1));
     
      if(b==secLast)
      	{
      	  hterm += omega*sites.op("Id", b)*(sites.op("Nph",b+1));
      	  hterm += gamma*sites.op("Id", b)*sites.op("NX",b+1);

      	}

      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
}
  template<typename Lattice, typename Iterator>
  auto holstGates< Lattice, Iterator>::oneGate_V(int b, double dt, double V)
  ->holstGates< Lattice, Iterator>::Gate
{
 int L_x=(N+1)/2;  
 double E=V/(N+1);
 double loc_V=(b-L_x)*E;
        auto hterm = -t0*sites.op("Cdag",b+1)*sites.op("C",b);
      hterm += -t0*sites.op("Cdag",b)*sites.op("C",b+1);
  
      

      hterm += gamma*sites.op("NX",b)*sites.op("Id", b+1);
      hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+1));
      hterm += loc_V*(sites.op("n",b)*sites.op("Id", b+1));
            hterm += Const*(sites.op("n",b)*sites.op("Id", b+1));
     
      if(b==secLast)
      	{
	  double loc_V=(b+1-L_x)*E;
      	  hterm += omega*sites.op("Id", b)*(sites.op("Nph",b+1));
      	  hterm += gamma*sites.op("Id", b)*sites.op("NX",b+1);
	  hterm += loc_V*sites.op("Id", b)*sites.op("n",b+1);
	  hterm += Const*sites.op("Id", b)*sites.op("n",b+1);

      	}

      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
}
  template<typename Lattice, typename Iterator>
ITensor holstGates< Lattice, Iterator>::oneGateIm1(int b)
  
{

      
  auto hterm = -t0*sites.op("Cdag", b)*sites.op("C", b+2);
  hterm+=-t0*sites.op("Cdag", b+2)*sites.op("C", b);
  hterm +=omegap*sites.op("Bdag", b)*sites.op("B", b+2);
  hterm+=omegap*sites.op("Bdag", b+2)*sites.op("B", b);

  hterm += gamma*sites.op("NX",b)*sites.op("Id", b+2);
  hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+2));
  hterm+=-(2*Const/(N))*(sites.op("Id",b)*sites.op("Id", b+2));
      if(b==secLast)
      	{
      	  hterm += omega*sites.op("Id", b)*(sites.op("Nph",b+2));
      	  hterm += gamma*sites.op("Id", b)*sites.op("NX",b+2);

      	}


   return hterm;
}
  template<typename Lattice, typename Iterator>
auto holstGates< Lattice, Iterator>::oneGateIm2(int b, double dt)
  ->holstGates< Lattice, Iterator>::Gate
{

         auto hterm = -t0*sites.op("Cdag",b+1)*sites.op("C",b);
       hterm += -t0*sites.op("Cdag",b)*sites.op("C",b+1);
      auto g = Gate(sites,b,b+1,Gate::tImag,dt, hterm);
   return g;
}
  template<typename Lattice, typename Iterator>
void holstGates< Lattice, Iterator>::makeGates_V(double V)
{

    gates.clear();
 

   for( int b = 2; b <= N-1; b+=2)
           {
	    

	     gates.push_back(Gate(sites,b,b+1, oneGate_V(b,tstep/2, V)));
	        

    }
     Iterator even=gates;
           
     for(int b = 1; b <= N-1; b+=2)
    {
      	   
      gates.push_back(Gate(sites,b,b+1, oneGate_V(b,tstep, V)));

    }
gates.insert(gates.end(), even.begin(), even.end());
 

}
template<typename Lattice, typename Iterator>
void holstGates< Lattice, Iterator>::makeGates()
{
  // std::cout<< "gam "<< gamma << " omg "<< omega << " t0 " << t0 << std::endl;
    gates.clear();
   

  //  Print(sites);
    auto evenGates=oneGate(2,tstep/2);
    auto oddGates=oneGate(1,tstep);
      gates.push_back(Gate(sites,2,3, evenGates));
     // auto finGate1=oneGate(N-1,tstep/2);
     // auto finGate2=oneGate(N-1,tstep);
   for( int b = 4; b <= N-1; b+=2)
           {
	     if(b==N-1)
	       {
	
	        gates.push_back(Gate(sites,b,b+1, oneGate(b,tstep/2)));
	        }
	      else{
	       auto i1=sites(b);
	       auto i2=sites(b+1);
	       ITensor currentGate=evenGates;
	       currentGate*=itensor::delta((sites(2)),dag(i1));
	       currentGate*=itensor::delta((sites(3)),dag(i2));
	       
	       
	       currentGate*=itensor::delta(prime(dag(sites(2))),prime(i1));
	       currentGate*=itensor::delta(prime(dag(sites(3))),prime(i2));
	       gates.push_back(Gate(sites,b,b+1, currentGate));
	     }
    }
     Iterator even=gates;
           gates.push_back(Gate(sites,1,2, oddGates));
     for(int b = 3; b <= N-1; b+=2)
    {
      	     if(b==N-1)
	       {
		 gates.push_back(Gate(sites,b,b+1, oneGate(b,tstep)));
	       }
	     else{
   auto i1=sites(b);
	       auto i2=sites(b+1);
	       ITensor currentGate=oddGates;
	       currentGate*=itensor::delta((sites(1)),dag(i1));
	       currentGate*=itensor::delta((sites(2)),dag(i2));
	       currentGate*=itensor::delta(prime(dag(sites(1))),prime(i1));
	       currentGate*=itensor::delta(prime(dag(sites(2))),prime(i2));
	       gates.push_back(Gate(sites,b,b+1, currentGate));
	     }
    }
gates.insert(gates.end(), even.begin(), even.end());
 
//   std::cout<< gates.size()<< std::endl;
}
template<typename SiteSet>
ITensor makeFermSwapGate(SiteSet const& sites, int i1, int i2)
    {
    auto s1 = sites(i1);
    auto s2 = sites(i2);
    auto a = ITensor(dag(s1),prime(s2));
    auto b = ITensor(dag(s2),prime(s1));
    for(auto j : range1(s1))
        {
        a.set(dag(s1)(j),prime(s2)(j),1.);
        b.set(dag(s2)(j),prime(s1)(j),1.);
        }
    auto M=a*b;
    for(int i=(int(s1.dim()/2)+1); i<=s1.dim(); i++)
        {
          for(int j=(int(s1.dim()/2)+1); j<=s1.dim(); j++)
        {
	    for(auto k : range1(s1))
        {
	    for(auto l : range1(s1))
	      {
	      if(std::abs(elt(M, dag(s1)(i),prime(s2)(k), dag(s2)(j),prime(s1)(l)))>1E-15)
		{
	
	    M.set(dag(s1)(i),prime(s2)(k), dag(s2)(j),prime(s1)(l), -1.);
		}
	  
	}
	    
	}
	}
        }
    return M;
    }
// template<typename Lattice, typename Iterator>
// void holstGates< Lattice, Iterator>::makeFTGates(Gate::Type type, int sign)
// {
//   secLast=N-3;
//   // std::cout<< "gam "<< gamma << " omg "<< omega << " t0 " << t0 << std::endl;
//     gates.clear();
   
//   using Gate = BondGate;
//   //  Print(sites);
//   ITensor hh;
//    for( int b = 3; b <= N-2; b+=4)
//            {            
	    

// 	      hh=oneGateIm1(b);

// 	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
//              for(int n = 1; n <= sites.si(b).dim(); ++n)
//                  {
//                  II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
//                  }
//              hh *= II;

// 	     hh *= dag(prime(II));
// 	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));

// 	       gates.push_back(Gate(sites,b+1,b+2,type,sign*tstep/2.,hh));
// 	       gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
//     }
//      Iterator even=gates;
//      for(int b = 1; b <= N-2; b+=4)
//     {
      

// 	       hh=oneGateIm1(b);

// 	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
//              for(int n = 1; n <= sites.si(b).dim(); ++n)
//                  {
//                  II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
//                  }

//              hh *= II;
// 	     hh *= dag(prime(II));
// 	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
// 	       gates.push_back(Gate(sites,b+1,b+2,type,sign*tstep,hh));
// 	       gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
//     }

//      gates.insert(gates.end(), even.begin(), even.end()); 

// }
template<typename Lattice, typename Iterator>
void holstGates< Lattice, Iterator>::makeFTGates(Gate::Type type, int sign)
{
  secLast=N-3;
    gates.clear();
    ITensor evenGatesP=oneGateIm1(3);
    ITensor oddGatesP=oneGateIm1(1);

  FTcomb_noAux(3,evenGatesP,sign*tstep/2, type );
   for( int b = 7; b <= N-2; b+=4)
           {
	       
     if(b==N-3)
     	       {
		 ITensor gatesP=oneGateIm1(b);
		 FTcomb_noAux(b,gatesP,sign*tstep/2, type );
     	        }
	      else{
	       auto i1=sites(b);
	       auto i2=sites(b+2);
	       ITensor currentGateP=evenGatesP;
	       currentGateP*=itensor::delta((sites(3)),dag(i1));
	       currentGateP*=itensor::delta((sites(5)),dag(i2));
	       currentGateP*=itensor::delta(prime(dag(sites(3))),prime(i1));
	       currentGateP*=itensor::delta(prime(dag(sites(5))),prime(i2));
	       FTcomb_noAux(b,currentGateP,sign*tstep/2, type ); 
	}	    

 
    }
     Iterator even=gates;
     FTcomb_noAux(1,oddGatesP,sign*tstep, type );
      for(int b = 5; b <= N-2; b+=4)
     {
   secLast=N-3;
     if(b==N-3)
     	       {
	
		 ITensor gatesP=oneGateIm1(b);
		 FTcomb_noAux(b,gatesP,sign*tstep, type);
     	        }
	      else{
	       auto i1=sites(b);
	       auto i2=sites(b+2);

	       ITensor currentGateP=oddGatesP;
	       currentGateP*=itensor::delta((sites(1)),dag(i1));
	       currentGateP*=itensor::delta((sites(3)),dag(i2));
	       currentGateP*=itensor::delta(prime(dag(sites(1))),prime(i1));
	       currentGateP*=itensor::delta(prime(dag(sites(3))),prime(i2));
	       FTcomb_noAux(b,currentGateP, sign*tstep, type ); 
	}	    
	      
     }
     gates.insert(gates.end(), even.begin(), even.end());
     }
  template<typename Lattice, typename Iterator>
  void holstGates< Lattice, Iterator>::FTcomb(int b, ITensor& First, ITensor& Sec, double dt)
  {
   auto hh=First;

	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
             for(int n = 1; n <= sites.si(b).dim(); ++n)
                 {
                 II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
                 }

             hh *= II;

	     hh *= dag(prime(II));
	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));

	       gates.push_back(Gate(sites,b+1,b+2,Gate::tReal,dt,hh));
	       gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
	       // backwards time evolve auxillery site

	        hh=Sec;

	      II = ITensor(sites.si(b+1),dag(sites.si(b+2)));
             for(int n = 1; n <= sites.si(b+1).dim(); ++n)
                 {
                 II.set(sites.si(b+1)(n),sites.si(b+2)(n),1.0);
                 }
             hh *= II;

	     hh *= dag(prime(II));
	     gates.push_back(Gate(sites,b+1,b+2, makeFermSwapGate(sites, b+1, b+2)));
	       gates.push_back(Gate(sites,b+2,b+3,Gate::tReal,-dt,hh));
	       gates.push_back(Gate(sites,b+1,b+2, makeFermSwapGate(sites, b+1, b+2)));
  }
  template<typename Lattice, typename Iterator>
  void holstGates< Lattice, Iterator>::FTcomb_noAux(int b, ITensor& First, double dt, Gate::Type type)
  {
   auto hh=First;

	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
             for(int n = 1; n <= sites.si(b).dim(); ++n)
                 {
                 II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
                 }
             hh *= II;

	     hh *= dag(prime(II));
	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
	       gates.push_back(Gate(sites,b+1,b+2,type,dt,hh));
	       gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
  }
//  template<typename Lattice, typename Iterator>
// void holstGates< Lattice, Iterator>::makeFTGatesAux()
// {
//   // do I need to change the sign for fermionic swap?
//   // std::cout<< "gam "<< gamma << " omg "<< omega << " t0 " << t0 << std::endl;
//     gates.clear();

//   using Gate = BondGate;
//   //  Print(sites);
//   ITensor hh;
//    for( int b = 3; b <= N-2; b+=4)
//            {            
	    
//   secLast=N-3;
// 	      hh=oneGateIm1(b);

// 	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
//              for(int n = 1; n <= sites.si(b).dim(); ++n)
//                  {
//                  II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
//                  }
//              hh *= II;

// 	     hh *= dag(prime(II));
// 	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));

// 	       gates.push_back(Gate(sites,b+1,b+2,Gate::tReal,tstep/2.,hh));
// 	       gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
// 	       // backwards time evolve auxillery site
// 	         secLast=N-2;
// 	        hh=oneGateIm1(b+1);

// 	      II = ITensor(sites.si(b+1),dag(sites.si(b+2)));
//              for(int n = 1; n <= sites.si(b+1).dim(); ++n)
//                  {
//                  II.set(sites.si(b+1)(n),sites.si(b+2)(n),1.0);
//                  }
//              hh *= II;

// 	     hh *= dag(prime(II));
// 	     gates.push_back(Gate(sites,b+1,b+2, makeFermSwapGate(sites, b+1, b+2)));

// 	       gates.push_back(Gate(sites,b+2,b+3,Gate::tReal,-tstep/2.,hh));
// 	       gates.push_back(Gate(sites,b+1,b+2, makeFermSwapGate(sites, b+1, b+2)));
//     }
//      Iterator even=gates;
//      for(int b = 1; b <= N-2; b+=4)
//     {
      
//   secLast=N-3;
// 	       hh=oneGateIm1(b);

// 	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
//              for(int n = 1; n <= sites.si(b).dim(); ++n)
//                  {
//                  II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
//                  }

//              hh *= II;
// 	     hh *= dag(prime(II));
// 	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
// 	       gates.push_back(Gate(sites,b+1,b+2,Gate::tReal,tstep,hh));
// 	       gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
// 	       // backwards time evolve auxcillery site
// 	         secLast=N-2;
// 	             hh=oneGateIm1(b+1);

// 	     II = ITensor(sites.si(b+1),dag(sites.si(b+2)));
//              for(int n = 1; n <= sites.si(b).dim(); ++n)
//                  {
//                  II.set(sites.si(b+1)(n),sites.si(b+2)(n),1.0);
//                  }

//              hh *= II;
// 	     hh *= dag(prime(II));
// 	     gates.push_back(Gate(sites,b+1,b+2, makeFermSwapGate(sites, b+1, b+2)));
// 	       gates.push_back(Gate(sites,b+2,b+3,Gate::tReal,-tstep,hh));
// 	       gates.push_back(Gate(sites,b+1,b+2, makeFermSwapGate(sites, b+1, b+2)));
//     }

//      gates.insert(gates.end(), even.begin(), even.end()); 

// }
  template<typename Lattice, typename Iterator>
void holstGates< Lattice, Iterator>::makeFTGatesAux()
{
  // do I need to change the sign for fermionic swap?
  // std::cout<< "gam "<< gamma << " omg "<< omega << " t0 " << t0 << std::endl;
    gates.clear();
    ITensor evenGatesP=oneGateIm1(3);
    ITensor oddGatesP=oneGateIm1(1);
    ITensor evenGatesA=oneGateIm1(4);
    ITensor oddGatesA=oneGateIm1(2);

  ITensor hh;
   FTcomb(3,evenGatesP, evenGatesA,tstep/2 );
   for( int b = 7; b <= N-2; b+=4)
           {
	       
     if(b==N-3)
     	       {
		 secLast=N-3;
		 ITensor gatesP=oneGateIm1(N-3);
		 secLast=N-2;
		 ITensor gatesA=oneGateIm1(N-2);

		 FTcomb(b,gatesP, gatesA,tstep/2 );
     	        }
	      else{
	       auto i1=sites(b);
	       auto i2=sites(b+2);

	       ITensor currentGateP=evenGatesP;
	       //	       Print(currentGateP);
	       currentGateP*=itensor::delta((sites(3)),prime(i1));
	       currentGateP*=itensor::delta((sites(5)),prime(i2));
	       currentGateP*=itensor::delta(prime(dag(sites(3))),dag(i1));
	       currentGateP*=itensor::delta(prime(dag(sites(5))),dag(i2));
	       ITensor currentGateA=evenGatesA;
	         i1=sites(b+1);
	        i2=sites(b+3);
	       currentGateA*=itensor::delta((sites(4)),prime(i1));
	       currentGateA*=itensor::delta((sites(6)),prime(i2));
	       currentGateA*=itensor::delta(prime(dag(sites(4))),dag(i1));
	       currentGateA*=itensor::delta(prime(dag(sites(6))),dag(i2));
	       FTcomb(b,currentGateP, currentGateA,tstep/2 ); 
	}	    

 
    }
     Iterator even=gates;
        FTcomb(1,oddGatesP, oddGatesA,tstep );
      for(int b = 5; b <= N-2; b+=4)
     {
      
   secLast=N-3;
     if(b==N-3)
     	       {
	
		 secLast=N-3;
		 ITensor gatesP=oneGateIm1(N-3);
		 secLast=N-2;
		 ITensor gatesA=oneGateIm1(N-2);

		 FTcomb(b,gatesP, gatesA,tstep );
     	        }
	      else{
	       auto i1=sites(b);
	       auto i2=sites(b+2);

	       ITensor currentGateP=oddGatesP;
	       currentGateP*=itensor::delta((sites(1)),prime(i1));
	       currentGateP*=itensor::delta((sites(3)),prime(i2));
	       currentGateP*=itensor::delta(prime(dag(sites(1))),dag(i1));
	       currentGateP*=itensor::delta(prime(dag(sites(3))),dag(i2));
	       ITensor currentGateA=oddGatesA;
	         i1=sites(b+1);
	        i2=sites(b+3);
	       currentGateA*=itensor::delta((sites(2)),prime(i1));
	       currentGateA*=itensor::delta((sites(4)),prime(i2));
	       currentGateA*=itensor::delta(prime(dag(sites(2))),dag(i1));
	       currentGateA*=itensor::delta(prime(dag(sites(4))),dag(i2));
	       FTcomb(b,currentGateP, currentGateA,tstep ); 
	}	    
	      
     }

     gates.insert(gates.end(), even.begin(), even.end()); 

}
template<typename Lattice, typename Iterator>
void holstGates< Lattice, Iterator>::makeFTGatesPar(Gate::Type type)
{
  secLast=N-3;
  // std::cout<< "gam "<< gamma << " omg "<< omega << " t0 " << t0 << std::endl;
    gates.clear();
   
  using Gate = BondGate;
  //  Print(sites);
  ITensor hh;
   for( int b = 3; b <= N-2; b+=4)
           {
	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
    }
      for( int b = 3; b <= N-2; b+=4)
           {
	      hh=oneGateIm1(b);
	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
             for(int n = 1; n <= sites.si(b).dim(); ++n)
                 {
                 II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
                 }
             hh *= II;
	     hh *= dag(prime(II));
	     gates.push_back(Gate(sites,b+1,b+2,type,tstep/2.,hh));
    }
       for( int b = 3; b <= N-2; b+=4)
           {
	     gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));
    }
     Iterator even=gates;
     for(int b = 1; b <= N-2; b+=4)
    {
      gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));

    }
       for(int b = 1; b <= N-2; b+=4)
    {
     hh=oneGateIm1(b);
	      auto II = ITensor(sites.si(b),dag(sites.si(b+1)));
             for(int n = 1; n <= sites.si(b).dim(); ++n)
                 {
                 II.set(sites.si(b)(n),sites.si(b+1)(n),1.0);
                 }

             hh *= II;
	     hh *= dag(prime(II));
	     gates.push_back(Gate(sites,b+1,b+2,type,tstep,hh));

    }
         for(int b = 1; b <= N-2; b+=4)
    {
      gates.push_back(Gate(sites,b,b+1, makeFermSwapGate(sites, b, b+1)));

    }
     gates.insert(gates.end(), even.begin(), even.end());
     
     
}

  template<typename Lattice, typename Iterator>
  void holstGates< Lattice, Iterator>::makeGates(std::vector<int> inds)
{

  if(inds.empty()){return;}
  int s1=int((N-1)/2);
  int s2=int((N)/2);
  // std::cout<< "S1 "<< s1<< std::endl;
  // std::cout<< "S2 "<< s2<< std::endl;

  for(auto b: inds)
    {
            
      if(b==N){
  	if(N%2==0)
  	  {

  	    // Print(gates[s1+s2-1]);
	    // Print(oneGate(b-1, tstep));
  	    gates[s1+s2-1]=oneGate(b-1, tstep);
  	  }
  	else{
  	  gates[s1-1]=oneGate(b-1, tstep/2);
  	  gates[s1+s2+s1-1]=oneGate(b-1, tstep/2);
  	    //	  Print(gates[s1-1]);
  	    //	  Print(gates[s1+s2+s1-1]);
  	}
  	continue;
      }
         if(b==1){
	    // Print(gates[b]);
	    //  Print(oneGate(b, tstep));
  	   gates[s1]=oneGate(b, tstep);

  	    continue;}

      if(b%2==0)
  	{
	  
  	  gates[int(b/2)-1]=oneGate(b, tstep/2);
	  
  	    gates[s1+int(b/2)-1]=oneGate(b-1, tstep);
	  
  	    gates[s1+s2+int(b/2)-1]=oneGate(b, tstep/2);
  	  continue;
  	}
      else{

  	gates[int((b-1)/2)-1]=oneGate(b-1, tstep/2);

  	gates[s1+int((b-1)/2)]=oneGate(b, tstep);
  	gates[s1+s2+int((b-1)/2)-1]=oneGate(b-1, tstep/2);
  	continue;
      }
    }
}

  using IHG=holstGates<Holstein,std::vector<BondGate> >;

}

#endif /* MAKETIMEEVOPHOLST_H */
