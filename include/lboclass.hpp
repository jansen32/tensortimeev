#ifndef LBOCLASS_H
#define LBOCLASS_H
#include"lbo.hpp"
#include<vector>
#include"omp.h"
#include"extra.hpp"
#include<type_traits>
#include"timeevclass.hpp"
namespace itensor{
  static int up{0};
  // To Do make a more effienct implementaion of svd for tebd
  // and onbe trafo less for obl
// template<class Tensor>
// Spectrum 

  /*
    class defenition */

     using TensorPair=std::tuple<ITensor, ITensor>;
  template< typename GatesMaker, typename MPSType, bool FT>
  class TimeEvolveLbo : public TimeEvolve<MPSType, FT>
  {
    // number of sites
    using Parent=TimeEvolve<MPSType, FT>;
    

 
    
    
      using  Parent::args;

     using typename Parent::Gate;
    
//     using Lattice= typename GatesMaker::Latticet;public:
//     // TimeEvolveLbo(){};
  public:
    using Parent::gatelist;
    using Parent::L;
    using Parent::psi;
    using Parent::N;
     using Parent::PARA;
    using Parent::paraNormalize;
    TimeEvolveLbo( GatesMaker& GM, MPSType& state, int L,  Args const& argsMPS = Args::global(), Args& argsState = Args::global()) :TimeEvolve<MPSType, FT>(GM.gates, state, L, argsMPS), GM(GM), argsState(argsState),argsMPS(argsMPS){
      // auto lboCut = argsMPS.getReal("CutLBO",0.);
       maxPh = argsMPS.getInt("MaxPh",10);
       maxLbo = argsMPS.getInt("MaxLBO",10);
       phCut=argsMPS.getReal("CutPH",0.);
       maxBd=argsMPS.getReal("MaxDim",500);
       if(argsState.getBool("DiffMaxOcc", false))
	 {

	   phdim=argsState.getVecInt("MaxOccVec");
	 }
       else{
	 phdim=std::vector<int>(length(state), argsState.getInt("MaxOcc",1));
       }
       // if constexpr(FT)
       // 		     {
       // 		       for(size_t i=1; i<=N; i+=2)
       // 			 {

       // 			   physicalSites.push_back(i);
       // 			 }
       // 		     }
       // else{
       // 	    for(size_t i=1; i<=N; i+=1)
       // 	      {
       // 		physicalSites.push_back(i);
       // 	      }
       // 	      }
    };
    
          inline double avLboDim()
    {
      double sum{0};
      for(auto& l: physicalSites)
	{
	  auto id=findInds(trafTens[l], "OM");
	  sum+=dim(id);
	}
      return sum/physicalSites.size();
    }
             inline double avLboDimEven()
    {
      double sum{0};
      int m{0};
      for(int l=2; l<=N; l+=2)
	{	  auto id=findInds(trafTens[l], "OM");
	  sum+=dim(id);
	  m++;
	}
      return sum/m;
    }
                 inline double avLboDimOdd()
    {
      double sum{0};
      int m=0;
      for(int l=1; l<=N; l+=2)
	{	  auto id=findInds(trafTens[l], "OM");
	  sum+=dim(id);
	  m++;
	}
      return sum/m;
    }
       inline int maxLboDim()
    {
      int mx{0};

      for(auto& l: physicalSites)
	{
	  auto id=findInds(trafTens[l], "OM");
	  int oldmx(mx);
	  mx=std::max(static_cast<int>(dim(id)), oldmx);
	
	  
	}
      return mx;
    }
            inline int maxLboDimOdd()
    {
      int mx{0};

      for(int l=1; l<=N; l+=2)
	{
	  auto id=findInds(trafTens[l], "OM");
	  int oldmx(mx);
	  mx=std::max(static_cast<int>(dim(id)), oldmx);
	  
	}
      return mx;
    }
        inline int maxLboDimEven()
    {
      size_t mx{0};

      for(size_t l=2; l<=N; l+=2)
	{
	  auto id=findInds(trafTens[l], "OM");
	  size_t oldmx(mx);
	  mx=std::max(static_cast<size_t>(dim(id)), oldmx);
	  
	}
      return mx;
    }
        inline int maxPhNr()
    {
      return *std::max_element(phdim.begin(), phdim.end());
    }
    void lbotDmrgStep();

    void lboTEBDStepO2();
    void lboTEBDStepO2para();
    void lboTEBDStepO2paraStruct(size_t Llead1, size_t Lchain);
     void lboTEBDStepO2paraFrom(size_t);
    void updateLboTrafo(size_t mn, size_t mx, const TensorPair& newTraTens);
    void updateLboTrafoOneSite(int lboSite, const ITensor& newTraTens);
    MPS getBareMps()
    {
      
      MPS psi2;
      if constexpr (is_vidalNot<MPSType>::value){
	  psi2=psi.psi;
	}
      else{
	psi2=psi;
      }
      if(isLbo)
	{
      for(int i=1; i<=N; i++)
    	{
    	  psi2.setA(i, psi.A(i)*trafTens[i]);
    	}
      return psi2;
	}
      else{
	return psi2;
      }
    }
void makeBare()
    {
      
      
      
      if(isLbo)
	{
      for(size_t i=1; i<=N; i++)
    	{
    	  psi.Aref(i)*=trafTens[i];
    	}

	}
      else{

       
      }
    return;
    }
        MPS getBareMpsFrom(size_t Ll)
    {
      
      MPS psi2;
      if constexpr (is_vidalNot<MPSType>::value){
	  psi2=psi.psi;
	}
      else{
	psi2=psi;
      }
      if(isLbo)
	{
      for(size_t i=Ll+1; i<=N; i++)
    	{
    	  psi2.setA(i, psi.A(i)*trafTens[i]);
    	}
      return psi2;
	}
      else{
	return psi2;
      }
    }
    MPS getBareMpsStruct(size_t Llead1, size_t Lchain)
    {
      
      MPS psi2;
      if constexpr (is_vidalNot<MPSType>::value){
	  psi2=psi.psi;
	}
      else{
	psi2=psi;
      }
      if(isLbo)
	{
      
    	   for(size_t i=Llead1+1; i<=Llead1+Lchain; i++)
    	{
    	  psi2.setA(i, psi.A(i)*trafTens[i]);
    	}
    //   for(int i=0; i<=length(psi); i++)
    // {std::cout<< " i " << i << " and " << trafTens.count(i)<<std::endl;}
      return psi2;
	}
      else{
	return psi2;
      }
    }

//     int getMaxPhDloc(){return *std::max_element(phdim.begin(), phdim.end());}
//     Index omInd(const ITensor& T){return findIndex(T, {"OM"});}
 
     std::vector<int> phdim; 
 
//   private:
     GatesMaker& GM;

    void update(std::vector<int>);
 
    int maxPh{};
 int maxBd{};
    int maxLbo{};
    double phCut{};
    double omCut;
    //    const Args& argsMPS;
     Args& argsState;
    const Args& argsMPS;
     void makeLbo();
    void makeLboStruct(size_t Llead1, size_t Lchain);
          void makeLboFrom(size_t);
    
    
    inline void lboAct(const Gate& gate);
    inline void lboActStruct(const Gate& gate, size_t Llead1, size_t Lchain);
        inline void lboActFrom(const Gate& gate, size_t Ll);
    inline void lboInsert(int, int);
    void lbotDmrgStepStruct(size_t Lleads1, size_t Lchain);
    std::map<int, ITensor> trafTens;
    bool isLbo=false;
    std::vector<size_t> physicalSites;
    void testDim(int);

    //void* makeTimeEv;
  };
  /*
Member function
   */
    
 
      template< typename GatesMaker, typename MPSType, bool FT>
      void TimeEvolveLbo< GatesMaker, MPSType,FT>::updateLboTrafo(size_t mn, size_t mx, const TensorPair& newTraTens)
{
  if constexpr(FT)
    {

       trafTens[mn]=((dag(std::get<0>(newTraTens))));
    trafTens[mx]=((dag(std::get<1>(newTraTens))));
    
    }
  else{
    trafTens[mn]=((dag(std::get<0>(newTraTens))));
    trafTens[mx]=((dag(std::get<1>(newTraTens))));
  }
  return;
  
 }
   template< typename GatesMaker, typename MPSType, bool FT>
      void TimeEvolveLbo< GatesMaker, MPSType,FT>::updateLboTrafoOneSite(int lboSite,  const ITensor& newTraTens)
{
     trafTens[lboSite]=((dag((newTraTens))));

  return;
  
 }
 
 template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo< GatesMaker, MPSType,  FT>::makeLbo()
{
  //Print(psi);
  if(trafTens.size())
    {
      trafTens.erase(trafTens.begin(), trafTens.end());
      physicalSites.erase(physicalSites.begin(), physicalSites.end());
    }
  for(size_t i=1; i<=N-1; i+=2 )
    {

       if constexpr(!is_vidalNot<MPSType>::value)
       		    {
		     
		      psi.position(i);
	    }
      auto phibar = psi.A(i)*psi.A(i+1);
      auto phi=phibar;
      // if constexpr(is_vidalNot<MPSType>::value)
      // 		    {

      // 		       		       if(i!=N-1)
      // 					 {
      // 					   phi*=psi.Ls[i+1];
      // 					 }
				       
      // 		    }
      
      auto indx1=findIndex(psi.A(i), "Site");
      auto indx2=findIndex(psi.A(i+1), "Site");
 
      auto [T1, T2]=applyLbo(phi, indx1, indx2, i, argsMPS);
       trafTens.insert({i, dag(T1)});
       trafTens.insert({i+1, dag(T2)});

       psi.setA(i,psi.A(i)*(T1) );
         psi.setA(i+1,psi.A(i+1)*(T2) );
       
    }
  //  Print(psi);
  if(N%2!=0)
    {

      psi.setA(N-1,psi.A(N-1)*(trafTens[N-1]));
      auto phibar = psi.A(N-1)*psi.A(N);
      auto phi=phibar;
      auto indx1=findIndex(psi.A(N-1), "Site");
      auto indx2=findIndex(psi.A(N), "Site");
      auto newTraTens=applyLbo(phi, indx1, indx2, N-1, argsMPS);
      trafTens[N-1]=(dag(std::get<0>(newTraTens)));
      trafTens.insert({N, dag(std::get<1>(newTraTens))});
      psi.setA(N-1,psi.A(N-1)*std::get<0>(newTraTens) );
      psi.setA(N,psi.A(N)*std::get<1>(newTraTens) );
    }
  for(size_t i=1; i<=N; i+=1 )
    {
	physicalSites.push_back(i);
    }
     if constexpr(!is_vidalNot<MPSType>::value)
       		    {
		     
		      psi.position(1);
	    }
     isLbo=true;

 }
   template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo< GatesMaker, MPSType,  FT>::makeLboFrom(size_t Ll)
{
  //Print(psi);
  if(trafTens.size())
    {
      trafTens.erase(trafTens.begin(), trafTens.end());
      physicalSites.erase(physicalSites.begin(), physicalSites.end());
    }

  for(size_t i=Ll+1; i<=N-1; i+=2 )
    {
       if constexpr(!is_vidalNot<MPSType>::value)
       		    {
		     
		      psi.position(i);
	    }
      auto phibar = psi.A(i)*psi.A(i+1);
      auto phi=phibar;
      // if constexpr(is_vidalNot<MPSType>::value)
      // 		    {

      // 		       		       if(i!=N-1)
      // 					 {
      // 					   phi*=psi.Ls[i+1];
      // 					 }
				       
      // 		    }
      
      auto indx1=findIndex(psi.A(i), "Site");
      auto indx2=findIndex(psi.A(i+1), "Site");
 
      auto [T1, T2]=applyLbo(phi, indx1, indx2, i, argsMPS);
       trafTens.insert({i, dag(T1)});
       trafTens.insert({i+1, dag(T2)});

       psi.setA(i,psi.A(i)*(T1) );
         psi.setA(i+1,psi.A(i+1)*(T2) );
       
    }

  //  Print(psi);
  // here we using Ll=Llead so this code will only work if both are equally long
  if((Ll)%2!=0)
    {

      psi.setA(N-1,psi.A(N-1)*(trafTens[N-1]));
      auto phibar = psi.A(N-1)*psi.A(N);
      auto phi=phibar;
      auto indx1=findIndex(psi.A(N-1), "Site");
      auto indx2=findIndex(psi.A(N), "Site");
      auto newTraTens=applyLbo(phi, indx1, indx2, N-1, argsMPS);
      trafTens[N-1]=(dag(std::get<0>(newTraTens)));
      trafTens.insert({N, dag(std::get<1>(newTraTens))});
      psi.setA(N-1,psi.A(N-1)*std::get<0>(newTraTens) );
      psi.setA(N,psi.A(N)*std::get<1>(newTraTens) );
    }
  for(size_t i=Ll+1; i<=N; i++)
    {
	physicalSites.push_back(i);
    }

     isLbo=true;

 }
  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo< GatesMaker, MPSType,  FT>::makeLboStruct(size_t Llead1, size_t Lchain)
{
  //Print(psi);
 
   

     //Print(psi);
  if(trafTens.size())
    {

      trafTens.erase(trafTens.begin(), trafTens.end());
      physicalSites.erase(physicalSites.begin(), physicalSites.end());
    }
  //single site case
if (Lchain==1)
    {

      std::cout<< " is single site!!! "<<std::endl;     
      auto phibar = psi.A(Llead1+Lchain-1)*psi.A(Llead1+Lchain);
      auto phi=phibar;
     // if constexpr(is_vidalNot<MPSType>::value)
     //  		    {

     // 		       		       if(Llead1+Lchain-1!=N-1)
     // 					 {
     // 					   phi*=psi.Ls[Llead1+Lchain];
     // 					 }
				       
     // 		    }
      auto indx1=findIndex(psi.A(Llead1+Lchain-1), "Site");
      auto indx2=findIndex(psi.A(Llead1+Lchain), "Site");
      auto newTraTens=applyLbo(phi, indx1, indx2, Llead1+Lchain-1, argsMPS);
      
      trafTens.insert({Llead1+Lchain , dag(std::get<1>(newTraTens))});
      std::cout<<"Insert "<< trafTens[Llead1+Lchain ]<<std::endl;
      psi.setA(Llead1+Lchain,psi.A(Llead1+Lchain)*std::get<1>(newTraTens) );
    }
 else{
   // general case
  for(size_t i=Llead1+1; i<=Llead1+Lchain-1; i+=2 )
    {

       if constexpr(!is_vidalNot<MPSType>::value)
       		    {
		     
		      psi.position(i);
	    }
      auto phibar = psi.A(i)*psi.A(i+1);
      auto phi=phibar;
      // if constexpr(is_vidalNot<MPSType>::value)
      // 		    {

      // 		       		       if(i!=N-1)
      // 					 {
      // 					   phi*=psi.Ls[i+1];
      // 					 }
				       
      // 		    }
      
      auto indx1=findIndex(psi.A(i), "Site");
      auto indx2=findIndex(psi.A(i+1), "Site");
 
      auto [T1, T2]=applyLbo(phi, indx1, indx2, i, argsMPS);
       trafTens.insert({i, dag(T1)});
       trafTens.insert({i+1, dag(T2)});

       psi.setA(i,psi.A(i)*(T1) );
         psi.setA(i+1,psi.A(i+1)*(T2) );
       
    }
 if((Lchain)%2!=0 )
    {

      psi.setA(Llead1+Lchain-1,psi.A(Llead1+Lchain-1)*(trafTens[Llead1+Lchain-1]));
      auto phibar = psi.A(Llead1+Lchain-1)*psi.A(Llead1+Lchain);
      auto phi=phibar;
     // if constexpr(is_vidalNot<MPSType>::value)
     //  		    {

     // 		       		       if(Llead1+Lchain-1!=N-1)
     // 					 {
     // 					   phi*=psi.Ls[Llead1+Lchain];
     // 					 }
				       
     // 		    }
      auto indx1=findIndex(psi.A(Llead1+Lchain-1), "Site");
      auto indx2=findIndex(psi.A(Llead1+Lchain), "Site");
      auto newTraTens=applyLbo(phi, indx1, indx2, Llead1+Lchain-1, argsMPS);
      trafTens[Llead1+Lchain-1]=(dag(std::get<0>(newTraTens)));
      trafTens.insert({Llead1+Lchain, dag(std::get<1>(newTraTens))});
      psi.setA(Llead1+Lchain-1,psi.A(Llead1+Lchain-1)*std::get<0>(newTraTens) );
      psi.setA(Llead1+Lchain,psi.A(Llead1+Lchain)*std::get<1>(newTraTens) );
    }
 }
 for(size_t i=Llead1+1; i<=Llead1+Lchain; i++)
   {	physicalSites.push_back(i);}
 // for(int i=0; i<=length(psi); i++)
 //   {std::cout<< " i " << i << " and " << trafTens.count(i)<<std::endl;}
 isLbo=true;

 }
  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo<GatesMaker, MPSType,  FT>::lbotDmrgStep()
{
psi.position(gatelist.front().i1());
    const bool do_normalize = argsMPS.getBool("Normalize",true);
        auto g = gatelist.begin();
	    Real tot_norm = norm(psi);
        while(g != gatelist.end())
            {
              auto i1 = g->i1();
	      auto i2 = g->i2();
	      ITensor G=*g;

	      G*=(trafTens[i1]);
	      G*=(trafTens[i2]);
	      auto AA = psi.A(i1)*psi.A(i2);

	      AA*=G;

	      psi.Aref(i1)*=(trafTens[i1]);
	      psi.Aref(i2)*=(trafTens[i2]);
		//g->gate();
	      AA.replaceTags("Site,1", "Site,0");
	      size_t mn=std::min(i1, i2);
	      size_t mx=std::max(i1, i2);    
	      auto indx1=commonIndex(AA, trafTens[mn],  "Site");
	      auto indx2=commonIndex(AA, trafTens[mx],  "Site");
	      // Print(indx1);
	      // Print(indx2);
	      auto phi=AA;
	      auto newTraTens=applyLbo(phi, indx1, indx2,mn, argsMPS);
	      updateLboTrafo(mn, mx, newTraTens);


	      AA*=dag(trafTens[mn]);

	      AA*=dag(trafTens[mx]);

	      psi.Aref(mn)*=dag(trafTens[mn]);

	      psi.Aref(mx)*=dag(trafTens[mx]);


   
	      ++g;
	      //     //    Print(AA);
	      if(g != gatelist.end())
                {
		  //         Look ahead to next gate position
		  auto ni1 = g->i1();
		  auto ni2 = g->i2();
		  // SVD AA to restore MPS form
		  // before applying current gate
		  if(ni1 >= i2)
		    {

	      psi.svdBond(i1,AA,Fromleft,argsMPS);
		      psi.position(ni1); // does no work if position already ni1
                    }
		  else
                    {
		      psi.svdBond(i1,AA,Fromright,argsMPS);
		      psi.position(ni2); // does no work if position already ni2
                    }
                }
	      else
                {
		  // No next gate to analyze, just restore MPS form
		  psi.svdBond(i1,AA,Fromright,argsMPS);
                }
            }

        if(do_normalize)
            {
            tot_norm *= psi.normalize();
            }

    return;
}
  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo<GatesMaker, MPSType,  FT>::lbotDmrgStepStruct(size_t Lleads1, size_t Lchain)
{
psi.position(gatelist.front().i1());
    const bool do_normalize = argsMPS.getBool("Normalize",true);
        auto g = gatelist.begin();
	    Real tot_norm = norm(psi);
	   
        while(g != gatelist.end())
            {
 bool lbo_site=false;
              auto i1 = g->i1();
	      auto i2 = g->i2();

	      // transform half gate  into optimal basis
	      ITensor G=*g;
	   
auto AA = psi.A(i1)*psi.A(i2);

	      if(trafTens.count(i1)==1)
		{

	      G*=(trafTens[i1]);
	      psi.Aref(i1)*=(trafTens[i1]);
	      lbo_site=true;
		}
	      if(trafTens.count(i2)==1)
		{

	      G*=(trafTens[i2]);
 psi.Aref(i2)*=(trafTens[i2]);
	      lbo_site=true;
	    }
	      
	      // print(AA);
	      AA*=G;
	      //print(AA);   	      

	      AA.replaceTags("Site,1", "Site,0");
	      // if a phonon site is included, I go through lbo process
		      if(lbo_site)
			{

 	      size_t mn=std::min(i1, i2);
 	      size_t mx=std::max(i1, i2);    
	      auto indx1=Index();
	      auto indx2=Index();
	      if(trafTens.count(mn)==1)
		{
indx1=commonIndex(AA, trafTens[mn],  "Site");
		}
else{
indx1=commonIndex(AA, psi.A(mn),  "Site");
}
	      if(trafTens.count(mx)==1)
		{
indx2=commonIndex(AA, trafTens[mx],  "Site");
		}
else{
indx2=commonIndex(AA, psi.A(mx),  "Site");
}
	      auto phi=AA;

	      auto newTraTens=applyLbo(phi, indx1, indx2,mn, argsMPS);
	      if(trafTens.count(mn)==1)
		{	updateLboTrafoOneSite(mn, std::get<0>(newTraTens));
	      AA*=dag(trafTens[mn]);
	      psi.Aref(mn)*=dag(trafTens[mn]);
}	      if(trafTens.count(mx)==1)
		{	updateLboTrafoOneSite(mx, std::get<1>(newTraTens));
	      AA*=dag(trafTens[mx]);
	      psi.Aref(mx)*=dag(trafTens[mx]);
}

	    }

   
	      ++g;

	      //     //    Print(AA);
	      if(g != gatelist.end())
                {
		  //         Look ahead to next gate position
		  auto ni1 = g->i1();
		  auto ni2 = g->i2();
		  // SVD AA to restore MPS form
		  // before applying current gate
		  if(ni1 >= i2)
		    {

	      psi.svdBond(i1,AA,Fromleft,argsMPS);
		      psi.position(ni1); // does no work if position already ni1
                    }
		  else
                    {
		      psi.svdBond(i1,AA,Fromright,argsMPS);
		      psi.position(ni2); // does no work if position already ni2
                    }
                }
	      else
                {
		  // No next gate to analyze, just restore MPS form
		  psi.svdBond(i1,AA,Fromright,argsMPS);
                }
            }

        if(do_normalize)
            {
            tot_norm *= psi.normalize();
            }

    return;
}
     template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo<GatesMaker, MPSType,FT>::lboTEBDStepO2()
{

        auto g = gatelist.begin();
        while(g != gatelist.end())
            {
	      lboAct(*g);
	      g++;
	    }



    return;
}
       template< typename GatesMaker, typename MPSType, bool FT>
      inline void TimeEvolveLbo<GatesMaker, MPSType, FT>::lboAct(const Gate& gate)
       {

	 auto i1 = gate.i1();
	 auto i2 = gate.i2();
	 ITensor G=gate;
	 G*=(trafTens[i1]);
	 G*=(trafTens[i2]);

	 
	 auto AA = psi.A(i1)*psi.A(i2);
	 
 psi.Aref(i1)*=(trafTens[i1]);
	  psi.Aref(i2)*=(trafTens[i2]);
	    size_t mx=std::max(i1, i2);
    size_t mn=std::min(i1, i2);

    AA*=G;
    //    Print(AA);
	  AA.replaceTags("Site,1","Site,0");

      auto indx1=commonIndex(AA, trafTens[i1],  "Site");
      auto indx2=commonIndex(AA, trafTens[i2],  "Site");
      auto phi=AA;
      // if(mx!=N)
      // 	{
      // 	  phi*=psi.Ls[mx];

      // 	}
    auto newTraTens=applyLbo(phi, indx1, indx2, mn, argsMPS);
      updateLboTrafo(mn, mx, newTraTens);

    AA*=std::get<0>(newTraTens);
    AA*=std::get<1>(newTraTens);
    //    Print(psi.Aref(mn));
    psi.Aref(mn)*=std::get<0>(newTraTens);
    psi.Aref(mx)*=std::get<1>(newTraTens);
    //   Print(psi.Aref(mn));
    Parent::Insert(mn, mx, AA);
       }
 template< typename GatesMaker, typename MPSType, bool FT>
 inline void TimeEvolveLbo<GatesMaker, MPSType, FT>::lboActStruct(const Gate& gate, size_t Llead1, size_t Lchain)
       {
	
	 auto i1 = gate.i1();
	 auto i2 = gate.i2();
	 size_t mx=std::max(i1, i2);
	 size_t mn=std::min(i1, i2);
	 ITensor G=gate;
	 if(mx<=Llead1+Lchain and mn>=Llead1+1)
	   {	      	    
  lboAct(gate);
	   }
	 else if(mn>Llead1+Lchain or mx < Llead1+1)
	   { 
Parent::Act(gate);
}

	  else{
	    if(mn==Llead1+Lchain)
	      {
		
	
	 G*=(trafTens[mn]);
	      }
	    else{
		
	     G*=(trafTens[mx]);
	    
	    }
	    
	    
	 auto AA = psi.A(mx)*psi.A(mn);
    	    if(mn==Llead1 + Lchain)
	      {
psi.Aref(mn)*=(trafTens[mn]);
	      }
	    else{

	 psi.Aref(mx)*=(trafTens[mx]);
	    }
	 AA*=G;
	  
AA.replaceTags("Site,1","Site,0");
 Index indx1;
 Index indx2;
 
 
      if(mn==Llead1+Lchain)
	      {
		indx1=commonIndex(AA, trafTens[mn],  "Site");
		indx2=commonIndex(AA, psi.A(mx),  "Site");
	      }
	    else{
	 	indx2=commonIndex(AA, trafTens[mx],  "Site");
		indx1=commonIndex(AA, psi.A(mn),  "Site");
	    }
      auto phi=AA;
      // if(mx!=N)
      // 	{
      // 	  phi*=psi.Ls[mx];

      // 	}
    auto newTraTens=applyLbo(phi, indx1, indx2, mn, argsMPS);
     if(mn==Llead1+Lchain)
	      {
	updateLboTrafoOneSite(Llead1+Lchain, std::get<0>(newTraTens));
	      }
	    else{
	 	updateLboTrafoOneSite(Llead1+1, std::get<1>(newTraTens));
	    }
      

  

       if(mn==Llead1+Lchain)
	      {
		  AA*=std::get<0>(newTraTens);
    psi.Aref(mn)*=std::get<0>(newTraTens);
	      }
       else{
	     AA*=std::get<1>(newTraTens);
    psi.Aref(mx)*=std::get<1>(newTraTens);
       }
    Parent::Insert(mn, mx, AA);
        }
       }
template< typename GatesMaker, typename MPSType, bool FT>
 inline void TimeEvolveLbo<GatesMaker, MPSType, FT>::lboActFrom(const Gate& gate, size_t Ll)
       {
	
	 auto i1 = gate.i1();
	 auto i2 = gate.i2();
	 size_t mx=std::max(i1, i2);
	 size_t mn=std::min(i1, i2);
	  if(mx<=Ll)
	    {
	      Parent::Act(gate);
	    }
	  else if (mn>Ll)
	    {
	      	      lboAct(gate);
	    }
	    else{

	     
	

	    
	    
	 ITensor G=gate;
G*=trafTens[mx];

	 auto AA = psi.A(mx)*psi.A(mn);
	 psi.Aref(mx)*=trafTens[mx];
	 AA*=G;

AA.replaceTags("Site,1","Site,0");
 Index indx1;
 Index indx2;
	 	indx2=commonIndex(AA, trafTens[mx],  "Site");
		indx1=commonIndex(AA, psi.A(mn),  "Site");

      auto phi=AA;
      //  phi*=psi.Ls[mx];
    auto newTraTens=applyLbo(phi, indx1, indx2, mn, argsMPS);

	 	updateLboTrafoOneSite(mx, std::get<1>(newTraTens));

  


	     AA*=std::get<1>(newTraTens);
    psi.Aref(mx)*=std::get<1>(newTraTens);

    Parent::Insert(mn, mx, AA);
       }
       }
       template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo<GatesMaker, MPSType, FT>::lboTEBDStepO2para()
  {
        const bool do_normalize = argsMPS.getBool("Normalize",true);
    PARA=true;
    size_t mod=(L+1)%2;
    size_t one=gatelist.size()/(3);
        para apply([this](size_t i){lboAct(gatelist[i]);});
    apply.act_para(one, 2*one+mod, gatelist.size());
     if(do_normalize)
       {
	 paraNormalize();
      }
return;
  };
   
  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo<GatesMaker, MPSType, FT>::lboTEBDStepO2paraStruct(size_t Llead1, size_t Lchain)
  {
          const bool do_normalize = argsMPS.getBool("Normalize",true);
	    PARA=true;
    size_t mod=(L+1)%2;
    size_t one=gatelist.size()/(3);
    para apply([this, Llead1,Lchain](size_t i){lboActStruct(gatelist[i],Llead1, Lchain);});
    apply.act_para(one, 2*one+mod, gatelist.size());

     if(do_normalize)
       {
	 paraNormalize();
      }

    return;
  };

  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo<GatesMaker, MPSType, FT>::lboTEBDStepO2paraFrom(size_t Ll)
  {
            const bool do_normalize = argsMPS.getBool("Normalize",true);
	    PARA=true;
    size_t mod=(L+1)%2;
    size_t one=gatelist.size()/(3);
    para apply([this, Ll](size_t i){lboActFrom(gatelist[i],Ll);});
    apply.act_para(one, 2*one+mod, gatelist.size());

     if(do_normalize)
       {
	 paraNormalize();
      }

    return;
  };



  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo< GatesMaker,  MPSType, FT>::update(std::vector<int> updatevec)
  {

    
    for(auto l : updatevec)
      {

     		phdim[l-1]+=1;

     auto sitesNew=Holstein(GM.sites, l, phdim);
 auto newIn=sitesNew(l);
 auto oldIn=GM.sites(l);
       

      GM.sites =sitesNew;
  
      auto minD=std::min(oldIn.dim(), newIn.dim());
     auto oD=oldIn.dim();
     auto nD=newIn.dim();
     auto transform=ITensor(dag(oldIn),(newIn));
     int minQdim=int(minD/2);
     transform.set(1, 1, Cplx(1, 0));
     for(size_t i=1; i<=minQdim; i++)
       {
	 transform.set(i, i, Cplx(1, 0));
	 transform.set(int(oD/2)+i, int(nD/2)+i, Cplx(1, 0));
       }

     trafTens[l]*=transform;


        
       }
    
}
  template< typename GatesMaker, typename MPSType, bool FT>
  void TimeEvolveLbo< GatesMaker, MPSType,  FT>::testDim(int Dchange)
{
  //Print(psi);
  if(isLbo)
    {
      return;
    }
  for(size_t i=1; i<=N-1; i+=2 )
    {
       if constexpr(!is_vidalNot<MPSType>::value)
       		    {
		     
		      psi.position(i);
	    }
      auto phibar = psi.A(i)*psi.A(i+1);
      auto phi=phibar;
      if constexpr(is_vidalNot<MPSType>::value)
      		    {

		       		       if(i!=N-1)
					 {
					   phi*=psi.Ls[i+1];
					 }
				       
		    }
      
      auto indx1=findIndex(psi.A(i), "Site");
      auto indx2=findIndex(psi.A(i+1), "Site");
 
      auto [T1, T2]=applyLbo(phi, indx1, indx2, i, argsMPS);
      auto indx3=findIndex(T1, "OM");
      auto indx4=findIndex(T2, "OM");
      if(dim(indx3)<Dchange||dim(indx4)<Dchange)
	{
	  std::cout<< "make lbo "<< std::endl;
	  makeLbo();
	  return;
	}
   
       
    }
  //  Print(psi);
  if(N%2!=0)
    {

      psi.setA(N-1,psi.A(N-1)*(trafTens[N-1]));
      auto phibar = psi.A(N-1)*psi.A(N);
      auto phi=phibar;
      auto indx1=findIndex(psi.A(N-1), "Site");
      auto indx2=findIndex(psi.A(N), "Site");
     auto [T1, T2]=applyLbo(phi, indx1, indx2, N-1, argsMPS);
            auto indx3=findIndex(T1, "OM");
      auto indx4=findIndex(T2, "OM");
      if(dim(indx3)<Dchange||dim(indx4)<Dchange)
	{
	  std::cout<< "make lbo "<< std::endl;
	  makeLbo();
	  return;
	}
    }
     

 }
  
  //    template<>
  //    template< typename GatesMaker, typename MPSType, bool FT>
  // struct use_lbo<TimeEvolveLbo<GatesMaker,MPSType,FT >>: std::true_type {
   
  //  };
}
#endif /* LBO_H */
