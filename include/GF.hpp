
#ifndef SPECFUNC_H
#define SPECFUNC_H
#include"itensor/all.h"
#include"GFopt.hpp"
namespace itensor{
  // want to compute <c(t)_j c^{\dagger}_(L/2)>
  // idee is to generate
  // |phi>= exp(-iHt)c^{\dagger}_{L/2}|psi>
  // |\tilde{phi}>=c^{\dagger}_{L/2}exp(-iHt) |psi>
  //   and then compute <\tilde{phi}|phi>
  // I take two time evolvers, TE1 has exp(-iHt) and TE2 has exp(+iHt)
  // psi1 belongs to TE1 and psi2 to TE2

 //    template<typename TimeEvolver, typename SiteSet>
 //  void GFXDAGXt(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {
   
 //       applyX(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();
 // 	 psi1.position(1);
 // 	 psi2.position(1);
 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }

 // 	 auto Ne = toMPO(ne);
 // 	 auto psi1x=TE1.getBareMps();
 // 	 psi1x.position(1);
 // 	 auto NORM=itensor::norm(psi1x);
 // 	 std::cout<< " start norm "<< NORM<<'\n';

	
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 // 	  psi1x=TE1.getBareMps();
	  
 // 	  applyX(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);

	  
 // 	  std::complex<double> XDAGXtval=innerC(psi1x, psi2x);
 // 	  ///NORM;
 // 	  	std::vector<std::complex<double>> xdagxt;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    xdagxt.push_back(XDAGXtval);
 //       std::string DIR="XDAGXtGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(xdagxt, DIR+"XDAGXtGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeXDAGXtGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxXDAGXtGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdXDAGXtGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdXDAGXtGF"+filename, vecSize);
 //       //  Many_Body::ToFile(DIR+"sitesetXXDAGG"+filename,sites);
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1,0}<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>(n2*n2,0)<<'\n';
 // 	       std::cout<< n*dt<<" overlapXDAGXt "<< XDAGXtval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";

 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )  
 //   {
 // //     std::string DIR="XXDAGGF/";
 // // Many_Body::bin_write(DIR+"XXDAGGF"+filename,xxdag);
 // //          Many_Body::bin_write(DIR+"timeXXDAGGF"+filename, t);
 // //    Many_Body::bin_write(DIR+"lbomxXXDAGGF"+filename, lbomx);
 // //     Many_Body::bin_write(DIR+"avBdXXDAGGF"+filename, avBd);
 // //      Many_Body::bin_write(DIR+"maxBdXXDAGGF"+filename, maxBd);
 // //      itensor::writeToFile(DIR+"MPSXXDAGG"+filename,psi1x);
 // //      itensor::writeToFile(DIR+"sitesetXXDAGG"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 // n++;
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //        psi1x=TE1.getBareMps();
 // 	//Print(psi1);
 //    return;
 //   }

 //    template<typename TimeEvolver, typename SiteSet>
 //  void GFXDAGXnoNormt(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {
   
 //       applyX(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();
 // 	 psi1.position(1);
 // 	 psi2.position(1);
 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }

 // 	 auto Ne = toMPO(ne);
 // 	 auto psi1x=TE1.getBareMps();
 // 	 psi1x.position(1);



	
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 // 	  psi1x=TE1.getBareMps();
	  
 // 	  applyX(psi2x, j, sites);

 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);

	  
 // 	  std::complex<double> XDAGXnoNormtval=innerC(psi1x, psi2x);
 // 	  ///NORM;
 // 	  	std::vector<std::complex<double>> xdagxt;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    xdagxt.push_back(XDAGXnoNormtval);
 //       std::string DIR="XDAGXnoNormtGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(xdagxt, DIR+"XDAGXnoNormtGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeXDAGXnoNormtGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxXDAGXnoNormtGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdXDAGXnoNormtGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdXDAGXnoNormtGF"+filename, vecSize);
 //       //  Many_Body::ToFile(DIR+"sitesetXXDAGG"+filename,sites);
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
 // 	       std::cout<< n*dt<<" overlapXDAGXnoNormt "<< XDAGXnoNormtval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";

 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )  
 //   {
 // //     std::string DIR="XXDAGGF/";
 // // Many_Body::bin_write(DIR+"XXDAGGF"+filename,xxdag);
 // //          Many_Body::bin_write(DIR+"timeXXDAGGF"+filename, t);
 // //    Many_Body::bin_write(DIR+"lbomxXXDAGGF"+filename, lbomx);
 // //     Many_Body::bin_write(DIR+"avBdXXDAGGF"+filename, avBd);
 // //      Many_Body::bin_write(DIR+"maxBdXXDAGGF"+filename, maxBd);
 // //      itensor::writeToFile(DIR+"MPSXXDAGG"+filename,psi1x);
 // //      itensor::writeToFile(DIR+"sitesetXXDAGG"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 // n++;
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }

 // 	//Print(psi1);
 //    return;
 //   }
 //   template<typename TimeEvolver, typename SiteSet>
 //  void GFXXDAG(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {
   
 //       applyXdag(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();
 //      		      psi1.position(1);
 // 		      psi2.position(1);
 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }

 // 	 auto Ne = toMPO(ne);
 // 	 auto psi1x=TE1.getBareMps();
 // 	 psi1x.position(1);
 // 	 auto NORM=itensor::norm(psi1x);
 // 	 std::cout<< " start norm "<< NORM<<'\n';

 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 // 	  psi1x=TE1.getBareMps();
	  
 // 	  applyXdag(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);

	  
 // 	  std::complex<double> XXDAGval=innerC(psi2x, psi1x);
 // 	  ///NORM;
 // 	  	std::vector<std::complex<double>> xxdag;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    xxdag.push_back(XXDAGval);
 //       std::string DIR="XXDAGGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(xxdag, DIR+"XXDAGGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeXXDAGGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxXXDAGGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdXXDAGGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdXXDAGGF"+filename, vecSize);
 //       //  Many_Body::ToFile(DIR+"sitesetXXDAGG"+filename,sites);
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1,0}<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>(n2*n2,0)<<'\n';
 // 	       std::cout<< n*dt<<" overlapXXDAG "<< XXDAGval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";

 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )  
 //   {
 // //     std::string DIR="XXDAGGF/";
 // // Many_Body::bin_write(DIR+"XXDAGGF"+filename,xxdag);
 // //          Many_Body::bin_write(DIR+"timeXXDAGGF"+filename, t);
 // //    Many_Body::bin_write(DIR+"lbomxXXDAGGF"+filename, lbomx);
 // //     Many_Body::bin_write(DIR+"avBdXXDAGGF"+filename, avBd);
 // //      Many_Body::bin_write(DIR+"maxBdXXDAGGF"+filename, maxBd);
 // //      itensor::writeToFile(DIR+"MPSXXDAGG"+filename,psi1x);
 // //      itensor::writeToFile(DIR+"sitesetXXDAGG"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 //  n++;
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //        psi1x=TE1.getBareMps();
	
 //    return;
 //   }

   template<typename TimeEvolver, typename SiteSet>
  void GFXXDAGnoNorm(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
  {
   
       applyXdag(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();
      		      psi1.position(1);
		      psi2.position(1);
	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi1); j+=2)
    {

    ne+=1,"n",j;
    }

	 auto Ne = toMPO(ne);
	 auto psi1x=TE1.getBareMps();
	 psi1x.position(1);


       while(n*dt<tot)
      {

 	 auto psi2x=TE2.getBareMps();
	  psi1x=TE1.getBareMps();
	  
 	  applyXdag(psi2x, j, sites);

	  psi1x.position(1);
	  psi2x.position(1);
	  double n2=norm(psi2x);
	  double n1=norm(psi1x);

	  
 	  std::complex<double> XXDAGnoNormval=innerC(psi2x, psi1x);
	  ///NORM;
	  	std::vector<std::complex<double>> xxdag;
	std::vector<double> t;
	std::vector<double> avBd;
	std::vector<double> maxBd;
	std::vector<double> lbomx;
 	    t.push_back(double(n*dt));
	    avBd.push_back(averageLinkDim(psi1));
	    maxBd.push_back(maxLinkDim(psi1));
	    lbomx.push_back(TE1.maxLboDim());
 	    xxdag.push_back(XXDAGnoNormval);
       std::string DIR="XXDAGnoNormGF/";
       auto vecSize=t.size();
       Many_Body::ToFile(xxdag, DIR+"XXDAGnoNormGF"+filename,vecSize);
       Many_Body::ToFile(t, DIR+"timeXXDAGnoNormGF"+filename, vecSize);
       Many_Body::ToFile(lbomx, DIR+"lbomxXXDAGnoNormGF"+filename, vecSize);
       Many_Body::ToFile(avBd, DIR+"avBdXXDAGnoNormGF"+filename, vecSize);
       Many_Body::ToFile(maxBd, DIR+"maxBdXXDAGnoNormGF"+filename, vecSize);
       //  Many_Body::ToFile(DIR+"sitesetXXDAGnoNormG"+filename,sites);
	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
	       std::cout<< n*dt<<" overlapXXDAGnoNorm "<< XXDAGnoNormval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";

 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )  
 //   {
 // //     std::string DIR="XXDAGnoNormGF/";
 // // Many_Body::bin_write(DIR+"XXDAGnoNormGF"+filename,xxdag);
 // //          Many_Body::bin_write(DIR+"timeXXDAGnoNormGF"+filename, t);
 // //    Many_Body::bin_write(DIR+"lbomxXXDAGnoNormGF"+filename, lbomx);
 // //     Many_Body::bin_write(DIR+"avBdXXDAGnoNormGF"+filename, avBd);
 // //      Many_Body::bin_write(DIR+"maxBdXXDAGnoNormGF"+filename, maxBd);
 // //      itensor::writeToFile(DIR+"MPSXXDAGnoNormG"+filename,psi1x);
 // //      itensor::writeToFile(DIR+"sitesetXXDAGnoNormG"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
  n++;
 	TE1.lbotDmrgStep();
	TE2.lbotDmrgStep();
      }
        psi1x=TE1.getBareMps();
	
    return;
   }

 //  template<typename TimeEvolver, typename SiteSet>
 //  void GFCCDAG(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {

 //     applyCdag(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();

		     
 // 		      psi1.position(1);
 // 		      psi2.position(1);

 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }

 // auto Ne = toMPO(ne);
 //  auto psi1x=TE1.getBareMps();
 //  psi1x.position(1);
 //  auto NORM=itensor::norm(psi1x);
 //  std::cout<< " start norm "<< NORM<<'\n';
 //       while(n*dt<tot)
 //      {


 // 	 auto psi2x=TE2.getBareMps();
 // 	  psi1x=TE1.getBareMps();

 // 	  applyCdag(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);

	  
 // 	  std::complex<double> CCDAGval=innerC(psi2x, psi1x);
 // 	std::vector<std::complex<double>> ccdag;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    ccdag.push_back(CCDAGval);
 //       std::string DIR="CCDAGGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(ccdag, DIR+"CCDAGGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeCCDAGGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxCCDAGGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdCCDAGGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdCCDAGGF"+filename, vecSize);
	    	    
 // 	    ccdag.push_back(CCDAGval);
 // 	    // /NORM
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 // 	       std::cout<< n*dt<<" overlapCCDAG "<< CCDAGval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100)) or (TE2.maxLboDim()>=2*(M)-4) )
 //   {
 // //     std::string DIR="CCDAGGF/";
 // // Many_Body::bin_write(DIR+"CCDAGGF"+filename,ccdag);
 // //          Many_Body::bin_write(DIR+"timeCCDAGGF"+filename, t);
 // //    Many_Body::bin_write(DIR+"lbomxCCDAGGF"+filename, lbomx);
 // //     Many_Body::bin_write(DIR+"avBdCCDAGGF"+filename, avBd);
 // //      Many_Body::bin_write(DIR+"maxBdCCDAGGF"+filename, maxBd);
 // //      Print(psi1);
 // //            itensor::writeToFile(DIR+"MPSCCDAGGF"+filename,psi1x);
 // //      itensor::writeToFile(DIR+"sitesetCCDAGGF"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //  }

  
 //  template<typename TimeEvolver, typename SiteSet>
 //  void GFCCDAGnoNorm(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {

 //     applyCdag(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();

		     
 // 		      psi1.position(1);
 // 		      psi2.position(1);

 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }

 // auto Ne = toMPO(ne);
 //  auto psi1x=TE1.getBareMps();
 //  psi1x.position(1);

 //       while(n*dt<tot)
 //      {


 // 	 auto psi2x=TE2.getBareMps();
 // 	  psi1x=TE1.getBareMps();

 // 	  applyCdag(psi2x, j, sites);

 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);

	  
 // 	  std::complex<double> CCDAGnoNormval=innerC(psi2x, psi1x);
 // 	std::vector<std::complex<double>> ccdag;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    ccdag.push_back(CCDAGnoNormval);
 //       std::string DIR="CCDAGnoNormGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(ccdag, DIR+"CCDAGnoNormGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeCCDAGnoNormGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxCCDAGnoNormGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdCCDAGnoNormGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdCCDAGnoNormGF"+filename, vecSize);
	    	    
 // 	    ccdag.push_back(CCDAGnoNormval);
 // 	    // /NORM
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 // 	       std::cout<< n*dt<<" overlapCCDAGnoNorm "<< CCDAGnoNormval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100)) or (TE2.maxLboDim()>=2*(M)-4) )
 //   {
 // //     std::string DIR="CCDAGnoNormGF/";
 // // Many_Body::bin_write(DIR+"CCDAGnoNormGF"+filename,ccdag);
 // //          Many_Body::bin_write(DIR+"timeCCDAGnoNormGF"+filename, t);
 // //    Many_Body::bin_write(DIR+"lbomxCCDAGnoNormGF"+filename, lbomx);
 // //     Many_Body::bin_write(DIR+"avBdCCDAGnoNormGF"+filename, avBd);
 // //      Many_Body::bin_write(DIR+"maxBdCCDAGnoNormGF"+filename, maxBd);
 // //      Print(psi1);
 // //            itensor::writeToFile(DIR+"MPSCCDAGnoNormGF"+filename,psi1x);
 // //      itensor::writeToFile(DIR+"sitesetCCDAGGF"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //  }

  
 //  template<typename TimeEvolver, typename SiteSet>
 //  void GFBBDAG(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {

 //       applyBdag(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();
 // 	std::vector<std::complex<double>> bbdag;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	 auto ne = AutoMPO(sites);
 // 	   auto psi1x=TE1.getBareMps();
 // 	   psi1x.position(1);
 // 	   auto NORM=itensor::norm(psi1x);
 // 	   std::cout<< " start norm "<< NORM<<'\n';
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }
 // 		      psi1.position(1);
 // 		      psi2.position(1);
 // 	 auto Ne = toMPO(ne);
	 
 //       while(n*dt<tot)
 //      {



 // 	 auto psi2x=TE2.getBareMps();
 // 	 psi1x=TE1.getBareMps();

 // 	  applyBdag(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);
	  
 // 	  std::complex<double> BBDAGval=innerC(psi2x, psi1x);
	  
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	     	    maxBd.push_back(maxLinkDim(psi1));
 // 		    lbomx.push_back(TE1.maxLboDim());
 // 	    bbdag.push_back(BBDAGval);
 // 	    // /NORM
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 //  	    std::cout<< n*dt<<" overlapBBDAG "<< BBDAGval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if( (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))  or (TE2.maxLboDim()>=2*(M)-4) )
 //   {
 //     std::string DIR="BBDAGGF/";
     
 // Many_Body::bin_write(DIR+"BBDAGGF"+filename,bbdag);
 //          Many_Body::bin_write(DIR+"timeBBDAGGF"+filename, t);
 //    Many_Body::bin_write(DIR+"lbomxBBDAGGF"+filename, lbomx);
 //     Many_Body::bin_write(DIR+"avBdBBDAGGF"+filename, avBd);
 //      Many_Body::bin_write(DIR+"maxBdBBDAGGF"+filename, maxBd);
 //      Print(psi1);
 //            itensor::writeToFile(DIR+"MPSBBDAGGF"+filename,psi1x);
 //      itensor::writeToFile(DIR+"sitesetBBDAGGF"+filename,sites);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       Print(psi1);
 //       psi1x=TE1.getBareMps();
 //       std::string DIR="BBDAGGF/";
 //             itensor::writeToFile(DIR+"MPSBBDAGGF"+filename,psi1x);
 //      itensor::writeToFile(DIR+"sitesetBBDAGGF"+filename,sites);
 // Many_Body::bin_write(DIR+"BBDAGGF"+filename,bbdag);
 //          Many_Body::bin_write(DIR+"timeBBDAGGF"+filename, t);
 //    Many_Body::bin_write(DIR+"lbomxBBDAGGF"+filename, lbomx);
 //     Many_Body::bin_write(DIR+"avBdBBDAGGF"+filename, avBd);
 //      Many_Body::bin_write(DIR+"maxBdBBDAGGF"+filename, maxBd);
 //    return;
 //   }
 //  template<typename TimeEvolver, typename SiteSet>
 //  void GFBDAGB(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, int M)
 //  {
    
 //    applyBdag(psi1, i, sites);
 //    int n=0;
 //    TE1.makeLbo();
 //    TE2.makeLbo();

 // 	std::vector<std::complex<double>> bdagb;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }
 // 		      psi1.position(1);
 // 		      psi2.position(1);
 // 	 auto Ne = toMPO(ne);
 // 	   auto psi1x=TE1.getBareMps();
 // 	   psi1x.position(1);
 // 	   auto NORM=itensor::norm(psi1x);
 // 	   std::cout<< " start norm "<< NORM<<'\n';
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 // 	 psi1x=TE1.getBareMps();

 // 	  applyB(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);

	  
 // 	  std::complex<double> BDAGBval=innerC(psi2x, psi1x);
 // 	  ///NORM;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	     	    maxBd.push_back(maxLinkDim(psi1));
 // 		    lbomx.push_back(TE1.maxLboDim());
 // 	    bdagb.push_back(BDAGBval);
 // 	    // /NORM
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 //  	    std::cout<< n*dt<<" overlapBBDAG "<< BDAGBval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100)) or (TE2.maxLboDim()>=2*(M)-4) )
 //   {
 //     std::string DIR="BBDAGGF/";
 //     itensor::writeToFile(DIR+"MPSBDAGBGF"+filename,psi1x);
 //     itensor::writeToFile(DIR+"sitesetBDAGBGF"+filename,sites);
 // Many_Body::bin_write(DIR+"BBDAGGF"+filename,bdagb);
 //          Many_Body::bin_write(DIR+"timeBDAGBGF"+filename, t);
 //    Many_Body::bin_write(DIR+"lbomxBDAGBGF"+filename, lbomx);
 //     Many_Body::bin_write(DIR+"avBdBDAGBGF"+filename, avBd);
 //      Many_Body::bin_write(DIR+"maxBdBDAGBGF"+filename, maxBd);
 //      Print(psi1);
 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       Print(psi1);
 //       psi1x=TE1.getBareMps();
 //       std::string DIR="BDAGBGF/";
 //            itensor::writeToFile(DIR+"MPSBDAGBGF"+filename,psi1x);
 //     itensor::writeToFile(DIR+"sitesetBDAGBGF"+filename,sites);
 // Many_Body::bin_write(DIR+"BDAGBGF"+filename,bdagb);
 //          Many_Body::bin_write(DIR+"timeBDAGBGF"+filename, t);
 //    Many_Body::bin_write(DIR+"lbomxBDAGBGF"+filename, lbomx);
 //     Many_Body::bin_write(DIR+"avBdBDAGBGF"+filename, avBd);
 //      Many_Body::bin_write(DIR+"maxBdBDAGBGF"+filename, maxBd);
 //    return;
 //   }
 //    template<typename TimeEvolver, typename SiteSet>
 //    void GFCDAGC(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
 //  {
 //    	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }
 // 	 auto Ne = toMPO(ne);
 //    std::cout<< " f1 "<< innerC(psi1,Ne, psi1)/innerC(psi1, psi1)<<'\n';
 //       applyC(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();


 // 		      psi1.position(1);
 // 		      psi2.position(1);

 // 	  auto psi1x=TE1.getBareMps();
 // 	  psi1x.position(1);
 // 	  auto NORM=itensor::norm(psi1x);
 // 	  std::cout<< " start norm "<< NORM<<'\n';
 // 	  std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{NORM*NORM, 0}<<'\n';
	 
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 	 
 // 	  psi1x=TE1.getBareMps();
 // 	  applyC(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);
 // 	  std::complex<double> CDAGCval=innerC(psi2x, psi1x);
 // 	std::vector<std::complex<double>> cdagc;	
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    cdagc.push_back(CDAGCval);
 //       std::string DIR="CDAGCGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(cdagc, DIR+"CDAGCGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeCDAGCGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxCDAGCGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdCDAGCGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdCDAGCGF"+filename, vecSize);
	    	    
	    	    
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 //  	    std::cout<< n*dt<<" overlapCDAGC "<< CDAGCval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       Print(psi1);
 //       psi1x=TE1.getBareMps();
 
 //    return;
 //   }
 //      template<typename TimeEvolver, typename SiteSet>
 //    void GFCDAGCNorm(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
 //  {
 //    	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }
 // 	 auto Ne = toMPO(ne);
 //    std::cout<< " f1 "<< innerC(psi1,Ne, psi1)/innerC(psi1, psi1)<<'\n';
 //       applyC(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();


 // 		      psi1.position(1);
 // 		      psi2.position(1);

 // 	  auto psi1x=TE1.getBareMps();
 // 	  psi1x.position(1);
 // 	  auto NORM=itensor::norm(psi1x);
 // 	  std::cout<< " start norm "<< NORM<<'\n';
 // 	  std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{NORM*NORM, 0}<<'\n';
	 
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 	 
 // 	  psi1x=TE1.getBareMps();
 // 	  applyC(psi2x, j, sites);
 // 	  //psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  	  psi1x.normalize();
 // 		  psi2x.normalize();
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);
 // 	  std::complex<double> CDAGCNormval=innerC(psi2x, psi1x);
 // 	std::vector<std::complex<double>> cdagc;	
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    cdagc.push_back(CDAGCNormval);
 //       std::string DIR="CDAGCNormGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(cdagc, DIR+"CDAGCNormGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeCDAGCNormGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxCDAGCNormGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdCDAGCNormGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdCDAGCNormGF"+filename, vecSize);
	    	    
	    	    
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
 //  	    std::cout<< n*dt<<" overlapCDAGCNorm "<< CDAGCNormval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       Print(psi1);
 //       psi1x=TE1.getBareMps();
 
 //    return;
 //   }

        template<typename TimeEvolver, typename SiteSet>
    void GFCDAGCnoNorm(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
  {
    	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi1); j+=2)
    {

    ne+=1,"n",j;
    }
	 auto Ne = toMPO(ne);
    std::cout<< " f1 "<< innerC(psi1,Ne, psi1)/innerC(psi1, psi1)<<'\n';
       applyC(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();


		      psi1.position(1);
		      psi2.position(1);

	  auto psi1x=TE1.getBareMps();
	  psi1x.position(1);
	  auto NORM=itensor::norm(psi1x);
	  std::cout<< " start norm "<< NORM<<'\n';
	  std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{NORM*NORM, 0}<<'\n';
	 
       while(n*dt<tot)
      {

 	 auto psi2x=TE2.getBareMps();
 	 
 	  psi1x=TE1.getBareMps();
	  applyC(psi2x, j, sites);
	  //psi1x*=NORM; 
	  psi1x.position(1);
	  psi2x.position(1);


	  double n2=norm(psi2x);
	  double n1=norm(psi1x);
 	  std::complex<double> CDAGCnoNormval=innerC(psi2x, psi1x);
	std::vector<std::complex<double>> cdagc;	
	std::vector<double> t;
	std::vector<double> avBd;
	std::vector<double> maxBd;
	std::vector<double> lbomx;
 	    t.push_back(double(n*dt));
	    avBd.push_back(averageLinkDim(psi1));
	    maxBd.push_back(maxLinkDim(psi1));
	    lbomx.push_back(TE1.maxLboDim());
 	    cdagc.push_back(CDAGCnoNormval);
       std::string DIR="CDAGCnoNormGF/";
       auto vecSize=t.size();
       Many_Body::ToFile(cdagc, DIR+"CDAGCnoNormGF"+filename,vecSize);
       Many_Body::ToFile(t, DIR+"timeCDAGCnoNormGF"+filename, vecSize);
       Many_Body::ToFile(lbomx, DIR+"lbomxCDAGCnoNormGF"+filename, vecSize);
       Many_Body::ToFile(avBd, DIR+"avBdCDAGCnoNormGF"+filename, vecSize);
       Many_Body::ToFile(maxBd, DIR+"maxBdCDAGCnoNormGF"+filename, vecSize);
	    	    
	    	    
	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
  	    std::cout<< n*dt<<" overlapCDAGCnoNorm "<< CDAGCnoNormval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" noNorm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" noNorm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 	TE1.lbotDmrgStep();
	TE2.lbotDmrgStep();
      }
       Print(psi1);
       psi1x=TE1.getBareMps();
 
    return;
   }

 //    template<typename TimeEvolver, typename SiteSet>
 //    void GFCCDAGt(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
 //  {
 //    	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }
 // 	 auto Ne = toMPO(ne);
 //    std::cout<< " f1 "<< innerC(psi1,Ne, psi1)/innerC(psi1, psi1)<<'\n';
 //       applyCdag(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();


 // 		      psi1.position(1);
 // 		      psi2.position(1);

 // 	  auto psi1x=TE1.getBareMps();
 // 	  psi1x.position(1);
 // 	  auto NORM=itensor::norm(psi1x);
 // 	  std::cout<< " start norm "<< NORM<<'\n';
 // 	  std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{NORM*NORM, 0}<<'\n';
	 
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 	 
 // 	  psi1x=TE1.getBareMps();
 // 	  applyCdag(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);
 // 	  std::complex<double> CCDAGtval=innerC(psi1x, psi2x);
 // 	std::vector<std::complex<double>> ccdagt;	
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    ccdagt.push_back(CCDAGtval);
 //       std::string DIR="CCDAGtGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(ccdagt, DIR+"CCDAGtGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeCCDAGtGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxCCDAGtGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdCCDAGtGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdCCDAGtGF"+filename, vecSize);
	    	    
	    	    
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 //  	    std::cout<< n*dt<<" overlapCCDAGt "<< CCDAGtval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" noNorm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" noNorm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       // Print(psi1);
 //       //psi1x=TE1.getBareMps();
 
 //    return;
 //   }
  
 //      template<typename TimeEvolver, typename SiteSet>
 //    void GFCCDAGNormt(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
 //  {
 //    	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }
 // 	 auto Ne = toMPO(ne);
 //    std::cout<< " f1 "<< innerC(psi1,Ne, psi1)/innerC(psi1, psi1)<<'\n';
 //       applyCdag(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();


 // 		      psi1.position(1);
 // 		      psi2.position(1);

 // 	  auto psi1x=TE1.getBareMps();
 // 	  psi1x.position(1);
 // 	  auto NORM=itensor::norm(psi1x);
 // 	  std::cout<< " start norm "<< NORM<<'\n';
 // 	  std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{NORM*NORM, 0}<<'\n';
	 
 //       while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps();
 	 
 // 	  psi1x=TE1.getBareMps();
 // 	  applyCdag(psi2x, j, sites);
 // 	  //psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  psi1x.normalize();
 // 	  psi2x.normalize();
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);
 // 	  std::complex<double> CCDAGNormtval=innerC(psi1x, psi2x);
 // 	std::vector<std::complex<double>> ccdagt;	
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	    maxBd.push_back(maxLinkDim(psi1));
 // 	    lbomx.push_back(TE1.maxLboDim());
 // 	    ccdagt.push_back(CCDAGNormtval);
 //       std::string DIR="CCDAGNormtGF/";
 //       auto vecSize=t.size();
 //       Many_Body::ToFile(ccdagt, DIR+"CCDAGNormtGF"+filename,vecSize);
 //       Many_Body::ToFile(t, DIR+"timeCCDAGNormtGF"+filename, vecSize);
 //       Many_Body::ToFile(lbomx, DIR+"lbomxCCDAGNormtGF"+filename, vecSize);
 //       Many_Body::ToFile(avBd, DIR+"avBdCCDAGNormtGF"+filename, vecSize);
 //       Many_Body::ToFile(maxBd, DIR+"maxBdCCDAGNormtGF"+filename, vecSize);
	    	    
	    	    
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 //  	    std::cout<< n*dt<<" overlapCCDAGNormt "<< CCDAGNormtval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 // 	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       // Print(psi1);
 //       //psi1x=TE1.getBareMps();
 
 //    return;
 //   }
        template<typename TimeEvolver, typename SiteSet>
    void GFCCDAGnoNormt(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
  {
    	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi1); j+=2)
    {

    ne+=1,"n",j;
    }
	 auto Ne = toMPO(ne);
    std::cout<< " f1 "<< innerC(psi1,Ne, psi1)/innerC(psi1, psi1)<<'\n';
       applyCdag(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();


		      psi1.position(1);
		      psi2.position(1);

	  auto psi1x=TE1.getBareMps();
	  psi1x.position(1);
	  auto NORM=itensor::norm(psi1x);
	  std::cout<< " start norm "<< NORM<<'\n';
	  std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{NORM*NORM, 0}<<'\n';
	 
       while(n*dt<tot)
      {

 	 auto psi2x=TE2.getBareMps();
 	 
 	  psi1x=TE1.getBareMps();
	  applyCdag(psi2x, j, sites);
	  //psi1x*=NORM; 
	  psi1x.position(1);
	  psi2x.position(1);
	  //	  psi1x.normalize();
	  //psi2x.normalize();
	  double n2=norm(psi2x);
	  double n1=norm(psi1x);
 	  std::complex<double> CCDAGnoNormtval=innerC(psi1x, psi2x);
	std::vector<std::complex<double>> ccdagt;	
	std::vector<double> t;
	std::vector<double> avBd;
	std::vector<double> maxBd;
	std::vector<double> lbomx;
 	    t.push_back(double(n*dt));
	    avBd.push_back(averageLinkDim(psi1));
	    maxBd.push_back(maxLinkDim(psi1));
	    lbomx.push_back(TE1.maxLboDim());
 	    ccdagt.push_back(CCDAGnoNormtval);
       std::string DIR="CCDAGnoNormtGF/";
       auto vecSize=t.size();
       Many_Body::ToFile(ccdagt, DIR+"CCDAGnoNormtGF"+filename,vecSize);
       Many_Body::ToFile(t, DIR+"timeCCDAGnoNormtGF"+filename, vecSize);
       Many_Body::ToFile(lbomx, DIR+"lbomxCCDAGnoNormtGF"+filename, vecSize);
       Many_Body::ToFile(avBd, DIR+"avBdCCDAGnoNormtGF"+filename, vecSize);
       Many_Body::ToFile(maxBd, DIR+"maxBdCCDAGnoNormtGF"+filename, vecSize);
	    	    
	    	    
	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
	    std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
  	    std::cout<< n*dt<<" overlapCCDAGnoNormt "<< CCDAGnoNormtval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 n++;
 // if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100))or (TE2.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 	TE1.lbotDmrgStep();
	TE2.lbotDmrgStep();
      }
       // Print(psi1);
       //psi1x=TE1.getBareMps();
 
    return;
   }
 // template<typename TimeEvolver, typename SiteSet>
 //    void GFXDAGX(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename, size_t M)
 //  {
  
 //       applyX(psi1, i, sites);
 //      int n=0;
 //       TE1.makeLbo();
 //       TE2.makeLbo();
 // 		      psi1.position(1);
 // 		      psi2.position(1);
 // 	std::vector<std::complex<double>> xdagx;
 // 	std::vector<double> t;
 // 	std::vector<double> avBd;
 // 	std::vector<double> maxBd;
 // 	std::vector<double> lbomx;
 // 	 auto ne = AutoMPO(sites);
 // 	 for(int j = 1; j < length(psi1); j+=2)
 //    {

 //    ne+=1,"n",j;
 //    }

 // 	 auto Ne = toMPO(ne);
 // 	 auto psi1x=TE1.getBareMps();
 // 	 psi1x.position(1);
 // 	 auto NORM=itensor::norm(psi1x);
 // 	   std::cout<< " start norm "<< NORM<<'\n';
 // 	   while(n*dt<tot)
 //      {

 // 	 auto psi2x=TE2.getBareMps(); 	 
 // 	 psi1x=TE1.getBareMps();
 // 	  applyX(psi2x, j, sites);
 // 	  psi1x*=NORM; 
 // 	  psi1x.position(1);
 // 	  psi2x.position(1);
 // 	  double n2=norm(psi2x);
 // 	  double n1=norm(psi1x);
 // 	  std::complex<double> XDAGXval=innerC(psi2x, psi1x);
 // 	    t.push_back(double(n*dt));
 // 	    avBd.push_back(averageLinkDim(psi1));
 // 	     	    maxBd.push_back(maxLinkDim(psi1));
 // 		    lbomx.push_back(TE1.maxLboDim());
 // 	    xdagx.push_back(XDAGXval);
 // 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/std::complex<double>{n1*n1, 0}<<'\n';
 // 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/std::complex<double>{n2*n2, 0}<<'\n';
 //  	    std::cout<< n*dt<<" overlapXDAGX "<< XDAGXval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 // n++;
 //  if(  (std::max(maxLinkDim(psi1),maxLinkDim(psi2)) >(TE1.maxBd-100)) or (TE2.maxLboDim()>=2*(M)-4) )
 //   {
 //      std::string DIR="XDAGXGF/";
 //      itensor::writeToFile(DIR+"MPSXDAGXGF"+filename,psi1x);
 // 	   itensor::writeToFile(DIR+"sitesetXDAGXGF"+filename,sites);
 // Many_Body::bin_write(DIR+"XDAGXGF"+filename,xdagx);
 //          Many_Body::bin_write(DIR+"timeXDAGXGF"+filename, t);
 //    Many_Body::bin_write(DIR+"lbomxXDAGXGF"+filename, lbomx);
 //     Many_Body::bin_write(DIR+"avBdXDAGXGF"+filename, avBd);
 //      Many_Body::bin_write(DIR+"maxBdXDAGXGF"+filename, maxBd);
 //     std::cout<< "to large "<< std::endl;
 //     Print(psi1);
 //     break;

 //   }
 //  	TE1.lbotDmrgStep();
 // 	TE2.lbotDmrgStep();
 //      }
 //       Print(psi1);
 //       psi1x=TE1.getBareMps();
 //       std::string DIR="XDAGXGF/";
 //            itensor::writeToFile(DIR+"MPSXDAGXGF"+filename,psi1x);
 //     itensor::writeToFile(DIR+"sitesetXDAGXGF"+filename,sites);
 // Many_Body::bin_write(DIR+"XDAGXGF"+filename,xdagx);
 //          Many_Body::bin_write(DIR+"timeXDAGXGF"+filename, t);
 //    Many_Body::bin_write(DIR+"lbomxXDAGXGF"+filename, lbomx);
 //     Many_Body::bin_write(DIR+"avBdXDAGXGF"+filename, avBd);
 //      Many_Body::bin_write(DIR+"maxBdXDAGXGF"+filename, maxBd);
 //    return;
 //   }
} 
#endif /* SPECFUNC_H */
