#pragma once
#ifdef ITENSOR_USE_TBBPARA
#include "tbb/tbb.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
template<typename Functor>
struct para{

   Functor action_;
  para(Functor action):action_(action){};

  void act_para(size_t one, size_t two, size_t three)
  {
    std::cout<<"tbb "<<std::endl;
  tbb::parallel_for( tbb::blocked_range<size_t>(0,one),
     		    [&](const tbb::blocked_range<size_t>& r) {
                       for(size_t i=r.begin(); i!=r.end(); ++i) 
			 action_(i);
     		    }
     		    );
  tbb::parallel_for( tbb::blocked_range<size_t>(one, two),
     		    [&](const tbb::blocked_range<size_t>& r) {
                       for(size_t i=r.begin(); i!=r.end(); ++i) 
			 action_(i);
     		    }
     		    );
  tbb::parallel_for( tbb::blocked_range<size_t>(two, three),
     		    [&](const tbb::blocked_range<size_t>& r) {
                       for(size_t i=r.begin(); i!=r.end(); ++i) 
			 action_(i);
     		    }
     		    );

}

};

#else
#ifdef ITENSOR_USE_OMP
#include<omp.h>
#endif
template<typename Functor>
struct para{

  Functor action_;
  para(Functor action):action_(action){};

  void act_para(size_t one, size_t two, size_t three)
  {
    std::cout<< "omp "<<std::endl;
     #pragma omp parallel
     {
      #pragma omp for
    
       for(size_t i=0; i<one; i++)
      {
	//	 int tid = omp_get_thread_num();
	 //	 std::cout<< "thread "<<tid<<std::endl;
    	action_(i);

	//std::cout<< "done thread "<<tid<<std::endl;
    	    }

      #pragma omp for
    for(size_t i=one; i<two; i++)
      {
    	action_(i);

    	    }
    #pragma omp for 
    for(size_t i=two; i<three; i++)
      {
     	    	action_(i);

    	    }
    
    }
}

};
#endif



