#ifndef SPECFUNC_H
#define SPECFUNC_H
#include"itensor/all.h"
namespace itensor{
  // want to compute <c(t)_j c^{\dagger}_(L/2)>
  // idee is to generate
  // |phi>= exp(-iHt)c^{\dagger}_{L/2}|psi>
  // |\tilde{phi}>=c^{\dagger}_{L/2}exp(-iHt) |psi>
  //   and then compute <\tilde{phi}|phi>
  // I take two time evolvers, TE1 has exp(-iHt) and TE2 has exp(+iHt)
  // psi1 belongs to TE1 and psi2 to TE2
  template<typename TimeEvolver, typename SiteSet>
  void specFuncCCDAG(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename)
  {
    auto NORM=innerC(psi2, psi1);
       applyCdag(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();

	std::vector<std::complex<double>> ccdag;
	std::vector<double> t;
	std::vector<double> avBd;
	std::vector<double> maxBd;
	std::vector<double> lbomx;
	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi1); ++j)
    {

    ne+=1,"n",j;
    }
     ne+=1,"n",length(psi1);
	 auto Ne = toMPO(ne);
       while(n*dt<tot)
      {
	TE1.lbotDmrgStep();
	TE2.lbotDmrgStep();
 	 auto psi2x=TE2.getBareMps();
 	  applyCdag(psi2x, j, sites);
 	  auto psi1x=TE1.getBareMps();
	  // psi1x.position(1);
	  // psi1x.normalize();
	  // psi2x.position(1);
	  // psi2x.normalize();
	  
 	  std::complex<double> CCDAGval=innerC(psi2x, psi1x)/NORM;
 	    t.push_back(double(n*dt));
	    avBd.push_back(averageLinkDim(psi1));
	     	    maxBd.push_back(maxLinkDim(psi1));
		    lbomx.push_back(TE1.maxLboDim());
 	    ccdag.push_back(CCDAGval);
	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/NORM<<'\n';
	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)/NORM<<'\n';
  	    std::cout<< n*dt<<" overlapCCDAG "<< CCDAGval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<"\n";
 n++;
 if( maxLinkDim(psi1) >2000)
   {
     std::cout<< "to large "<< std::endl;
     break;

   }       
      }
       std::string DIR="SP/";
 Many_Body::bin_write(DIR+"CCDAGSP"+filename,ccdag);
          Many_Body::bin_write(DIR+"timeSP"+filename, t);
    Many_Body::bin_write(DIR+"lbomxSP"+filename, lbomx);
     Many_Body::bin_write(DIR+"avBdSP"+filename, avBd);
      Many_Body::bin_write(DIR+"maxBdSP"+filename, maxBd);
    return;
   }


  template<typename TimeEvolver, typename SiteSet>
  void specFuncTebdparaCCDAG(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename)
  {

 auto NORM=innerC(psi2, psi1);
       applyCdag(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();
 	Print(psi1);
 	std::vector<std::complex<double>> ccdag;
 	std::vector<double> t;
 	 auto ne = AutoMPO(sites);
 	 for(int j = 1; j < length(psi1); ++j)
    {

    ne+=1,"n",j;
    }
     ne+=1,"n",length(psi1);
 	 auto Ne = toMPO(ne);
       while(n*dt<tot)
      {
 		TE1.lboTEBDStepO2paraFT();
 	TE2.lboTEBDStepO2paraFT();
 	 auto psi2x=TE2.getBareMps();
 	  applyCdag(psi2x, j, sites);
 	  auto psi1x=TE1.getBareMps();
 	  std::complex<double> CCDAGval=innerC(psi2x, psi1x)/NORM;
 	    t.push_back(double(n*dt));
 	    ccdag.push_back(CCDAGval);
 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
  	    std::cout<< n*dt<<" overlapCCDAG "<< CCDAGval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<"\n";
 n++;
 if( maxLinkDim(psi1) >2000)
   {
     std::cout<< "to large "<< std::endl;
     break;

   }       
      }
       std::string DIR="SP/";
 Many_Body::bin_write(DIR+"CCDAGSPtebdlbopara"+filename,ccdag);
          Many_Body::bin_write(DIR+"timeSPtebdlbopara"+filename, t);
    // Many_Body::bin_write(DIR+"lbomxSPtebdlbopara"+filename, lbomx);
    //  Many_Body::bin_write(DIR+"avBdSPtebdlbopara"+filename, avBd);
    //   Many_Body::bin_write(DIR+"maxBdSPtebdlbopara"+filename, maxBd);
   
    return;
   }
    template<typename TimeEvolver, typename SiteSet>
  void specFuncCDAGC(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename)
  {
    auto NORM=innerC(psi2, psi1);
       applyC(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();

	std::vector<std::complex<double>> cdagc;
	std::vector<double> t;
	std::vector<double> avBd;
	std::vector<double> maxBd;
	std::vector<double> lbomx;
	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi1); ++j)
    {

    ne+=1,"n",j;
    }
     ne+=1,"n",length(psi1);
	 auto Ne = toMPO(ne);
       while(n*dt<tot)
      {
	TE1.lbotDmrgStep();
	TE2.lbotDmrgStep();
 	 auto psi2x=TE2.getBareMps();
 	  applyC(psi2x, j, sites);
 	  auto psi1x=TE1.getBareMps();
 	  std::complex<double> CDAGCval=innerC(psi2x, psi1x)/NORM;
 	    t.push_back(double(n*dt));
	    avBd.push_back(averageLinkDim(psi1));
	     	    maxBd.push_back(maxLinkDim(psi1));
		    lbomx.push_back(TE1.maxLboDim());
 	    cdagc.push_back(CDAGCval);
	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
  	    std::cout<< n*dt<<" overlapCDAGC "<< CDAGCval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<"\n";
 n++;
 if( maxLinkDim(psi1) >2000)
   {
     std::cout<< "to large "<< std::endl;
     break;

   }       
      }
       std::string DIR="SP/";
 Many_Body::bin_write(DIR+"CDAGCSP"+filename,cdagc);
          Many_Body::bin_write(DIR+"timeSP"+filename, t);
    Many_Body::bin_write(DIR+"lbomxSP"+filename, lbomx);
     Many_Body::bin_write(DIR+"avBdSP"+filename, avBd);
      Many_Body::bin_write(DIR+"maxBdSP"+filename, maxBd);
    return;
   }


  template<typename TimeEvolver, typename SiteSet>
  void specFuncTebdparaCDAGC(TimeEvolver& TE1,  MPS& psi1,TimeEvolver& TE2,  MPS& psi2,   SiteSet& sites, int i, int j, double dt, double tot, std::string filename)
  {
   
 auto NORM=innerC(psi2, psi1);
       applyC(psi1, i, sites);
      int n=0;
       TE1.makeLbo();
       TE2.makeLbo();
 	Print(psi1);
 	std::vector<std::complex<double>> cdagc;
 	std::vector<double> t;
 	 auto ne = AutoMPO(sites);
 	 for(int j = 1; j < length(psi1); ++j)
    {

    ne+=1,"n",j;
    }
     ne+=1,"n",length(psi1);
 	 auto Ne = toMPO(ne);
       while(n*dt<tot)
      {
 		TE1.lboTEBDStepO2paraFT();
 	TE2.lboTEBDStepO2paraFT();
 	 auto psi2x=TE2.getBareMps();
 	  applyC(psi2x, j, sites);
 	  auto psi1x=TE1.getBareMps();
 	  std::complex<double> CDAGCval=innerC(psi2x, psi1x)/NORM;
 	    t.push_back(double(n*dt));
 	    cdagc.push_back(CDAGCval);
 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)<<'\n';
 	       std::cout<< " f2 "<< innerC(psi2x,Ne, psi2x)<<'\n';
  	    std::cout<< n*dt<<" overlapCCDAG "<< CDAGCval<<"lbo max psi1 "<< TE1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<< TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<"\n";
 n++;
 if( maxLinkDim(psi1) >2000)
   {
     std::cout<< "to large "<< std::endl;
     break;

   }   
 
    }
       std::string DIR="SP/";
 Many_Body::bin_write(DIR+"CDAGCSPtebdlbopara"+filename,cdagc);
          Many_Body::bin_write(DIR+"timeSPtebdlbopara"+filename, t);
    // Many_Body::bin_write(DIR+"lbomxSPtebdlbopara"+filename, lbomx);
    //  Many_Body::bin_write(DIR+"avBdSPtebdlbopara"+filename, avBd);
    //   Many_Body::bin_write(DIR+"maxBdSPtebdlbopara"+filename, maxBd);
    return;
   }
}


#endif /* SPECFUNC_H */
