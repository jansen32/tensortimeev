#ifndef MAKETIMEEVOP_H
#define MAKETIMEEVOP_H
#include"holstein.hpp"
#include<algorithm>
//#include"htham.hpp"
#include"itensor/mps/sites/electron.h"
#include<map>
#include"maketimeevopHolst.hpp"
#include"maketimeevopStruct.hpp"
#include"maketimeevopInt.hpp"
namespace itensor{

 template<typename Lattice, typename Iterator>
 struct ferHetGates{
   using Latticet=Lattice;
   ferHetGates(Lattice& sites, Iterator& gates, double tstep, std::map<std::string, double> param, int Llead1, int Llead2, int Lchain):sites(sites), N(length(sites)), Llead1(Llead1), Llead2(Llead2), Lchain(Lchain),  gates(gates), tstep(tstep), t0(param["t0"]), tl(param["tl"]), tint(param["tint"]), V(param["V"]) {
       makeGates();
   };
   
   void makeGates();
   //void makeGates(std::vector<int>);
 using Gate = BondGate;
   Gate oneGate(int, double);
   Lattice& sites;
   int N;
   int Llead1;
   int Llead2;
   int Lchain;
   double t0;
   double tl;
   double tint;
   double tstep;
   double V;
   Iterator& gates;  
 };
  

   template<typename Lattice, typename Iterator>
 struct spinGates{
     spinGates(Lattice& sites, Iterator& gates, double tstep, std::map<std::string, double> param):sites(sites), gates(gates), N(sites.N()), tstep(tstep), J(param["J"]){makeGates();};
   void makeGates();   
   
     Lattice& sites;
   Iterator& gates;
     int N;
     double tstep{};
   double J;

 };
   template<typename Lattice, typename Iterator>
 struct spinGatesImp{
   spinGatesImp(Lattice& sites, Iterator& gates, double tstep, std::map<std::string, double> param):sites(sites), gates(gates), N(sites.N()), tstep(tstep), J(param["J"]), h(param["h"]){makeGates();};
   void makeGates();   
   
     Lattice& sites;
   Iterator& gates;
     int N;
     double tstep{};
   double J;
      double h;

 };
   template<typename Lattice, typename Iterator>
 void spinGatesImp< Lattice, Iterator>::makeGates()
 {

  using Gate = BondGate;
  
   for(int b = 2; b <= N-1; b+=2)
    {

   auto hterm = J*sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += J*0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += J*0.5*sites.op("S-",b)*sites.op("S+",b+1);
if(b==2){hterm+= h*sites.op("Sz",2)*sites.op("Id",3);}
     
      auto g = Gate(sites,b,b+1,Gate::tReal,tstep/(2), hterm);
         gates.push_back(g);
    }
   Iterator even=gates;
   
      for(int b = 1; b <= N-1; b+=2)
    {

   auto hterm = sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += 0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += 0.5*sites.op("S-",b)*sites.op("S+",b+1);
    


      auto g = Gate(sites,b,b+1,Gate::tReal,tstep, hterm);
         gates.push_back(g);
    }
      gates.insert(gates.end(), even.begin(), even.end());
}
 template<typename Lattice, typename Iterator>
 void spinGates< Lattice, Iterator>::makeGates()
 {

  using Gate = BondGate;
  
   for(int b = 2; b <= N-1; b+=2)
    {

   auto hterm = J*sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += J*0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += J*0.5*sites.op("S-",b)*sites.op("S+",b+1);

     
      auto g = Gate(sites,b,b+1,Gate::tReal,tstep/(2), hterm);
         gates.push_back(g);
    }
   Iterator even=gates;
      for(int b = 1; b <= N-1; b+=2)
    {

   auto hterm = sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += 0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += 0.5*sites.op("S-",b)*sites.op("S+",b+1);


      auto g = Gate(sites,b,b+1,Gate::tReal,tstep, hterm);
         gates.push_back(g);
    }
      gates.insert(gates.end(), even.begin(), even.end());
}

template<typename Lattice, typename Iterator>
void ferHetGates< Lattice, Iterator>::makeGates()
{
  // std::cout<< "gam "<< gamma << " omg "<< omega << " t0 " << t0 << std::endl;
    gates.clear();
   

  //  Print(sites);
   for( int b = 2; b <= N-1; b+=2)
           {
	     
	     gates.push_back(oneGate(b,tstep/2));
    }
     Iterator even=gates;
     for(int b = 1; b <= N-1; b+=2)
    {
 gates.push_back(oneGate(b,tstep));
    }
gates.insert(gates.end(), even.begin(), even.end());
 
//   std::cout<< gates.size()<< std::endl;
}
template<typename Lattice, typename Iterator>
auto ferHetGates< Lattice, Iterator>::oneGate(int b, double dt)
  ->ferHetGates< Lattice, Iterator>::Gate
{

 

  if(b<Llead1)
    {
      auto hterm=-tl*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tl*sites.op("Cdag", b+1)*(sites.op("C",b));
      hterm+=-V*sites.op("N", b)*(sites.op("Id",b+1));
 auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
     }
     else if(b==Llead1)
    {
      auto hterm=-V*sites.op("N", b)*(sites.op("Id",b+1));
      hterm+=-tint*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tint*sites.op("Cdag", b+1)*(sites.op("C",b));

          
auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
    }

else if(b> Llead1 && b<(Llead1+Lchain))
      {
  
  	auto hterm=-t0*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-t0*sites.op("Cdag", b+1)*(sites.op("C",b));
 auto g = Gate(sites,b,b+1,Gate::tReal,tstep, hterm);
   return g;
      }
     else if(b==Llead1+Lchain)
     {
       auto hterm=-tint*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tint*sites.op("Cdag", b+1)*(sites.op("C",b));
      
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
     }
  else {
           auto hterm=-tl*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tl*sites.op("Cdag", b+1)*(sites.op("C",b));
      
      hterm+=V*sites.op("N", b)*(sites.op("Id",b+1));

      if(b==Llead2+Llead1+Lchain-1)
  	{
  	   hterm+=V*sites.op("Id", b)*(sites.op("N",b+1));


  	}
            auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
   }   
}

 
  using IFHG=ferHetGates<Fermion,std::vector<BondGate> >;

}

#endif /* MAKETIMEEVOP_H */
