#ifndef MAKETIMEEVOPINT_H
#define MAKETIMEEVOPINT_H
#include"holstein.hpp"
#include<algorithm>
//#include"htham.hpp"
#include"itensor/mps/sites/electron.h"
#include<map>
namespace itensor{


     template<typename Lattice, typename Iterator>
 struct holstIntGates{
   using Latticet=Lattice;
       holstIntGates(Lattice& sites, Iterator& gates, double tstep, std::map<std::string, double> param, int Llead1, int LH):sites(sites),  N(length(sites)), Llead1(Llead1), LH(LH),   t0(param["t0"]), tl(param["tl"]), tint(param["tint"]), V(param["V"]),omega(param["omega"]), gamma(param["gamma"]), eps0(param["eps0"]), lambda(param["lambda"]),tstep(tstep), gates(gates) {makeGates();};
   
   void makeGates();
       void set_current_time(const double& time)
       {
	 current_time=time;
}
   void makeGates(std::vector<int>);
 using Gate = BondGate;
     Gate oneGate(int, double);
     Lattice& sites;
  
     int N;
       
     int Llead1;
     int LH;
     
     double t0{};
     double tl{};
     double tint{};
    
     double V{};
     double omega{};
     double gamma{};
     double eps0{};
       double lambda;
      double tstep{};
     Iterator& gates;
       double current_time{0};

 };  

template<typename Lattice, typename Iterator>
void holstIntGates< Lattice, Iterator>::makeGates()
{

    gates.clear();
   
    auto evenGates_L1=oneGate(2,tstep/2);
    auto oddGates_L1=oneGate(1,tstep);

    auto evenGates_LH=oneGate(Llead1+2,tstep/2);
    auto oddGates_LH=oneGate(Llead1+1,tstep);

      gates.push_back(Gate(sites,2,3, evenGates_L1));
   for( int b = 4; b <= N-1; b+=2)
           {

	     if(b<Llead1)
	       {
		 auto i1=sites(b);
	       auto i2=sites(b+1);
	       ITensor currentGate=evenGates_L1;
	       currentGate*=itensor::delta((sites(2)),dag(i1));
	       currentGate*=itensor::delta((sites(3)),dag(i2));
	       
	       
	       currentGate*=itensor::delta(prime(dag(sites(2))),prime(i1));
	       currentGate*=itensor::delta(prime(dag(sites(3))),prime(i2));
	       gates.push_back(Gate(sites,b,b+1, currentGate));
}
	     else if(b==Llead1)
	       {gates.push_back(oneGate(b,tstep/2));} // end else if at center link
	     else
	       { // if b is on right lead
		 if((b==N-1) or ( b==Llead1+2)) // if b is on secodn to last link
		   {
	     gates.push_back(oneGate(b,tstep/2));
} // end if b is on second to last link
		 else{

	 auto i1=sites(b);
	       auto i2=sites(b+1);
	       ITensor currentGate=evenGates_LH;

	       
	       currentGate*=itensor::delta((sites(Llead1+2)),dag(i1));
	       currentGate*=itensor::delta((sites(Llead1+3)),dag(i2));

	       
	       currentGate*=itensor::delta(prime(dag(sites(Llead1+2))),prime(i1));
	       currentGate*=itensor::delta(prime(dag(sites(Llead1+3))),prime(i2));
	       gates.push_back(Gate(sites,b,b+1, currentGate));
}
	       } // end else : b on right lead
	     

    }

     Iterator even=gates;
gates.push_back(Gate(sites,1,2, oddGates_L1));
     for(int b = 3; b <= N-1; b+=2)
    {

 if(b<Llead1)
	       {
		 auto i1=sites(b);
	       auto i2=sites(b+1);
	       ITensor currentGate=oddGates_L1;
	       currentGate*=itensor::delta((sites(1)),dag(i1));
	       currentGate*=itensor::delta((sites(2)),dag(i2));
	       
	       
	       currentGate*=itensor::delta(prime(dag(sites(1))),prime(i1));
	       currentGate*=itensor::delta(prime(dag(sites(2))),prime(i2));
	       gates.push_back(Gate(sites,b,b+1, currentGate));
}
	     else if(b==Llead1)
	       {gates.push_back(oneGate(b,tstep));} // end else if at center link
	     else
	       { // if b is on right lead
		 if((b==N-1) or (b==Llead1+1)) // if b is on secodn to last link or Llead1+15A
		   {
	     gates.push_back(oneGate(b,tstep));
} // end if b is on second to last link
		 else{

	 auto i1=sites(b);
	       auto i2=sites(b+1);
	       ITensor currentGate=oddGates_LH;
	       currentGate*=itensor::delta((sites(Llead1+1)),dag(i1));
	       currentGate*=itensor::delta((sites(Llead1+2)),dag(i2));
	       
	       
	       currentGate*=itensor::delta(prime(dag(sites(Llead1+1))),prime(i1));
	       currentGate*=itensor::delta(prime(dag(sites(Llead1+2))),prime(i2));
	       gates.push_back(Gate(sites,b,b+1, currentGate));
}
	       } // end else : b on right lead
	     
 
    }
gates.insert(gates.end(), even.begin(), even.end());

}
 
 template<typename Lattice, typename Iterator>
auto holstIntGates< Lattice, Iterator>::oneGate(int b, double dt)
  ->holstIntGates< Lattice, Iterator>::Gate
{


  if(b<Llead1)
    {
      double decay=1;
      // if(b<=4)
      // 	{
      // 	  //      decay=std::pow(lambda, -double(Llead1-1-b)/2.);
      //   decay=std::pow(lambda, -double(5-b)/2.);
      // 	}
      auto hterm=(-tl)*decay*sites.op("Cdag", b)*(sites.op("C",b+1));
            hterm+=-tl*decay*sites.op("Cdag", b+1)*(sites.op("C",b));
	    hterm+=-V*step_func(current_time)*sites.op("N", b)*(sites.op("Id",b+1));
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
      return g;
     }
     else if(b==Llead1)
    {
            
      auto hterm=-tint*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-tint*sites.op("Cdag", b+1)*(sites.op("C",b));
      hterm+=-V*step_func(current_time)*sites.op("N", b)*(sites.op("Id",b+1));
          
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
    }
  else if(b==Llead1+1)
    {
      double decay=1.;
   // if((Llead1+LH-b)<=4)
   // 	    {
   // 	      decay=std::pow(lambda, -double(5-(Llead1+LH-b))/2.);
   // 	      //	      decay=std::pow(lambda, -double(b-(Llead1+1))/2.);
   // 	    }

      	auto hterm=-t0*decay*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-t0*decay*sites.op("Cdag", b+1)*(sites.op("C",b));
      hterm += gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      
      hterm += gamma*sites.op("NB",b)*sites.op("Id", b+1);
      hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+1));
      hterm += eps0*(sites.op("n",b)*sites.op("Id", b+1));
hterm+=+V*step_func(current_time)*sites.op("N", b)*(sites.op("Id",b+1));
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
    }
  else {
     double decay=1.;
    //     if((Llead1+LH-b)<=4)
    // 	    {
    // 	        decay=std::pow(lambda, -double(5-(Llead1+LH-b))/2.);
    // 	      // decay=std::pow(lambda, -double(b-(Llead1+1))/2.);
    // 	    }
     	  
	auto hterm=-t0*decay*sites.op("Cdag", b)*(sites.op("C",b+1));
      hterm+=-t0*decay*sites.op("Cdag", b+1)*(sites.op("C",b));
      hterm += gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      hterm += gamma*sites.op("NB",b)*sites.op("Id", b+1);
      hterm += omega*(sites.op("Nph",b)*sites.op("Id", b+1));
      hterm += eps0*(sites.op("n",b)*sites.op("Id", b+1));
       hterm+=V*step_func(current_time)*sites.op("N", b)*(sites.op("Id",b+1));

      if(b==Llead1+LH-1)
   	{
	   hterm+=V*step_func(current_time)*sites.op("Id", b)*(sites.op("N",b+1));
		   hterm += gamma*sites.op("Id", b)*sites.op("NBdag",b+1);
		   hterm += gamma*sites.op("Id", b)*sites.op("NB",b+1);
		   hterm += omega*sites.op("Id", b)*(sites.op("Nph",b+1));
		   hterm += eps0*sites.op("Id", b)*(sites.op("n",b+1));

   	}
      auto g = Gate(sites,b,b+1,Gate::tReal,dt, hterm);
   return g;
   }   
}

 using IHGint=holstIntGates<Holstein,std::vector<BondGate> >;


}

#endif /* MAKETIMEEVOPINT_H */
