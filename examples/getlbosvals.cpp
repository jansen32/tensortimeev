#include "itensor/all.h"

#include"../src/maketimeevop.hpp"

#include"files.hpp"
#include"../src/holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/makehamiltonians.hpp"
#include <iomanip> 
#include <iostream>
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  using Gate = BondGate<IQTensor>;
  const double t0=std::stod(argv[1]);
  const double gamma=std::stod(argv[2]);
  const double omega=std::stod(argv[3]);
    
  std::string sgam=std::string(argv[2]).substr(0,3);
  std::string somg=std::string(argv[3]).substr(0,3);

  std::string st0=std::string(argv[1]).substr(0,3);
//Create a std::vector (dynamically sizeable array)
//to hold the Trotter gates
 
 auto gates = vector<Gate>();
  int N = 15;
  auto tstep = 1e-3;
  auto cutoff=1E-13;
  int Mph=3;
  auto sites = Holstein(N, Mph);
  auto state = InitState(sites);
  
 auto args = Args("Cutoff=",cutoff,"Maxm=",5000, "Normalize",true);
state.setAll("EmpPh");
 state.set(int(N/2), "OccPh");

 

auto psi = IQMPS(state);

 psi.position(1);
 psi.normalize();

 std::map<std::string, double> param;
 param.insert({"t0",t0});
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
     // double alpha=0.1;
//   param.insert({"alpha",alpha});
    
    
 auto psiv=vMPS<IQTensor>(psi);
IQHG GM(sites, gates, tstep, param);
  TimeEvolveLbo< IQHG, decltype(psiv)> C(GM,  psiv,  Mph, args);
  std::string filename="holstdmrglbovalsL"+std::to_string(N) + "t0_"+st0 + "gam" +sgam + "omg" +somg+ ".bin"; 

 std::vector<double> maxD;
  std::vector<double> minD;
    std::vector<double> avD;
  std::vector<double> time;
  //int mx= C.getMaxOBD();

  // int mn= C.getMinOBD();
 // C.tDmrgStepO2();
 // C.tDmrgStepO2();
 // double  av= C.getOBDav();
 // std::cout<< av << std::endl;
  int i=0;  
   while(i*tstep<3)
     {
 C.TEBDStepO2para();

 int mx= C.getMaxOBD();
  int mn= C.getMinOBD();
    double  av= C.getOBDav();
    std::cout<< mx <<"  "<< mn<< "  "<< "  " << av<< "  "<<i*tstep<<"  " << maxM(psi)<<std::endl;
      maxD.push_back(mx);
      minD.push_back(mn);
      avD.push_back(av);
 	time.push_back(i*tstep);
 	i+=1;
 	  }
  Many_Body::bin_write("maxD"+filename, maxD);
    Many_Body::bin_write("minD"+filename, minD);
        Many_Body::bin_write("avD"+filename, avD);

 Many_Body::bin_write("time" + filename, time);
 Print(psi);
  return 0;
}
