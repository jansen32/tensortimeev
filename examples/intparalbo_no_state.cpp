#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include<iostream>
	
using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
  int maxSweeps{};
    double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
    int Mph{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int LH{};
  bool saveNr=false;
  bool RDM{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
    double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLH{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string filename="GSint";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("LH", boost::program_options::value(&LH)->default_value(4), "LH")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("MS", boost::program_options::value(&maxSweeps)->default_value(20), "MS")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("RDM", boost::program_options::value(&RDM)->default_value(false), "RDM");
    ("saveNr", boost::program_options::value(&saveNr)->default_value(false), "saveNr");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("LH"))
      {      std::cout << "LH: " << LH << '\n';
      	sLH="LH"+std::to_string(vm["LH"].as<int>());
	filename+=sLH;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 3);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 3);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 4);
      		filename+=seps0;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << V << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << tot << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << dt << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		       	     if (vm.count("Md"))
      {      std::cout << "Md: " << Md << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << lboMd << '\n';
      	slboMd="lboMd"+std::to_string(lboMd);
      	filename+=slboMd;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << cutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       		 if (vm.count("lbocut"))
      {      std::cout << "lbocutoff: " << lbocutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 scutoff="lbocut"+ss.str();
      	filename+=scutoff;
      }
      	     if (vm.count("RDM"))
      {      std::cout << "RDM: " << RDM << '\n';

      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	filename+=".bin";
  std::cout<<filename<<std::endl;

  int N=Llead1+LH;
  
  std::vector<int> v(N, 0);
  // is stored as vecto so start site-1
  for(int i=Llead1; i<v.size(); i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",true);

    auto sites = itensor::Holstein(N, argsState);
 
    auto H1=itensor::makeIntHam(sites, Llead1, LH, tl, tint, t0, gamma, omega, eps0 );
    auto H2=itensor::makeIntHamV(sites, Llead1, LH, V/2, tl, tint, t0, gamma, omega, eps0);
    
    auto state = itensor::InitState(sites);
    for(int i=1; i<=N; i+=1)
      {
 	if(i%2==0){state.set(i, "Occ");}
      }

    auto psi = MPS(state);
      psi.position(1);
     psi.normalize();
         Print(psi);
    
  	 auto sweeps = itensor::Sweeps(maxSweeps);
    //very important to use noise for this model
	  sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
	  sweeps.maxdim() = 10,20, 80,200,300, 400, 400, 500, 500;
	  sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-09, 1E-09,1E-10, 1E-10;
    std::cout<< "start "<< std::endl;
    auto [energy,psi0] = itensor::dmrg(H1,psi,sweeps,{"Quiet=",true});
 psi0.position(1);
     psi0.normalize();
        itensor::printfln("Ground State Energy1 = %.12f",energy);
auto args = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);

  auto psiOne=applyMPO(H1,psi0, args2);
//auto H1sqrd=itensor::multSiteOps(H2, H2);
 auto E0=itensor::innerC(psi0, H1,psi0);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psiOne,psiOne);
 std::cout<< " djei "<< E0Sqrd<<std::endl;
 	  auto VAL=E0*E0;
 std::cout<< " djei 2 2 "<< VAL<<std::endl;
 auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
 		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
 		    if(std::abs((E0Sqrd-VAL )/E0Sqrd)>1e-04){return 1;}
 		    if(std::abs(E0Sqrd-VAL)>1e-04){return 1;}
  psi0.position(1);
  psi0.normalize();
  std::cout<< "stop 1"<< std::endl;
  auto Curr=MPO(itensor::jint(sites, Llead1, tint ));
    auto Neint=MPO(itensor::Neint(sites, Llead1 ));
     auto EHYB=MPO(itensor::EHYBint(sites, Llead1, tint));
     auto EL=MPO(itensor::ELint(sites, Llead1, LH, tl, V/2 ));
     auto ER=MPO(itensor::ERint(sites, Llead1, LH,V/2, t0,gamma, omega,  eps0 ));
   std::vector<double> obstebd;
   std::vector<double> Xvec;
      std::vector<double> Pvec;
     
   std::vector<double> nphvec;
      std::vector<double> nevec;
  std::vector<double> time;
  std::map<std::string, double> param;
   std::cout<< "stop "<< std::endl;
 param.insert({"t0",t0});
 param.insert({"tl",tl});
 param.insert({"tint",tint});
 param.insert({"V",-V/2}); // now using -V
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"eps0",eps0});


 

  int i=0;

  itensor::IHGint GM(sites, gates, dt, param, Llead1,LH);
  auto psiv=itensor::vMPS(psi0);
 
  itensor::TimeEvolveLbo< itensor::IHGint, decltype(psiv), false> TE(GM,  psiv, N,  argsMPS, argsState);
 TE.makeLboFrom(Llead1);

     while(dt*i<tot)
    {
       if(std::abs(itensor::step_func(dt*i)-1.)>0.00001)
     	{
     	  GM.set_current_time(dt*i);
     	  std::cout<< "curr time step func "<<itensor::step_func(dt*i)<<std::endl;
     	  GM.makeGates();
     	}
auto psir2=TE.getBareMpsFrom(Llead1);
    	 psir2.position(1);
    	 psir2.normalize();
		 
    		 auto c = (itensor::innerC(psir2, Curr, psir2));
		 auto neintval = (itensor::innerC(psir2, Neint, psir2));
		 auto er = (itensor::innerC(psir2, ER, psir2));
		 auto el = (itensor::innerC(psir2,EL, psir2));
		 auto ehyb = (itensor::innerC(psir2, EHYB, psir2));
    		  if(std::abs(imag(c))>1E-14){std::cout<< " errror in obs"; std::cout<<std::abs(imag(c))<<std::endl; }
		  
    		    double avl2=real(c);
		    double neint=real(neintval);
		    std::vector<double> nevec2;
		    std::vector<double> nphvec;
		    if(saveNr)
		      {
		    for(int j=1; j<=length(psir2); j++)
		      {
			psir2.position(j);
			  auto ket = psir2(j);
        auto bra = dag(prime(ket,"Site"));
        auto Neloc = op(sites,"n",j);
	auto Nphloc = op(sites,"Nph",j);
	auto nph = eltC(bra*Nphloc*ket);
	auto ne = eltC(bra*Neloc*ket);
	nevec2.push_back(real(ne));
	nphvec.push_back(real(nph));
	Many_Body::bin_write("Nphloct"+std::to_string(dt*i).substr(0, 6)+filename, nphvec);
	Many_Body::bin_write("Neloct"+std::to_string(dt*i).substr(0, 6)+filename, nevec2);
		      }
		      }
    		  time.push_back(i*dt);
    		  obstebd.push_back(avl2);
		  nevec.push_back(real(neint));
    
		  if(maxLinkDim(psi0)>=Md-100)
		    {
      Many_Body::bin_write("time"+filename, time);
    Many_Body::bin_write("J"+filename, obstebd);
     Many_Body::bin_write("Neint"+filename, nevec);

    std::cout<< "finish "<<std::endl;
break;
		    }
    		  std::cout<< std::setprecision(12)<<dt*i <<"  " << c << "  E "<< innerC(psir2, H2, psir2)<< " comp  "<< el+er+ehyb<<"  "<<c<< " n "<<real(neint)<<" " "lbo max "<< TE.maxLboDim()<<"   "<< " mx  "<< maxLinkDim(psi0)<<" av  "<< averageLinkDim(psi0)<<"  "<<"\n";
		  std::cout<< "start TC"<<std::endl;
       	   TE.lboTEBDStepO2paraFrom(Llead1);
	   i++;
	       		 	  }
 
     std::cout<< "size "<<nevec.size()<<std::endl;
      Many_Body::bin_write("time"+filename, time);
    Many_Body::bin_write("J"+filename, obstebd);
    // Many_Body::bin_write("X"+filename, Xvec);
    //     Many_Body::bin_write("Edot"+filename, Edotvec);
    // 	    Many_Body::bin_write("P"+filename, Pvec);
    // Many_Body::bin_write("Nph"+filename, nphvec);
     Many_Body::bin_write("Neint"+filename, nevec);
     //     Print(psi0);
     psi0.position(1);
     std::cout<< " norm "<< norm(psi0)<< std::endl;
     return 0;
}
