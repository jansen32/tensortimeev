
#include "itensor/all.h"
#include"files.hpp"

#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include"../src/GFopt.hpp"
using namespace std;
using namespace itensor;
void apply_trivial(std::vector<ITensor> gates, MPS& psi)
{
    size_t N=length(psi);
    size_t gate_pos=0;
       for(int i=1; i<=N; i+=2)
     {
	psi.position(i);
   psi.Aref(i) *= gates[gate_pos];
   gate_pos++;
   

       psi.Aref(i).noPrime("Site");

     }
}
std::vector<ITensor> trivial_timeev(SiteSet& sites,  double omega_0, double dt)
{
    size_t N=length(sites);
  std::vector<std::complex<double>> data;

  auto s1=sites(1);
  auto d=blocksize(s1, 1);

    for(int j=0; j<d; j++)
    {

      data.push_back(std::exp(std::complex<double>{0,-dt*omega_0*(j)}));         
    }
    for(int j=0; j<d; j++)
    {
       
           data.push_back(std::exp(std::complex<double>{0,-dt*omega_0*(j)}));         
    }

    std::vector<ITensor> time_ev;
   for(int i=1; i<=N; i+=2)
     {
     
        auto T= op(sites,"Bdag",i);
       auto D = ITensor(T.inds());
       
	auto j1=T.inds()[0];
	auto j2=T.inds()[1];


	for(int m=1; m<=data.size(); m+=1)
     {
         D.set(j1(m), j2(m), data[m-1]);       

     }
	time_ev.push_back(D);
     }

}
int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int i{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};

  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("i", boost::program_options::value(&i)->default_value(0), "i")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	     	     if (vm.count("j"))
		       {      if(i==0)
			   {
 i=(int(L/2)+1);
			   }
			 else{}
std::cout << "j: " << vm["j"].as<int>() << '\n';
	sj="i"+std::to_string(i)+"j"+std::to_string(j);
	filename+=sj;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }

      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    //    Print(psi);
    // Print(sites);
   using Gate = BondGate;
    
auto gates1 = vector<Gate>();
auto gates2 = vector<Gate>();
  int N=2*L;
   std::vector<int> v(N, M);
   itensor::Args argsState={"ConserveNf=",true,
                              "ConserveNb=",false,
 			   "DiffMaxOcc=",true, "MaxOccVec=", v};
   auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",false);
   auto GM=trivial_timeev( sites, omega,0.5*dt);
  auto GM1=trivial_timeev( sites, omega,-0.5*dt);


   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   


 auto psi2=psi;
  
 auto ampo=makePureHolstHam(sites, t0, gamma, omega);
 auto H=toMPO(ampo);

 
 j*=2;
 j--;
 i*=2;
 i--;
  applyC(psi, i, sites);
    
       int n=0;
        std::cout<< " i in aux space "<< i << std::endl;
  	 auto ne = AutoMPO(sites);
 	 for(int j = 1; j < length(psi); j+=2)
    {

    ne+=1,"n",j;
    }

 auto Ne = toMPO(ne);
      mpsName.insert(0, filename); 
 
  mpsName.insert(0, "tdmrglboAux");
  std::string obsname="CDAGCTriv";
       std::string DIR=obsname+"noNormKarGF/";
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     if(boost::filesystem::exists(DIR+obsname+"noNormKarGF"+mpsName))
	       {
		 mpsName+="2";
}

  std::cout<< "start "<< std::endl;
    while(n*dt<tot)
      {
	auto psi2x=psi2;
applyC(psi2, j, sites);
	  double n2=norm(psi2);
 	  double n1=norm(psi);
	  std::complex<double> CCDAGnoNormKarval= innerC(psi2, psi);
 	std::vector<std::complex<double>> ccdag;
 	std::vector<double> t;
		    t.push_back(double(n*dt));
 	    ccdag.push_back(CCDAGnoNormKarval);
	           auto vecSize=t.size();
       Many_Body::ToFile(ccdag, DIR+obsname+"noNormKarGF"+filename,vecSize);
       Many_Body::ToFile(t, DIR+"time"+obsname+"noNormKarGF"+filename, vecSize);
        	    // /NORM
 	    std::cout<< " f1 "<< innerC(psi,Ne, psi)/innerC(psi, psi)<<'\n';
	    std::cout<< " f2 "<< innerC(psi2,Ne, psi2)/innerC(psi2, psi2)<<'\n';
	     	       std::cout<< n*dt<<" overlap"+obsname+"KarnoNorm "<< CCDAGnoNormKarval<< " mx psi1  "<< maxLinkDim(psi)<<" av psi1  "<< averageLinkDim(psi)<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi)<<" Norm 1.1= "<< norm(psi)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
	 n++;
  // 	 apply_trivial(GM,  psi);
  // apply_trivial(GM1,  psi2);
    }


  return 0;
}
