#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include<iostream>
#include "tbb/task_scheduler_init.h"			
using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
#ifdef ITENSOR_USE_TBB
 tbb::task_scheduler_init init(8);
#endif
  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
 //tbb::task_scheduler_init init(6);
    double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
    int maxSweeps{};
    int Mph{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  bool RDM{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
    bool SV{false};
    double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string filename="holstructGS";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("SV", boost::program_options::value(&SV)->default_value(false), "SV")
            ("MS", boost::program_options::value(&maxSweeps)->default_value(20), "MS");;
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << Llead2 << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << Lchain << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 3);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 3);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 4);
      		filename+=seps0;
      }
  
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	filename+=".bin";
  std::cout<<filename<<std::endl;

  int N=Llead1+Llead2+Lchain;
  
  std::vector<int> v(N, 0);
  for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",true);

    auto sites = itensor::Holstein(N, argsState);
 
    auto H1=itensor::makeHetHam(sites, Llead1, Llead2,  Lchain, tl, tint, t0, gamma, omega, eps0 );
    
    auto state = itensor::InitState(sites);
    int elNR=0;
    for(int i=1; i<=N; i+=1)
      {
 	if(i%2==0){state.set(i, "Occ"); elNR+=1;}
      }
    std::cout<< "nr of electron " << elNR<<std::endl;
    auto psi = MPS(state);
      psi.position(1);
     psi.normalize();
     //         Print(psi);
    
  	 auto sweeps = itensor::Sweeps(maxSweeps);
    //very important to use noise for this model
	  sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
	  sweeps.maxdim() = 10,20, 80,200,300, 400, 400, 500, 500;
	  sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-09, 1E-09,1E-10, 1E-10;
    std::cout<< "start "<< std::endl;
    auto [energy,psi0] = itensor::dmrg(H1,psi,sweeps,{"Quiet=",true});
 psi0.position(1);
     psi0.normalize();
        itensor::printfln("Ground State Energy1 = %.12f",energy);
auto args = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);

  auto psiOne=applyMPO(H1,psi0, args2);

 auto E0=itensor::innerC(psi0, H1,psi0);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psiOne,psiOne);
 std::cout<< " djei "<< E0Sqrd<<std::endl;
 	  auto VAL=E0*E0;
 std::cout<< " djei 2 2 "<< VAL<<std::endl;
 auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
 		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
		    //    auto Neint=MPO(itensor::Neint(sites, Llead1 ));

   
 		    if(std::abs((E0Sqrd-VAL )/E0Sqrd)>1e-04){return 1;}
 		    if(std::abs(E0Sqrd-VAL)>1e-04){return 1;}
  psi0.position(1);
  psi0.normalize();
  // auto neintval = (itensor::innerC(psi0, Neint, psi0));
  //double neint=real(neintval);
 		 auto mpsName=filename;
	 	 
	 	         auto ampo = AutoMPO(sites);
	     	 ampo += 1,"N",Llead1+1;
	 auto O=toMPO(ampo);
	 std::cout<< "on site i exp "<< Llead1+1 << " is "<< itensor::innerC(psi0, O, psi0)<<std::endl;
	// }
		 mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
		 // Many_Body::ToFile<std::vector<double>>({neint}, "Neint"+mpsName,1);
		 //		 std::cout<< "tot pol "<< neint<<std::endl;
 		 if(SV){		
 	       itensor::writeToFile("MPS"+filename,psi0);
 		       itensor::writeToFile("siteset"+filename,sites);
  		 }
     return 0;
}
