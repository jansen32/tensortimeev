#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include<iostream>
	
using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();

    double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
    int Mph{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  bool RDM{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
    double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string filename="hetstparalborev";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("RDM", boost::program_options::value(&RDM)->default_value(false), "RDM");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << Llead2 << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << Lchain << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 3);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 3);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 3);
      		filename+=seps0;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << V << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << tot << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << dt << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		       	     if (vm.count("Md"))
      {      std::cout << "Md: " << Md << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << lboMd << '\n';
      	slboMd="lboMd"+std::to_string(lboMd);
      	filename+=slboMd;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << cutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       		 if (vm.count("lbocut"))
      {      std::cout << "lbocutoff: " << lbocutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 scutoff="lbocut"+ss.str();
      	filename+=scutoff;
      }
      	     if (vm.count("RDM"))
      {      std::cout << "RDM: " << RDM << '\n';

      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	filename+=".bin";
  std::cout<<filename<<std::endl;

  int N=Llead1+Llead2+Lchain;
  
  std::vector<int> v(N, 0);
  for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",true);

    auto sites = itensor::Holstein(N, argsState);
 
    auto H1=itensor::makeHetHam(sites, Llead1, Llead2,  Lchain, tl, tint, t0, gamma, omega, eps0 );
    auto H2=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, -V/2, tl, tint, t0, gamma, omega, eps0);

    auto state = itensor::InitState(sites);
    for(int i=1; i<=N; i+=1)
      {
 	if(i%2==0){state.set(i, "Occ");}
      }

    auto psi = MPS(state);
      psi.position(1);
     psi.normalize();
     //         Print(psi);
    
  	 auto sweeps = itensor::Sweeps(15);
    //very important to use noise for this model
	  sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
	  sweeps.maxdim() = 10,20, 80,200,300, 400, 400, 500, 500;
	 sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-09, 1E-09;
    std::cout<< "start "<< std::endl;
     auto [energy,psi0] = itensor::dmrg(H2,psi,sweeps,{"Quiet=",true});
 psi.position(1);
     psi.normalize();
        itensor::printfln("Ground State Energy1 = %.12f",energy);
auto args = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",1e-10,"MaxDim=",3000);


 auto H1sqrd=itensor::nmultMPO(prime(H2), H2, args2);
 auto E0=itensor::innerC(psi0, H2,psi0);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psi0,H1sqrd,psi0);

	  auto VAL=E0*E0;
		    auto VARIANCE =  (E0Sqrd-VAL )/E0Sqrd;
		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
		    std::cout<< "THE GS VAR WAS "<< (E0Sqrd-VAL )<<std::endl;
    // itensor::writeToFile("GS",psi0);
    //  auto psi0=itensor::readFromFile<MPS>("GS",sites);
    //itensor::printfln("Ground State Energy = %.12f",energy);
  psi0.position(1);
  psi0.normalize();
  std::cout<< "stop 1"<< std::endl;
  auto Curr=MPO(itensor::j(sites, Llead1,Llead2, Lchain, tint ));
   std::vector<double> obstebd;
   std::vector<double> Xvec;
      std::vector<double> Pvec;
         std::vector<double> Edotvec;
   std::vector<double> nphvec;
      std::vector<double> nevec;
  std::vector<double> time;
  std::map<std::string, double> param;
   std::cout<< "stop "<< std::endl;
 param.insert({"t0",t0});
 param.insert({"tl",tl});
 param.insert({"tint",tint});
 param.insert({"V",0});
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"eps0",eps0});


 

  int i=0;

  itensor::IHGdot GM(sites, gates, dt, param, Llead1,Llead2, Lchain);
  auto psiv=itensor::vMPS(psi0);
 
  itensor::TimeEvolveLbo< itensor::IHGdot, decltype(psiv), false> TE(GM,  psiv, N,  argsMPS, argsState);

 TE.makeLboOneSite(Llead1+1);

     while(dt*i<tot)
    {
      

     std::cout<< "START2121 "<<std::endl;
   
	auto psir2=TE.getBareMpsOneSite(Llead1+1);
    	 psir2.position(Llead1+1);
    	 psir2.normalize();
	 

        auto ket = psir2(Llead1+1);
        auto bra = dag(prime(ket,"Site"));

        auto Nph = op(sites,"Nph",Llead1+1);
	        auto Ne = op(sites,"n",Llead1+1);
		auto X = op(sites,"Bdag",Llead1+1)+op(sites,"B",Llead1+1);
		auto P =itensor::Cplx_i*(op(sites,"Bdag",Llead1+1)-op(sites,"B",Llead1+1));
		auto Edot=Nph*omega+gamma*op(sites,"NBdag",Llead1+1)+gamma*op(sites,"NB",Llead1+1)+eps0*op(sites,"n",Llead1+1);
        //take an inner product 
        auto nph = eltC(bra*Nph*ket);
	        auto ne = eltC(bra*Ne*ket);
	auto x = eltC(bra*X*ket);
	auto p= eltC(bra*P*ket);
		auto edot= eltC(bra*Edot*ket);
psir2.position(Llead1+1);
 if(RDM and (i%100==0 or i==0)){
 auto reddm=ket*bra;
 auto [U,D] = diagHermitian(reddm, {"ShowEigs" , true});
 Print(norm(reddm-dag(U)*D*prime(U)));
 U=dag(U);
 auto ii =D.inds();
 auto j =U.inds();
 std::vector<double> ev0;
  std::vector<double> ev1;
 Print(j[0]);
 Print(j[1]);
 std::vector<std::complex<double>> evecn01;
 std::vector<std::complex<double>> evecn11;
  std::vector<std::complex<double>> evecn02;
 std::vector<std::complex<double>> evecn12;
  std::vector<std::complex<double>> evecn03;
 std::vector<std::complex<double>> evecn13;
  std::vector<std::complex<double>> evecn04;
 std::vector<std::complex<double>> evecn14;
for(int l=1; l<=int(dim(j[0])/2); l++)
   {
     ev0.push_back(itensor::elt(D, ii[0](l),ii[1](l) ));
     ev1.push_back(itensor::elt(D, ii[0](int(dim(j[0])/2)+l),ii[1](int(dim(j[0])/2)+l) ));
     evecn01.push_back(itensor::eltC(U, j[0](l),j[1](1) ));
     evecn11.push_back(itensor::eltC(U, j[0](int(dim(j[0])/2)+l), j[1](int(dim(j[1])/2)+1 )));
     evecn02.push_back(itensor::eltC(U, j[0](l),j[1](2) ));
     evecn12.push_back(itensor::eltC(U, j[0](int(dim(j[0])/2)+l), j[1](int(dim(j[1])/2)+2 )));
          evecn03.push_back(itensor::eltC(U, j[0](l),j[1](3) ));
     evecn13.push_back(itensor::eltC(U, j[0](int(dim(j[0])/2)+l), j[1](int(dim(j[1])/2)+3 )));
     evecn04.push_back(itensor::eltC(U, j[0](l),j[1](4) ));
     evecn14.push_back(itensor::eltC(U, j[0](int(dim(j[0])/2)+l), j[1](int(dim(j[1])/2)+4 )));
	      std::cout<< std::setprecision(4)<<itensor::eltC(U, j[0](int(dim(j[0])/2)+l), j[1](int(dim(j[1])/2)+1))<< ' ';    

 std::cout<< '\n';
   }
 Many_Body::bin_write("DM/OM0t"+std::to_string(dt*i).substr(0, 6)+filename, ev0);
 Many_Body::bin_write("DM/OM1t"+std::to_string(dt*i).substr(0, 6)+filename, ev1);
    Many_Body::bin_write("DM/reddmn11t"+std::to_string(dt*i).substr(0, 6)+filename, evecn11);
    Many_Body::bin_write("DM/reddmn01t"+std::to_string(dt*i).substr(0, 6)+filename, evecn01);
        Many_Body::bin_write("DM/reddmn12t"+std::to_string(dt*i).substr(0, 6)+filename, evecn12);
    Many_Body::bin_write("DM/reddmn02t"+std::to_string(dt*i).substr(0, 6)+filename, evecn02);
        Many_Body::bin_write("DM/reddmn13t"+std::to_string(dt*i).substr(0, 6)+filename, evecn13);
    Many_Body::bin_write("DM/reddmn03t"+std::to_string(dt*i).substr(0, 6)+filename, evecn03);
        Many_Body::bin_write("DM/reddmn14t"+std::to_string(dt*i).substr(0, 6)+filename, evecn14);
    Many_Body::bin_write("DM/reddmn04t"+std::to_string(dt*i).substr(0, 6)+filename, evecn04);
 }
 // writeToFile("DM/redmEVt"+std::to_string(dt*i).substr(0, 6)+filename,U);
 //writeToFile("DM/OMEVt"+std::to_string(dt*i).substr(0, 6)+filename,D);
 //Print(reddm);

    		 
		 
    		 auto c = (itensor::innerC(psir2, Curr, psir2))/(itensor::innerC(psir2,  psir2));
    		  if(std::abs(imag(c))>1E-14){std::cout<< " errror in obs"; std::cout<<std::abs(imag(c))<<std::endl; }
		  
    		    double avl2=real(c);
    		  time.push_back(i*dt);
    		  obstebd.push_back(avl2);
		  Xvec.push_back(real(x));
		  Pvec.push_back(real(p));
		  Edotvec.push_back(real(edot));
		  nphvec.push_back(real(nph));
		  nevec.push_back(real(ne));
    		 //psi0.position(1);
    		 //	 psi0.normalize();
		  if(maxLinkDim(psi0)>=Md-100)
		    {
      Many_Body::bin_write("time"+filename, time);
    Many_Body::bin_write("J"+filename, obstebd);
    Many_Body::bin_write("X"+filename, Xvec);
        Many_Body::bin_write("Edot"+filename, Edotvec);
	    Many_Body::bin_write("P"+filename, Pvec);
    Many_Body::bin_write("Nph"+filename, nphvec);
    Many_Body::bin_write("Ne"+filename, nevec);

    std::cout<< "finish "<<std::endl;
break;
		    }
    		  std::cout<<c<< "  "<< std::setprecision(12)<<dt*i << "  E "<< innerC(psir2, H1, psir2)<< "  "<< "lbo max "<< TE.maxLboDim()<<"   "<< " mx  "<< maxLinkDim(psi0)<<" av  "<< averageLinkDim(psi0)<<"  "<<"\n";
      
       	   TE.lboTEBDStepO2paraOneSite(Llead1+1);
	   i++;
	       		 	  }
     Print(psi0.A(Llead1));
     Print(psi0.A(Llead1+1));
     Print(psi0.A(Llead1+2));
     // Print(psi0);
      Many_Body::bin_write("time"+filename, time);
    Many_Body::bin_write("J"+filename, obstebd);
    Many_Body::bin_write("X"+filename, Xvec);
        Many_Body::bin_write("Edot"+filename, Edotvec);
	    Many_Body::bin_write("P"+filename, Pvec);
    Many_Body::bin_write("Nph"+filename, nphvec);
    Many_Body::bin_write("Ne"+filename, nevec);
     psi0.position(1);
     std::cout<< " norm "<< norm(psi0)<< std::endl;
     return 0;
}
