#include"../src/exapp.hpp"
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/specfunc.hpp"
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
  int i{0};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string sj{};
  std::string si{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};

  std::string filename="hetst";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(10), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      if (vm.count("j"))
      {
	i=int(L/2)+1;

      	si="i"+std::to_string(i);
      	filename+=si;
	std::cout << "j: " << vm["j"].as<int>() << '\n';
      	sj="j"+std::to_string(j);
      	filename+=sj;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << vm["V"].as<double>() << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 3);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
  auto sites = Fermion(L,{"ConserveNf",true} );
 auto N=L;
auto ampo = AutoMPO(sites);
 auto ne = AutoMPO(sites);
for(int j = 1; j < N; ++j)
    {
    ampo += -t0,"Cdag",j,"C",j+1;
    ampo += -t0,"Cdag",j+1,"C",j;
    ne+=1,"n",j;
    }
     ne+=1,"n",N;
auto H = toMPO(ampo);
 auto Ne = toMPO(ne);

    auto state = itensor::InitState(sites);
    for(int i=1; i<=N; i+=1)
      {
if(i%2==0){state.set(i, "Occ");}
      }
    //	state.set(1, "Occ");
    auto psi = MPS(state);
      psi.position(1);
     psi.normalize();
         Print(psi);
    
    	   	 auto sweeps = itensor::Sweeps(7);
    //very important to use noise for this model
	 sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
	 sweeps.maxdim() = 10,20, 80,200,300, 400, 400;
	 sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-13, 1E-13;
    auto [energy,psi0] = itensor::dmrg(H,psi,sweeps,{"Quiet=",true});
 psi0.position(1);
     psi0.normalize();
        itensor::printfln("Ground State Energy1 = %.12f",energy);
auto args = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000);


 auto H1sqrd=itensor::nmultMPO(prime(H), H, args2);
 auto E0=itensor::innerC(psi0, H,psi0);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psi0,H1sqrd,psi0);

 auto VAL=E0*E0;
 auto VARIANCE =  (E0Sqrd-VAL )/E0Sqrd;
 std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 std::cout<< "THE GS VAR WAS "<< (E0Sqrd-VAL )<<std::endl;
//Make a Fermion site set which only conserves parity

  auto taua = dt/2.*(1.+1._i);
  auto taub = dt/2.*(-1.+1._i);
   auto  expHb = toExpH(ampo, taub);
   auto expHa = toExpH(ampo,taua);

   auto expH =toExpH(ampo,dt*Cplx_i);

   auto args3 = Args("Method=","DensityMatrix","Cutoff=",cutoff,"MaxDim=",Md, "Normalize", false);
 
  auto psi01=psi0;
  auto psi01x=psi0;
  int n=0;

  std::cout<< " i  "<< i << " and j "<< j<<std::endl;
  applyCdag(psi01x, j, sites);
  applyC(psi01, i, sites);
   auto psi02=psi0;
    auto psi02x=psi0;
     applyCdag(psi02x, i, sites);
  applyC(psi02, j, sites);
  itensor::exApp E(psi01, dt, ampo, args3);
itensor::exApp E2(psi01x, -dt, ampo, args3);
 std::vector<std::complex<double>> cdagc;
 std::vector<std::complex<double>> ccdag;
  std::vector<double> t;
  
 while(n*dt<tot)
   {
  
 std::complex<double> CDAGCval=innerC(psi02, psi01);
  std::complex<double> CCDAGval=innerC(psi02x, psi01x);
    std::cout<< " f1 "<< innerC(psi02,Ne, psi02)<<" max "<< maxLinkDim(psi01)<<" av  "<< averageLinkDim(psi01)<< std::endl;
  std::cout<< " f2 "<< innerC(psi02x,Ne, psi02x)<<" max "<< maxLinkDim(psi01x)<<" av  "<< averageLinkDim(psi01x)<<std::endl;
  std::cout<< " f1 "<< innerC(psi02,Ne, psi02)<<" max "<< maxLinkDim(psi02)<<" av  "<< averageLinkDim(psi02)<< std::endl;
  std::cout<< " f2 "<< innerC(psi02x,Ne, psi02x)<<" max "<< maxLinkDim(psi02x)<<" av  "<< averageLinkDim(psi02x)<<std::endl;
 std::cout<< n*dt<<" overlapCDAGC "<< CDAGCval<<std::endl;
  std::cout<< n*dt<<" overlapCCDAG "<< CCDAGval<<std::endl;
  std::cout<< n*dt<<" overlapCDAGC times extra t "<< CDAGCval*std::exp(1._i*E0*double(n*dt))<<std::endl;
  std::cout<< n*dt<<" overlapCCDAG times extra t "<< CCDAGval*std::exp(-1._i*E0*double(n*dt))<<std::endl;
  cdagc.push_back(CDAGCval);
  ccdag.push_back(CCDAGval);
   t.push_back(double(n*dt));
      E.apply();
      E2.apply();
     //	  psi01.noPrime().normalize();
        n++;
   }
    Many_Body::bin_write("CDAGCMPSFer"+filename,cdagc);
    Many_Body::bin_write("CCDAGMPSFer"+filename, ccdag);
    Many_Body::bin_write("timeMPSFer"+filename, t);
  return 0;
}
