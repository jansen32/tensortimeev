#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include<iostream>
	
using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
    
  using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;

  double cutoff{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  double t0{};
  double tl{};
  double tint{};
  double U{};
   double ep0{};
  double dt{};
  double tot{};
  double V{};
  std::string scutoff{};
  std::string sM{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string sU{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
  std::string filename="hetst";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("U", boost::program_options::value(&U)->default_value(1.0), "U")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << vm["Ll1"].as<int>() << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << vm["Ll2"].as<int>() << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << vm["Lc"].as<int>() << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << vm["tl"].as<double>() << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 3);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << vm["tint"].as<double>() << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 3);
      		filename+=stint;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << vm["V"].as<double>() << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 3);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	filename+=".bin";
  std::cout<<filename<<std::endl;

  size_t N=Llead1+Llead2+Lchain;
  std::vector<int> v(N, 0);
  auto sites = itensor::Fermion(N);
  

  //itensor::readFromFile("MottSites.bin", sites);

    
 
    auto H1=itensor::makeHetFerHam(sites, Llead1, Llead2, Lchain, tl, tint, t0);
    auto H2=itensor::makeHetFerHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, 0, 0);

    auto state = itensor::InitState(sites);
    for(int i=1; i<=N; i+=1)
      {
		if(i%2==0){state.set(i, "Occ");}
  	

      }
    //state.set(3, "Occ");
    auto psi = MPS(state);

      
    //itensor::randomMPS(state);
      psi.position(1);
     psi.normalize();
         Print(psi);
  	 std::cout<< "start "<< std::endl;
  	 auto sweeps = itensor::Sweeps(40);
    //very important to use noise for this model
    sweeps.noise() = 1E-6,1E-6,1E-8, 1E-10,  1E-12;
    sweeps.maxdim() = 10,20,100,100,100;
    sweeps.cutoff() = 1E-14;
     auto [energy,psi0] = itensor::dmrg(H1,psi,sweeps,{"Quiet=",true});
    //  itensor::writeToFile("MottGS.bin",psi0);
    //  itensor::writeToFile("MottSites.bin",sites);
     

    //     or::readFromFile<MPS>("GS",sites);
      itensor::printfln("Ground State Energy = %.12f",energy);
 //        auto psi0=MPS(state);
   psi0.position(1);
 psi0.normalize();

      auto Curr=MPO(itensor::jFer(sites, Llead1, Llead2, Lchain, tint ));
 //   std::vector<double> obstebd;
 //  std::vector<double> time;
 // std::map<std::string, double> param;
 // param.insert({"t0",t0});
 // param.insert({"tl",tl});
 // param.insert({"tint",tint});
 // param.insert({"V",V});
 // param.insert({"ep0",ep0});
 // param.insert({"U",U});
   std::vector<double> obstebd;
  std::vector<double> time;

 auto args = itensor::Args("Method=","DensityMatrix","Cutoff=",cutoff,"MaxDim=",3000);
  itensor::exApp E(psi0, dt, H2, args);
   int i=0;
 //   auto M=AutoMPO(sites);
    auto psiGS=psi0;

     while(dt*i<tot)
    {

      	 E.apply();

  		 i++;
 		 	
		 auto c = (itensor::innerC(psi0, Curr, psi0));
		 
  		  if(std::abs(imag(c))>1E-14){std::cout<< " errror in obs"; std::cout<<std::abs(imag(c))<<std::endl; }
		  double avl2=real(c);
  		  time.push_back(i*dt);
  		  obstebd.push_back(avl2);
		  std::cout<<c << "  "<< std::setprecision(12)<<dt*i <<"  E "<< innerC(psi0, H1, psi0)<< "  "<<" mx  "<< maxLinkDim(psi0)<<" av  "<< averageLinkDim(psi0)<<"\n";
  	//<<maxM(psi)<< "  "<< norm(psi)<<std::endl;
       	  
 		 	  }
 //     // Print(psi0);
      Many_Body::bin_write("extimeFermi"+filename, time);
    Many_Body::bin_write("exJFermi"+filename, obstebd);
     return 0;
}
