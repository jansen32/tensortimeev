#include"itensor/all.h"
#include<vector>
#include"../src/holstein.hpp"
#include"../src/owntime.h"
#include"files.hpp"
using namespace std;
using namespace itensor;
int main(int argc, char *argv[])
{

  size_t N=12;
  double gamma=-1.1;
    double omega=1;
      double t0=1;

  auto sites = Holstein(N);
  auto state = InitState(sites);
  
  auto H=  makeHolstein(N);
  state.setAll("Emp");
 state.set(1, "Occ");
 auto psi = MPS(state);


   int j=1;
    psi.position(1);
     psi.normalize();
 auto psi0 = psi;
 
 
    ITensor ket = psi.A(j);
//   ITensor ket1 = psi0.A(j);
  
   ITensor bra = dag(prime(ket,Site));
  
// // //Time evolve, overwriting psi when done
 
   //auto avl = (bra*OBS*ket).real();
   //std::cout<<avl<< std::endl;
  Real tstep = 0.01; //time step (smaller is generally more accurate)
  Real ttotal = 2.0; //total time to evolve
  Real cutoff = 1E-8; //truncation error cutoff when restoring MPS form
  using Gate = BondGate<ITensor>;
  auto gates = vector<Gate>();
  auto ampo = AutoMPO(sites);
   for(int b = 1; b < N; ++b)
     {


        ampo += -t0,"Cdag",b+1,"C",b;
        ampo +=  -t0,"Cdag",b,"C",b+1;
      }
      for(int b = 1; b < N; ++b)
     {


       //   ampo += omega,"Nph",b;
    
   
      }
   auto OBS = MPO(ampo);
  for(int b = 1; b <= N-1; ++b)
    {
      auto hterm = -t0*sites.op("Cdag",b+1)*sites.op("C",b);
      hterm += -t0*sites.op("Cdag",b)*sites.op("C",b+1);
      hterm += 0.5*gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      hterm += 0.5*gamma*sites.op("NB",b)*sites.op("Id", b+1);
      hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NB",b+1);
      hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NBdag",b+1);
      hterm += 0.5*omega*(sites.op("Nph",b)*sites.op("Id", b+1));
      hterm += 0.5*omega*sites.op("Id", b)*(sites.op("Nph",b+1));
      if(b==N-1)
	{
	  hterm += 0.5*omega*sites.op("Id", b)*(sites.op("Nph",b+1));
	  hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NB",b+1);
	  hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NBdag",b+1);
	}
      if(b==1)
	{  hterm += 0.5*gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      hterm += 0.5*gamma*sites.op("NB",b)*sites.op("Id", b+1);
  hterm += 0.5*omega*(sites.op("Nph",b)*sites.op("Id", b+1));
	}

    auto g = Gate(sites,b,b+1,Gate::tReal,tstep/2., hterm);
    gates.push_back(g);
    }

  for(int b = N-1; b >= 1; --b)
    {
      auto hterm = -t0*sites.op("Cdag",b+1)*sites.op("C",b);
      hterm += -t0*sites.op("Cdag",b)*sites.op("C",b+1);
      hterm += 0.5*gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      hterm += 0.5*gamma*sites.op("NB",b)*sites.op("Id", b+1);
      hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NB",b+1);
      hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NBdag",b+1);
      hterm += 0.5*omega*(sites.op("Nph",b)*sites.op("Id", b+1));
      hterm += 0.5*omega*sites.op("Id", b)*(sites.op("Nph",b+1));
      if(b==N-1)
	{
	  hterm += 0.5*omega*sites.op("Id", b)*(sites.op("Nph",b+1));
	  hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NB",b+1);
	  hterm += 0.5*gamma*sites.op("Id", b)*sites.op("NBdag",b+1);
	}
      if(b==1)
	{  hterm += 0.5*gamma*sites.op("NBdag",b)*sites.op("Id", b+1);
      hterm += 0.5*gamma*sites.op("NB",b)*sites.op("Id", b+1);
  hterm += 0.5*omega*(sites.op("Nph",b)*sites.op("Id", b+1));
	}
      auto g = Gate(sites,b,b+1,Gate::tReal,tstep/2.,hterm);
      gates.push_back(g);
    }
 
  //Save initial state;
  std::vector<double> obs;
  std::vector<double> time;
  int i=0;  
  while(i*tstep*omega<=25)
    {
//  // psi.position(j);
//  //  psi0.position(j);
       gateTEvolOneStep(gates,ttotal,tstep,psi,{"Cutoff=",cutoff,"Verbose=",true});
//    ITensor ket = psi.A(j);
// //   ITensor ket1 = psi0.A(j);
//    ITensor bra = dag(prime(ket,Site));
 
// // //Time evolve, overwriting psi when done
 
   auto avl = overlap(psi, OBS, psi);
 obs.push_back(avl/omega);
  time.push_back(tstep*i*omega);
  std::cout<<obs[i]<< "  " << time[i]<<std::endl;
  i+=1;
    }
 Many_Body::bin_write("datalam0.bin", obs);
 Many_Body::bin_write("timelam0.bin", time);
//  //printfln("Maximum MPS bond dimension after tim e evolution is %d",maxM(psi));
 // Print(overlapC(psi, obs, psi0));
  return 0;
}

