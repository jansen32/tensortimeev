#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include <iostream>
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include <boost/program_options.hpp>
#include<iostream>
#ifdef ITENSOR_USE_TBB
#include "tbb/task_scheduler_init.h"	
#endif
//#include "tbb/task_scheduler_init.h"		
using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
  using itensor::MPO;
  using itensor::MPS;
#ifdef ITENSOR_USE_TBB
   tbb::task_scheduler_init init(6);
#endif

  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
  int maxSweeps{};
    double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
    int Mph{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int LH{};
  bool saveNr=false;
  bool RDM{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double lambda={};
  double V{};
  bool SV{false};
  bool left{false};
    double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string sleft{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLH{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
   std::string slambda{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string filename="intGS";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("LH", boost::program_options::value(&LH)->default_value(4), "LH")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("SV", boost::program_options::value(&SV)->default_value(false), "SV")
      ("left", boost::program_options::value(&left)->default_value(false), "left")
      ("MS", boost::program_options::value(&maxSweeps)->default_value(20), "MS");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("LH"))
      {      std::cout << "LH: " << LH << '\n';
      	sLH="LH"+std::to_string(vm["LH"].as<int>());
	filename+=sLH;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
   if (vm.count("left"))
      {      std::cout << "left: " << left << '\n';
      	sleft="left"+std::to_string(vm["left"].as<bool>());
      	filename+=sleft;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 5);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 5);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 5);
      		filename+=seps0;
      }
	
      
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	filename+=".bin";
  std::cout<<filename<<std::endl;

  int N=Llead1+LH;
  if(Llead1!=LH)
    {std::cout<< "the time evolution only works for equal Ll1 and LH due to efficiency"<<std::endl;
      return 0;}  
  std::vector<int> v(N, 0);
  // is stored as vecto so start site-1
  for(int i=Llead1; i<v.size(); i++)
    {
      v[i]=M;

    }

  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
    auto sites = itensor::Holstein(N, argsState); 
    auto H1=itensor::makeIntHam(sites, Llead1, LH, tl, tint, t0, gamma, omega, eps0);

    // setting electrons either on the left or the right side
    auto state = itensor::InitState(sites);
    int n_elec=int((Llead1+LH)/2);
    
    std::cout<< "started with "<< n_elec<<std::endl;
    if(left)
      {
int start_site=1;
    while(n_elec>0)
      {
 	state.set(start_site, "Occ");
	std::cout<<"put e nr  "<<n_elec << " on "<< start_site<<std::endl;
	start_site++;      
	n_elec--;
}
      }
    else{
int start_site=N;
    while(n_elec>0)
      {
 	state.set(start_site, "Occ");
	std::cout<<"put e nr  "<<n_elec << " on "<< start_site<<std::endl;
	start_site--;      
	n_elec--;
}
    }


    auto psi = MPS(state);
      psi.position(1);
     psi.normalize();
     //         Print(psi);
    
  	 auto sweeps = itensor::Sweeps(maxSweeps);
    //very important to use noise for this model
	  sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
	  sweeps.maxdim() = 10,20, 80,200,300, 400, 400, 500, 500;
	  sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-09, 1E-09,1E-10, 1E-10;
    std::cout<< "start "<< std::endl;
    auto [energy,psi0] = itensor::dmrg(H1,psi,sweeps,{"Quiet=",true});
 psi0.position(1);
     psi0.normalize();
        itensor::printfln("Ground State Energy1 = %.12f",energy);
auto args = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);

  auto psiOne=applyMPO(H1,psi0, args2);

 auto E0=itensor::innerC(psi0, H1,psi0);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psiOne,psiOne);
 std::cout<< " djei "<< E0Sqrd<<std::endl;
 	  auto VAL=E0*E0;
 std::cout<< " djei 2 2 "<< VAL<<std::endl;
 auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
 		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
    auto Neint=MPO(itensor::Neint(sites, Llead1 ));

   
 		    if(std::abs((E0Sqrd-VAL )/E0Sqrd)>1e-04){return 1;}
 		    if(std::abs(E0Sqrd-VAL)>1e-04){return 1;}
  psi0.position(1);
  psi0.normalize();
 		 auto neintval = (itensor::innerC(psi0, Neint, psi0));
 		 double neint=real(neintval);
 		 auto mpsName=filename;
		 for(int i=1; i<=N; i++)
		   {        auto ampo = AutoMPO(sites);
	
      	ampo += 1,"N",i;
	auto O=toMPO(ampo);
	std::cout<< "on site i exp "<< i << " is "<< itensor::innerC(psi0, O, psi0)<<std::endl;
	}
		 mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
 		 Many_Body::ToFile<std::vector<double>>({neint}, "Neint"+mpsName,1);
		 std::cout<< "tot pol "<< neint<<std::endl;
 		 if(SV){		
 	       itensor::writeToFile("MPS"+filename,psi0);
 		       itensor::writeToFile("siteset"+filename,sites);
 		 }
     return 0;
}
