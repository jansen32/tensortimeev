
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/GF.hpp"
using namespace std;
using namespace itensor;
// saves psiR=e^{-iHt/2}\psi_0>
// saves psiL=e^{iHt/2}\psi_0>
int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};

  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
  std::string loadDir{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string T{};

  // file nyme where I will load states from
  std::string filename="";
  // filename to which we write results
  std::string filenameNew="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("DIR", boost::program_options::value(&loadDir)->default_value("."), "DIR")
      ("T", boost::program_options::value(&T)->default_value("0.10"), "T")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
	filenameNew+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      	filenameNew+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      	filenameNew+=sMph;
      }
      	     	     if (vm.count("j"))
      {      std::cout << "j: " << vm["j"].as<int>() << '\n';
      	sj="i"+std::to_string(int(L/2)+1)+"j"+std::to_string(j);
      	filenameNew+=sj;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
	filenameNew+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      	filenameNew+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      	filenameNew+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      	filenameNew+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
		filenameNew+=somega;
      }

      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
		filenameNew+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      		filenameNew+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      	filenameNew+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      	filenameNew+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
	filenameNew+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
loadDir+="/SVMPS";
 if(T.length()!=4){
    std::cout<< "T was wrong format, you eneterd  "<< T << std::endl;
    return 1;}
 loadDir+="T"+T+"/";
 std::cout<< "loading from "<<loadDir<<std::endl;
    auto sites = readFromFile<Holstein>(siteSetName);
     auto psi = readFromFile<MPS>(mpsName);
//     //print(psi);
   using Gate = BondGate;
    auto gates = vector<Gate>();
    int N=itensor::length(sites);
   std::vector<int> v(N, M);
   itensor::Args argsState={"ConserveNf=",true,
                              "ConserveNb=",false,
 			   "DiffMaxOcc=",true, "MaxOccVec=", v};
   auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",false);


  std::map<std::string, double> param;
  param.insert({"t0", t0});
  param.insert({"gamma", gamma});
   param.insert({"omega", omega});
 IHG GM(sites, gates, -dt*0.5, param);


//    std::chrono::duration<double> elapsed_seconds;
//  std::chrono::time_point<std::chrono::system_clock> start, end;
   

 GM.makeFTGatesAux();



 TimeEvolveLbo< IHG, decltype(psi), false> C1(GM,  psi, N,  argsMPS, argsState);

  std::string newFile=mpsName;  
      newFile.insert(0, filenameNew); 
      mpsName.insert(0, filename); 
 
   mpsName.insert(0, "tdmrglboAux");
newFile.insert(0, "tdmrglboAux");

 newFile.replace(newFile.end()-4, newFile.end(), ".dat");
  j*=2;
  j--;


 int n=0;
 const std::string obsname="CCDAGt";
     applyCdag(psi, j, sites);
       C1.makeLbo();
 	 auto ne = AutoMPO(sites);
 	 for(int l = 1; l < N; l+=2)
    {

    ne+=1,"n",l;
    }

	 auto Ne = toMPO(ne);
  // std::cout<<"start "<< itensor::norm(psi)<<std::endl;
        while(n*dt<tot)
       {
 	  auto psi1x=C1.getBareMps();
 	  psi1x.position(1);
 	  double n1=norm(psi1x);
	  std::string sdtn=std::to_string(n*dt).substr(0, 6);
   auto psiBra = readFromFile<MPS>(loadDir+"psiBradt"+sdtn+mpsName);
 	  std::complex<double> CCDAGnoNormKarval=innerC(psiBra, psi1x);
 	std::vector<std::complex<double>> ccdag;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
 	    t.push_back(double(n*dt));
 	    avBd.push_back(averageLinkDim(psi));
 	    maxBd.push_back(maxLinkDim(psi));
 	    lbomx.push_back(C1.maxLboDim());
 	    ccdag.push_back(CCDAGnoNormKarval);
       std::string DIR=obsname+"noNormKarGF/";
       auto vecSize=t.size();
       Many_Body::ToFile(ccdag, DIR+obsname+"noNormKarGF"+newFile,vecSize);
       Many_Body::ToFile(t, DIR+"time"+obsname+"noNormKarGF"+newFile, vecSize);
       Many_Body::ToFile(lbomx, DIR+"lbomx"+obsname+"noNormKarGF"+newFile, vecSize);
       Many_Body::ToFile(avBd, DIR+"avBd"+obsname+"noNormKarGF"+newFile, vecSize);
       Many_Body::ToFile(maxBd, DIR+"maxBd"+obsname+"noNormKarGF"+newFile, vecSize);
	    	    
 	 
 	    // /NORM
 	    std::cout<< " f1 "<< innerC(psi1x,Ne, psi1x)/innerC(psi1x,psi1x)<<'\n';

 	       std::cout<< n*dt<<" overlap"+obsname+"KarnoNorm "<< CCDAGnoNormKarval<<"lbo max psi1 "<< C1.maxLboDim()<<"   "<< " mx psi1  "<< maxLinkDim(psi)<<" av psi1  "<< averageLinkDim(psi)<<  " Norm 1.1= "<< norm(psi)<<" Norm 1.2= "<<n1 <<"\n";

 // if( ( maxLinkDim(psi) >(C1.maxBd-100)) or (C1.maxLboDim()>=2*(M)-4) )
 //   {

 //     std::cout<< "to large "<< std::endl;
 //     break;

 //   }
  n++;
   C1.lbotDmrgStep();

       }

  return 0;
}
