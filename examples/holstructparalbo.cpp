#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include<iostream>
#ifdef ITENSOR_USE_TBB
#include "tbb/task_scheduler_init.h"		

#endif
using namespace std;

using namespace itensor;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{

#ifdef ITENSOR_USE_TBB
 tbb::task_scheduler_init init(10);
#endif

  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
std::string mpsName{};
 std::string siteSetName{};

    double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
    int Mph{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  bool saveNr=false;
  bool RDM{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  // double lambda{};
  double dt{};
  double tot{};
  double V{};
    double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  // std::string slambda{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      // ("lam", boost::program_options::value(&lambda)->default_value(std::sqrt(2)), "lam")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("RDM", boost::program_options::value(&RDM)->default_value(false), "RDM");
    //    ("saveNr", boost::program_options::value(&saveNr)->default_value(false), "saveNr");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
    if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << Llead2 << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << Lchain << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
   {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 3);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 3);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 4);
      		filename+=seps0;
      }
			// 	 if (vm.count("lam"))
      // {      std::cout << "lambba: " << lambda << '\n';
      // 	slambda="lambda"+std::to_string(vm["lam"].as<double>()).substr(0, 3);
      // 		filename+=slambda;
      // }
      		 if (vm.count("V"))
      {      std::cout << "V: " << V << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << tot << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << dt << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		       	     if (vm.count("Md"))
      {      std::cout << "Md: " << Md << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << lboMd << '\n';
      	slboMd="lboMd"+std::to_string(lboMd);
      	filename+=slboMd;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << cutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       		 if (vm.count("lbocut"))
      {      std::cout << "lbocutoff: " << lbocutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 scutoff="lbocut"+ss.str();
      	filename+=scutoff;
      }
      	     if (vm.count("RDM"))
      {      std::cout << "RDM: " << RDM << '\n';

      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  //	filename+=".bin";
  std::cout<<filename<<std::endl;
  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi0 = itensor::readFromFile<MPS>(mpsName);
  size_t N=length(psi0);
  std::vector<int> v(N, 0);
  // is stored as vecto so start site-1
for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",true);

  psi0.position(1);
  psi0.normalize();
  std::cout<< "stop 1"<< std::endl;
    auto H2=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, gamma, omega, eps0);
  auto Curr=MPO(itensor::j(sites, Llead1,Llead2, Lchain, tint ));
  auto CurrE=MPO(itensor::jEdot(sites, Llead1,Llead2, Lchain, tl , V/2));
  auto EL=MPO(itensor::ELdot(sites, Llead1,Llead2, Lchain, tl, V/2 ));
  auto ER=MPO(itensor::ERdot(sites, Llead1,Llead2, Lchain, tl, V/2 ));
  auto EHYB=MPO(itensor::EHYBdot(sites, Llead1,Llead2, Lchain, tint ));
  std::map<std::string, double> param;
   std::cout<< "stop "<< std::endl;
 param.insert({"t0",t0});
 param.insert({"tl",tl});
 param.insert({"tint",tint});
 param.insert({"V",V/2}); // now using -V
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"eps0",eps0});
 //  param.insert({"lambda",lambda});


 

  int i=0;


  itensor::IHGdot GM(sites, gates, dt, param, Llead1,Llead2, Lchain);
  auto psiv=itensor::vMPS(psi0);
 
   itensor::TimeEvolveLbo< itensor::IHGdot, decltype(psiv), false> TE(GM,  psiv, N,  argsMPS, argsState);

 TE.makeLboOneSite(Llead1+1);

 size_t mps_pos=mpsName.find("MPS");
 mpsName=mpsName.substr(mps_pos);

  mpsName.insert(0, filename);

  mpsName.insert(0, "dottebdlbopara");
  std::string dirName=mpsName;
  dirName.replace(dirName.end()-4, dirName.end(), "dir");
  boost::filesystem::path dir(dirName);
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success " << std::endl;
}	     
	     else{
dir=boost::filesystem::path(dirName+"2");
 if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success second attempt" << std::endl;
}
 else{ std::cout<< "could not make dir error "<<std::endl;
return 0;}	     
}

//    if(boost::filesystem::exists("time"+mpsName))
	     // {
	     //	 mpsName+="2";
	     ///}
    bool set_time=true;
     while(dt*i<tot)
    {
       if(std::abs(itensor::step_func(dt*i)-1.)>0.001)
       	{
       	  GM.set_current_time(dt*i);
       	  std::cout<< "curr time step func "<<itensor::step_func(dt*i)<<std::endl;
       	  GM.makeGates();
       	}
       auto psir2=TE.getBareMpsOneSite(Llead1+1);

    	 psir2.position(Llead1+1);
    
 auto ket = psir2(Llead1+1);
        auto bra = dag(prime(ket,"Site"));
		 
    	    auto Nph = op(sites,"Nph",Llead1+1);
	        auto Ne = op(sites,"n",Llead1+1);
		auto X = op(sites,"Bdag",Llead1+1)+op(sites,"B",Llead1+1);
		auto P =itensor::Cplx_i*(op(sites,"Bdag",Llead1+1)-op(sites,"B",Llead1+1));
		auto NX=gamma*op(sites,"NBdag",Llead1+1)+gamma*op(sites,"NB",Llead1+1);
		auto Neps=eps0*op(sites,"n",Llead1+1);

		auto Edot=Nph*omega+gamma*op(sites,"NBdag",Llead1+1)+gamma*op(sites,"NB",Llead1+1)+eps0*op(sites,"n",Llead1+1);
        //take an inner product 
        auto nph = eltC(bra*Nph*ket);
	        auto ne = eltC(bra*Ne*ket);
	auto x = eltC(bra*X*ket);
	auto p= eltC(bra*P*ket);
		auto edot= eltC(bra*Edot*ket);
auto neps=eltC(bra*Neps*ket);
auto nx=eltC(bra*NX*ket);
    		//  if(std::abs(imag(c))>1E-14){std::cout<< " errror in obs"; std::cout<<std::abs(imag(c))<<std::endl; }
		  auto c = (itensor::innerC(psir2, Curr, psir2));
		  auto el=(itensor::innerC(psir2, EL, psir2));
		  auto er=(itensor::innerC(psir2, ER, psir2));
		  auto curre= (itensor::innerC(psir2, CurrE, psir2));
		  auto ehybe= (itensor::innerC(psir2, EHYB, psir2));
    		    double avl2=real(c);
		     double avl3=real(curre);
		    double xval=real(x);

		    double pval=real(p);
		    double edotval=real(edot);
		    double neval=real(ne);
		    double nphval=real(nph);
		    double elval=real(el);
		    double erval=real(er);
       		    std::vector<double> v1;
       		    std::vector<double> v2;
       		    std::vector<double> v3;
       		    std::vector<double> v4;
       		    std::vector<double> v5;
       		    std::vector<double> v6;
       		    std::vector<double> v7;
       		    std::vector<double> v8;
       		    std::vector<double> v9;
       		    std::vector<double> v10;
       		    v1.push_back(i*dt);
       		    v2.push_back(avl2);
       		    v3.push_back(xval);
       		    v4.push_back(pval);
       		    v5.push_back(edotval);
       		    v6.push_back(neval);
       		    v7.push_back(nphval);
       		    v8.push_back(avl3);
       		    v9.push_back(erval);
       		    v10.push_back(elval);
			      
       		    auto vecSize=v1.size();
		    
       		    std::cout<< vecSize << "print "<<v1[0] <<std::endl;
       		    std::cout<<  "/time"+mpsName <<std::endl;
       		    Many_Body::ToFile(v1, boost::filesystem::canonical(dir).string()+"/time.dat",vecSize);
       Many_Body::ToFile(v2, boost::filesystem::canonical(dir).string()+"/J.dat",vecSize);
       Many_Body::ToFile(v3, boost::filesystem::canonical(dir).string()+"/X.dat",vecSize);
       Many_Body::ToFile(v4,boost::filesystem::canonical(dir).string()+ "/P.dat",vecSize);
       Many_Body::ToFile(v5,boost::filesystem::canonical(dir).string()+ "/Edot.dat",vecSize);
       Many_Body::ToFile(v6, boost::filesystem::canonical(dir).string()+"/Ne.dat",vecSize);
       Many_Body::ToFile(v7, boost::filesystem::canonical(dir).string()+"/Nph.dat",vecSize);
       Many_Body::ToFile(v8, boost::filesystem::canonical(dir).string()+"/Ecur.dat",vecSize);
       Many_Body::ToFile(v9, boost::filesystem::canonical(dir).string()+"/ER.dat",vecSize);
       Many_Body::ToFile(v10, boost::filesystem::canonical(dir).string()+"/EL.dat",vecSize);
       
       // Many_Body::ToFile<std::vector<double>>({neint}, "Neint"+mpsName,vecSize);

		  if(maxLinkDim(psi0)>=Md-100)
		    {

    std::cout<< "finish "<<std::endl;
return 0;
		    }
   
   		  std::cout<< std::setprecision(12)<<dt*i <<" ne "<< neval << " " <<curre<< "  " << "  E "<< innerC(psir2, H2, psir2)<< " compare  "<< ehybe+edot+er+el<<" " "lbo max "<< TE.maxLboDim()<<"   "<< " mx  "<< maxLinkDim(psi0)<<" av  "<< averageLinkDim(psi0)<<"  dot energies "<<edot << " "<<nph*omega<< "  " <<nx << "  " << neps << "\n";
		  std::cout<< "start TC"<<std::endl;
       	   TE.lboTEBDStepO2paraOneSite(Llead1+1);		  

	   i++;
	       		 	  }
 

     return 0;
}
