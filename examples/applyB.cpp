#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include <dirent.h>
#include <sys/stat.h>
using namespace std;
using namespace itensor;
int main(int argc, char *argv[])
{
   
std::string mpsName{};
 std::string siteSetName{};

 int site{};
 std::string ssite{};
  std::string newdir{};
   std::string statedir{};
  std::string filename="B";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
       ("site", boost::program_options::value(&site)->default_value(1), "site")
      ("statedir", boost::program_options::value(&statedir)->default_value(""), "statedir")
     ("newdir", boost::program_options::value(&newdir)->default_value(""), "newdir");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
      
  else if (vm.count("site"))
      {      std::cout << "site: " << vm["site"].as<int>() << '\n';
      	ssite="site"+std::to_string(vm["site"].as<int>());
      		filename+=ssite;
      }
else if (vm.count("statedir"))
  {      std::cout << "statedir: " << vm["statedir"].as<std::string>() << '\n';

    statedir=vm["statedir"].as<std::string>();
      }
else if (vm.count("newdir"))
  {      std::cout << "newdir: " << vm["newdir"].as<std::string>() << '\n';

    statedir=vm["new"].as<std::string>();
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

    auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(statedir+mpsName);
	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi); j+=2)
    {

    ne+=1,"n",j;
    }
     ne+=1,"n",length(psi);
	 auto Ne = toMPO(ne);

  site*=2;
  site--;
  applyB(psi, site, sites);
  
 
itensor::writeToFile(newdir+filename+mpsName,psi);
 std::cout<<innerC(psi,Ne, psi)<<std::endl;
     
       
    return(0);
}
