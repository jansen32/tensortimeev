
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/GF.hpp"
#include "itensor/util/parallel.h"
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/complex.hpp>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <iostream>
namespace mpi = boost::mpi;
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{

// Environment env(argc,argv);

//if(env.firstNode()) printfln("There are %d nodes",env.nnodes());

  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};

  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	     	     if (vm.count("j"))
      {      std::cout << "j: " << vm["j"].as<int>() << '\n';
	sj="i"+std::to_string(int(L/2)+1)+"j"+std::to_string(j);
	filename+=sj;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }

      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  mpi::environment env;
  mpi::communicator world;
  std::cout << "I am process " << world.rank() << " of " << world.size()
            << "." << std::endl;
   auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    //print(psi);
   using Gate = BondGate;
    auto gates1 = vector<Gate>();
    auto gates2 = vector<Gate>();
  int N=2*L;
   std::vector<int> v(N, M);
   itensor::Args argsState={"ConserveNf=",true,
                              "ConserveNb=",false,
 			   "DiffMaxOcc=",true, "MaxOccVec=", v};
   auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",true);

   // since only half steps

   std::map<std::string, double> param;
  param.insert({"t0", t0});
  param.insert({"gamma", gamma});
   param.insert({"omega", omega});
IHG GM1(sites, gates1, -dt/2, param);
 IHG GM2(sites, gates2, dt/2, param);
  
   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   
 // GM.makeFTGates(Gate::tReal);
 GM1.makeFTGatesAux();
 GM2.makeFTGatesAux();


 auto psi2=psi;



  


 int i=(int(L/2)+1)*2 -1;
 j*=2;
 j--;
 std::cout<< " i in aux space "<< i << std::endl;
 
      mpsName.insert(0, filename); 
 
  mpsName.insert(0, "tdmrglboAuxpara");
 std::cout<< " HERE  " << std::endl;
 // tot/=2;
 int steps=int(tot/dt);
 std::cout<< "steps "<< steps<<std::endl;
 std::cout<< "each thread should do "<< int(steps/world.size())<<std::endl;


 	 auto ne = AutoMPO(sites);
	 for(int j = 1; j < length(psi); j+=2)
    {

    ne+=1,"n",j;
    }

 auto Ne = toMPO(ne);
 std::vector<std::complex<double>> all_data;
//    std::vector<double> t;
 for(int k=1; k<=steps; k+=world.size())
   {
        MPS psi1copy=psi;
	MPS psi2copy=psi;
	TimeEvolveLbo< IHG, decltype(psi), false> C1(GM1,psi1copy, N,  argsMPS, argsState);
TimeEvolveLbo< IHG, decltype(psi), false> C2(GM2,  psi2copy, N,  argsMPS, argsState);
     GFCCDAGpara(C1,  psi1copy,   GM2.gates, sites, i,  k+world.rank());

    GFCCDAGpara(C2,  psi2copy,   GM1.gates, sites, j,  k+world.rank());
  C1.makeBare();
   C2.makeBare();

 std::complex<double> CCDAGval=innerC(psi2copy, psi1copy);

 std::cout << "I " << world.rank() << " dt "<<(k+world.rank())*dt<<std::endl;
 if(world.rank() == 0)
   {
     std::cout<<" and  " <<CCDAGval << " belong to "<< dt<< " and n1 "<<  innerC(psi1copy,Ne, psi1copy)<< "  and "<<innerC(psi2copy,Ne, psi2copy) <<" lbo odd max 1 "<<C1.maxLboDimOdd()<<" lbo even max 1 "<<C1.maxLboDimEven()<<" lbo odd max 2 "<<C2.maxLboDimOdd()<<" lbo even max 2 "<<C2.maxLboDimEven()<< " mx psi1  "<< maxLinkDim(psi1copy)<<" av psi1  "<< averageLinkDim(psi1copy)<<" mx psi2  "<< maxLinkDim(psi2copy)<<" av psi2  "<< std::endl;}
        if (world.rank() == 0) {
	  std::vector<std::complex<double>> data;
     std::vector<double> time_numbers;
     gather(world, CCDAGval, data, 0);
     //     gather(world, dt, time_numbers, 0);
     all_data.insert(all_data.end(), data.begin(), data.end());
//     t.insert(t.end(), time_numbers.begin(), time_numbers.end());
   } else {
gather(world, CCDAGval, 0);
//       gather(world, dt, 0);
   }
 }
 if (world.rank() == 0) {
   std::vector<double> time;
   for(int l=0; l<all_data.size(); l++)
     {
       //time.push_back((l+1)*dt);
       std::cout<<(l+1)*dt<< " "<< all_data[l]<<"  "<<std::endl;
       
     }
         std::string DIR="CCDAGGF/";
	 Many_Body::bin_write(DIR+"CCDAGGF"+mpsName,all_data);
	 Many_Body::bin_write(DIR+"time"+mpsName, time);
  }
  


  return 0;
}
