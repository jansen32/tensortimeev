#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include<iostream>
	
using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
  double cutoff{};
  int M{};
  int Md{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
  double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string sMd{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string filename="hetstpara";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("Md", boost::program_options::value(&Md)->default_value(2000), "Md")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << vm["Ll1"].as<int>() << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << vm["Ll2"].as<int>() << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << vm["Lc"].as<int>() << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << vm["tl"].as<double>() << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 3);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << vm["tint"].as<double>() << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 3);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		 if (vm.count("eps0"))
      {      std::cout << "eps0: " << vm["eps0"].as<double>() << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 3);
      		filename+=seps0;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << vm["V"].as<double>() << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	filename+=".bin";
  std::cout<<filename<<std::endl;

  int N=Llead1+Llead2+Lchain;
  std::vector<int> v(N, 0);
  for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }

   auto argsState = itensor::Args({"ConserveNf=",true,
                             "ConserveNb=",false,
      "DiffMaxOcc=",true, "MaxOccVec=", v} );
    auto sites = itensor::Holstein(N, argsState);
 
    auto H1=itensor::makeHetHam(sites, Llead1, Llead2,  Lchain, tl, tint, t0, gamma, omega, eps0 );
    auto H2=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, gamma, omega, eps0);

    auto state = itensor::InitState(sites);
    for(int i=1; i<=N; i+=1)
      {
 	if(i%2==0){state.set(i, "Occ");}
      }

    auto psi = MPS(state);
      psi.position(1);
     psi.normalize();
         Print(psi);
    
  	 auto sweeps = itensor::Sweeps(7);
    //very important to use noise for this model
	 sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
	 sweeps.maxdim() = 10,20, 80,200,300, 400, 400;
	 sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-13, 1E-13;
    std::cout<< "start "<< std::endl;
     auto [energy,psi0] = itensor::dmrg(H1,psi,sweeps,{"Quiet=",true});
 psi.position(1);
     psi.normalize();
        itensor::printfln("Ground State Energy1 = %.12f",energy);
auto args = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",cutoff,"MaxDim=",3000);


 auto H1sqrd=itensor::nmultMPO(prime(H1), H1, args2);
 auto E0=itensor::innerC(psi0, H1,psi0);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psi0,H1sqrd,psi0);

	  auto VAL=E0*E0;
		    auto VARIANCE =  (E0Sqrd-VAL )/E0Sqrd;
		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
		    std::cout<< "THE GS VAR WAS "<< (E0Sqrd-VAL )<<std::endl;
    // itensor::writeToFile("GS",psi0);
    //  auto psi0=itensor::readFromFile<MPS>("GS",sites);
    //itensor::printfln("Ground State Energy = %.12f",energy);
  psi0.position(1);
  psi0.normalize();
  std::cout<< "stop 1"<< std::endl;
  auto Curr=MPO(itensor::j(sites, Llead1,Llead2, Lchain, tint ));
   std::vector<double> obstebd;
   std::vector<double> Xvec;
      std::vector<double> Pvec;
         std::vector<double> Edotvec;
   std::vector<double> nphvec;
      std::vector<double> nevec;
  std::vector<double> time;
  std::map<std::string, double> param;
   std::cout<< "stop "<< std::endl;
 param.insert({"t0",t0});
 param.insert({"tl",tl});
 param.insert({"tint",tint});
 param.insert({"V",V/2});
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
  param.insert({"eps0",eps0});


 

  int i=0;

  itensor::IHGhet GM(sites, gates, dt, param, Llead1,Llead2, Lchain);
  auto psiv=itensor::vMPS(psi0);
  itensor::TimeEvolve<itensor::vMPS, false> TE(gates, psiv, psi0.N(), args);

     while(dt*i<tot)
    {
      //  start = std::chrono::system_clock::now();
;
		     	 psi0.position(Llead1+1);
    	 psi0.normalize();
	 

        auto ket = psi0(Llead1+1);
        auto bra = dag(prime(ket,"Site"));

        auto Nph = op(sites,"Nph",Llead1+1);
	        auto Ne = op(sites,"n",Llead1+1);
		auto X = op(sites,"Bdag",Llead1+1)+op(sites,"B",Llead1+1);
		auto P =itensor::Cplx_i*(op(sites,"Bdag",Llead1+1)-op(sites,"B",Llead1+1));
		auto Edot=Nph*omega+gamma*op(sites,"NBdag",Llead1+1)+gamma*op(sites,"NB",Llead1+1);
        //take an inner product 
        auto nph = eltC(bra*Nph*ket);
	        auto ne = eltC(bra*Ne*ket);
	auto x = eltC(bra*X*ket);
	auto p= eltC(bra*P*ket);
		auto edot= eltC(bra*Edot*ket);
  		 auto c = (itensor::innerC(psi0, Curr, psi0))/(itensor::innerC(psi0,  psi0));
  	
    		  if(std::abs(imag(c))>1E-14){std::cout<< " errror in obs"; std::cout<<std::abs(imag(c))<<std::endl; }
		  
    		    double avl2=real(c);
    		  time.push_back(i*dt);
    		  obstebd.push_back(avl2);
		  Xvec.push_back(real(x));
		  Pvec.push_back(real(p));
		  Edotvec.push_back(real(edot));
		  nphvec.push_back(real(nph));
		  nevec.push_back(real(ne));
    		 //psi0.position(1);
    		 //	 psi0.normalize();
		  if(maxLinkDim(psi0)>=Md-100)
		    {
		      		      break;
		    }
		 //	 psi0.normalize();
		  std::cout<<c<< "  "<< std::setprecision(12)<<dt*i << "  E "<< innerC(psi0, H1, psi0)<< "  "<< " mx  "<< maxLinkDim(psi0)<<" av  "<< averageLinkDim(psi0)<<"  "<<"\n";
  	//<<maxM(psi)<< "  "<< norm(psi)<<std::endl;
       	     TE.TEBDStepO2para();
   
   

	     i++;
		 	  }
    
      Many_Body::bin_write("time"+filename, time);
    Many_Body::bin_write("J"+filename, obstebd);
    Many_Body::bin_write("X"+filename, Xvec);
        Many_Body::bin_write("Edot"+filename, Edotvec);
	    Many_Body::bin_write("P"+filename, Pvec);
    Many_Body::bin_write("Nph"+filename, nphvec);
    Many_Body::bin_write("Ne"+filename, nevec);
     return 0;
}
