#include "parallel_tdvp.h"
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/GFopt.hpp"
#include <boost/filesystem.hpp>
#include"../src/timeevclass.hpp"
using namespace itensor;
 const std::string obsname="JJ";

void write_data(MPS& psi1x, MPS& psi2x, double& final_lboMaxB, double& final_lboMaxK, double time, MPO Op, Args argsObs, std::string DIR, Args const& args = Args::global())
{
  std::vector<std::complex<double>> o_vec;
  std::vector<double> t_vec;
  std::vector<double> normB_vec;
  std::vector<double> normK_vec;
  std::vector<double> avBdB_vec;
  std::vector<double> maxBdB_vec;
  std::vector<double> avBdK_vec;
  std::vector<double> maxBdK_vec;
  std::vector<double> maxlboK_vec;
  std::vector<double> maxlboB_vec;


  psi1x.position(1, args);
  psi2x.position(1, args);
	 psi2x= applyMPO(Op,psi2x,argsObs); 
	 psi2x.noPrime();
	 std::complex<double> Oval= innerC(psi2x,psi1x);
			  
	 auto NormB=norm(psi2x);
	 auto NormK=norm(psi1x);
	 auto maxBdK=maxLinkDim(psi1x);
	 auto maxBdB=maxLinkDim(psi2x);
	 auto avBdK=averageLinkDim(psi1x);
	 auto avBdB=averageLinkDim(psi2x);
	
 
    std::cout<< time<<" overlap" << Oval<< " mx psi1  "<< maxBdK<<" av psi1  "<< avBdK<<" mx psi2  "<< maxBdB<<" av psi 2  "<< avBdB<<" Norm 1.1= "<< NormK<<" norm 2.1= "<< NormB<<"\n";

    t_vec.push_back(time);
    o_vec.push_back(Oval);
    normK_vec.push_back(NormK);
    normB_vec.push_back(NormB);
 
    avBdK_vec.push_back(avBdK);
    maxBdK_vec.push_back(maxBdK);
    avBdB_vec.push_back(avBdB);
    maxBdB_vec.push_back(maxBdB);
 

    maxlboB_vec.push_back(final_lboMaxB);
    maxlboK_vec.push_back(final_lboMaxK);
    auto vecSize=t_vec.size();
    Many_Body::ToFile(t_vec, DIR+"/time.dat", vecSize);
    Many_Body::ToFile(o_vec, DIR+"/"+obsname+".dat",vecSize);
    Many_Body::ToFile(avBdB_vec, DIR+"/avBdB.dat", vecSize);
    Many_Body::ToFile(maxBdB_vec, DIR+"/maxBdB.dat", vecSize);
    Many_Body::ToFile(avBdK_vec, DIR+"/avBdK.dat", vecSize);
    Many_Body::ToFile(maxBdK_vec, DIR+"/maxBdK.dat", vecSize);
    Many_Body::ToFile(maxlboK_vec, DIR+"/lbomxK.dat", vecSize);
    Many_Body::ToFile(maxlboB_vec, DIR+"/lbomxB.dat", vecSize);
    Many_Body::ToFile(normK_vec, DIR+"/normK.dat", vecSize);
    Many_Body::ToFile(normB_vec, DIR+"/normB.dat", vecSize);
      return;
}
int
main(int argc, char* argv[])
    {
    Environment env(argc,argv);

    parallelDebugWait(env);

double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
     std::string somegap{};
std::string mpsNameB{};
std::string mpsNameK{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};
 double t_in{};
 bool apply_op=false;
  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
std::string DIR{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("appOP", boost::program_options::value(&apply_op)->default_value(false), "appOP")
      ("mpsNB", boost::program_options::value(&mpsNameB)->default_value("noName"), "mpsNB")
      ("mpsNK", boost::program_options::value(&mpsNameK)->default_value("noName"), "mpsNK")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Md", boost::program_options::value(&Md)->default_value(2000), "Md")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("t_in", boost::program_options::value(&t_in)->default_value(0.0), "t_in")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("d", boost::program_options::value(&DIR)->default_value("NODIR"), "d")
          ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omegap"+std::to_string(vm["omgp"].as<double>()).substr(0, 5);
      		filename+=somegap;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
	
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  
  filename+="prcs"+std::to_string(env.nnodes());
       Holstein sites;
    MPO H1;
        MPO H2;
    MPS psi1;
      MPS psi2;
    Sweeps sweeps1;
    Sweeps sweeps2;
    double lbomaxB{0};
    double lbomaxK{0};
    MPO Curr;
       int N=L;
  
               auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "MinDim=",10,"Truncate", true,"DoNormalize",false);
       	   auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
    if(env.firstNode())
        {

      Holstein sites = readFromFile<Holstein>(siteSetName);
   psi1 = readFromFile<MPS>(mpsNameK);
psi2 = readFromFile<MPS>(mpsNameB);
    auto curr= makeCurr(sites, t0);
  Curr = toMPO(curr);
  if(apply_op)
    { 
      auto y1 = applyMPO(Curr,psi1,argsObs);
     psi1=y1;
     psi1.noPrime();
    }
     sweeps1 = Sweeps(1);
    sweeps1.maxdim() = Md;
     sweeps1.mindim() = 1;
    sweeps1.cutoff() = cutoff;
    sweeps1.niter() = 10;
    sweeps2=sweeps1;
        auto am=makeHolstHam_disp(sites, t0, gamma, omega, omegap);
    H1 = toMPO(am);
        H2 = toMPO(am);

	   
	}
    auto  t_step=-dt*Cplx_i;



      env.broadcast(sites,H1,psi1,sweeps1);
      env.broadcast(sites,H2,psi2,sweeps2);

    bool first=true;
    Partition P1;
     std::vector<ITensor> Vs1;
           Partition P2;
     std::vector<ITensor> Vs2;
     Args args = Args::global();
         Observer obs;
	   splitWavefunction(env,psi1,P1,Vs1,argsMPS);
        splitWavefunction(env,psi2,P2,Vs2,argsMPS);
	 auto PH1 = computeHEnvironment(env,P1,psi1,Vs1,H1,argsMPS);
	 auto PH2 = computeHEnvironment(env,P2,psi2,Vs2,H2,argsMPS);
	 

	 int i=0;
     	   while(t_in +i*dt<tot)
      {


	//Act(env,P1,psi1,Vs1,PH1,sweeps1,obs,t_step, argsMPS, false);
	//	Act(env,P2,psi2,Vs2,PH2,sweeps2,obs,t_step, argsMPS, false);    
	env.barrier();
          MPS psi1x=psi1;
	     MPS psi2x=psi2;
	     double final_lboMaxB=gatherMax_val(env,lbomaxB);
	     double final_lboMaxK=gatherMax_val(env,lbomaxK);
	     gather_vector(env,P1, psi1x, Vs1);

	     gather_vector(env,P2, psi2x, Vs2);
       	   if(env.firstNode())
       	    	     {
write_data(psi1x, psi2x, final_lboMaxB , final_lboMaxK, t_in+i*dt,Curr, argsObs, DIR, argsMPS);
  	  	       
       	   	     }


  
 
        	env.barrier();

          	 lbomaxK=Act_lbo(env,P1,psi1,Vs1,PH1,sweeps1,obs,t_step, argsMPS, first);
		 lbomaxB=Act_lbo(env,P2,psi2,Vs2,PH2,sweeps2,obs,t_step, argsMPS, first);
		//        	 auto a=Act_lbo(env,P1,psi1,Vs1,PH1,sweeps1,obs,t_step, argsMPS, first);
		// auto b=Act_lbo(env,P2,psi2,Vs2,PH2,sweeps2,obs,t_step, argsMPS, first);
		//			Act(env,P1,psi1,Vs1,PH1,sweeps1,obs,t_step, argsMPS, first);
		//		 		Act(env,P2,psi2,Vs2,PH2,sweeps2,obs,t_step, argsMPS, first);
       	    	if(first)
       	   {
   
            first=false;
       	   }
     	   	   i++;
	std::cout<< i << " and "<< t_in +i*dt << "  and "   << tot << "   and "  <<  (t_in +i*dt<tot)<<std::endl;     
     	   }
 MPS psi1x=psi1;
	     MPS psi2x=psi2;
	     gather_vector(env,P1, psi1x, Vs1);

	     gather_vector(env,P2, psi2x, Vs2);
       	   if(env.firstNode())
       	    	     {
		       auto find1=mpsNameK.find("MPSGS");
		       auto find2=mpsNameB.find("MPSGS");
		       std::string app_J="";

		       if(apply_op)
			 {app_J="appliedJ";}
		       itensor::writeToFile(DIR+"/MPSKat"+stot+app_J+mpsNameK.substr(find1, mpsNameK.size()),psi1x);
		      itensor::writeToFile(DIR+"/MPSBat"+stot+mpsNameB.substr(find2, mpsNameB.size()),psi2x);
		     }

    return 0;
    }
