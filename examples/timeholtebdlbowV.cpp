
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#ifdef ITENSOR_USE_TBB
#include "tbb/task_scheduler_init.h"	
#endif

using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
#ifdef ITENSOR_USE_TBB
   tbb::task_scheduler_init init(12);
#endif
   std::string mpsName{};
 std::string siteSetName{};
  double cutoff{};
  double lbocutoff{};
  double norm_change;
  double phcutoff{};
  int M{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};

  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
    double eps0{};
  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
    std::string seps0{};
  std::string snorm_change{};
  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
           ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(10), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("Norm", boost::program_options::value(&norm_change)->default_value(0.0001), "Norm")
      ("eps", boost::program_options::value(&eps0)->default_value(1.0), "eps")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		     	 	 if (vm.count("eps"))
      {      std::cout << "eps0: " << vm["eps"].as<double>() << '\n';
      	
      		filename+="eps0"+std::to_string(vm["eps"].as<double>()).substr(0, 3);
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << vm["V"].as<double>() << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 3);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		       		 if (vm.count("Norm"))
      {      std::cout << "NormChange: " << norm_change << '\n';
	 std::stringstream ss;
	 ss<<vm["Norm"].as<double>();
	 snorm_change="NC"+ss.str();
      	filename+=snorm_change;
      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  using Gate = BondGate;
   auto gates = vector<Gate>();
  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi = itensor::readFromFile<MPS>(mpsName);
  size_t N=length(psi);
  std::vector<int> v(N, 0);
  
for(int i=0; i<N; i++)
    {
      v[i]=M;

    }//"MinDim=",maxLinkDim(psi0)
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "CutoffLBO",lbocutoff  , "MaxDim=",Md, "Normalize",false, "SVDMethod", "ITensor");


 psi.position(1);
    // Print(psi.A(1));
     psi.normalize();
 

 auto psiv=vMPS(psi);
 std::map<std::string, double> param;
 param.insert({"t0", t0});
 param.insert({"gamma", gamma});
  param.insert({"omega", omega});
  param.insert({"Const", eps0});
  itensor::IHG GM(sites, gates, dt, param);

 TimeEvolveLbo< IHG, decltype(psiv), false> C(GM,  psiv, L,  argsMPS, argsState);
 
  C.makeLbo();
  std::cout<< "start "<<std::endl;
  GM.makeGates_V(V);
 std::vector<double> obstebdlbo;
  std::vector<double> time;

    std::chrono::duration<double> elapsed_seconds;
    std::chrono::time_point<std::chrono::system_clock> start, end;//  C.TEBDStepO2();
    auto CDWdisp=CDWDispOparam(sites);

auto CDWO=CDWOparam(sites);
 
 size_t mps_pos=mpsName.find("MPS");
 mpsName=mpsName.substr(mps_pos);

  mpsName.insert(0, filename);

  mpsName.insert(0, "holtebdlbopara");
  std::string dirName=mpsName;
  dirName.replace(dirName.end()-4, dirName.end(), "dir");
  boost::filesystem::path dir(dirName);
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success " << std::endl;
}	     
	     else{
dir=boost::filesystem::path(dirName+"2");
 if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success second attempt" << std::endl;
}
 else{ std::cout<< "could not make dir error "<<std::endl;
return 0;}
	     }
	        int i=0;
//     std::cout<< "j2j2j "<< std::endl;
     while(i*dt<tot)
     {
             start = std::chrono::system_clock::now(); 
	  

      	 end = std::chrono::system_clock::now();
      	 		  
      				auto psir2=C.getBareMps();
      	    psir2.position(1);
 
       	  auto NORM=itensor::norm(psir2);
	 if(std::abs(NORM-1.)>norm_change)
	   {

	     std::cout<< "normalize!!!"<<std::endl;
	     std::cout<< "before "<<NORM<<std::endl;
	 C.paraNormalize();
       psir2=C.getBareMps();

    	 psir2.position(1);
       	   NORM=itensor::norm(psir2);
	   std::cout<<"norm after "<< NORM<<std::endl;

}


  std::vector<double> v1;
       		    std::vector<double> v2;
       		    std::vector<double> v3;

			    auto cdwdisp = (itensor::innerC(psir2, CDWdisp, psir2));
			    auto cdwo = (itensor::innerC(psir2, CDWO, psir2));
			    	  double cdwdisp_val=real(cdwdisp);

double cdwo_val=real(cdwo);
 v1.push_back(i*dt);
 v2.push_back(cdwdisp_val);
v3.push_back(cdwo_val);
 for(int j=1; j<=L; j++){

   	std::vector<double> nevec2;
			std::vector<double> nphvec;
			psir2.position(j);
			  auto ket = psir2(j);
        auto bra = dag(prime(ket,"Site"));
        auto Neloc = op(sites,"n",j);
	auto Nphloc = op(sites,"Nph",j);
	auto nph = eltC(bra*Nphloc*ket);
	auto ne = eltC(bra*Neloc*ket);
	nevec2.push_back(real(ne));
	nphvec.push_back(real(nph));
	auto vecSize=nevec2.size();
	Many_Body::ToFile(nphvec, boost::filesystem::canonical(dir).string()+"/Nphloct"+std::to_string(j)+".dat", vecSize);
	Many_Body::ToFile(nevec2, boost::filesystem::canonical(dir).string()+"/Neloct"+std::to_string(j)+".dat", vecSize);
		      }
 
       		   
        		    auto vecSize=v1.size();
		     Many_Body::ToFile(v1, boost::filesystem::canonical(dir).string()+"/time.dat",vecSize);
        Many_Body::ToFile(v3, boost::filesystem::canonical(dir).string()+"/CDWO.dat",vecSize);
        Many_Body::ToFile(v2, boost::filesystem::canonical(dir).string()+"/CDWdisp.dat",vecSize);
 	  std::cout<<std::setprecision(8)<<"NO "<<norm(psir2)<<"  "<<cdwo<< "  "<< i*dt<< "  max lbo "<< C.maxLboDim()<<"  "<<'\n';
  
    C.lboTEBDStepO2para();
	i+=1;
     }

  
  return 0;
}
