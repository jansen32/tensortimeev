
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/GFopt.hpp"
#include <boost/filesystem.hpp>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
     std::string somegap{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};

  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
               ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omegap"+std::to_string(vm["omgp"].as<double>()).substr(0, 3);
      		filename+=somegap;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    //print(psi);
   using Gate = BondGate;
    auto gates = vector<Gate>();
  int N=L;
   
   std::vector<int> v(N, M);
   itensor::Args argsState={"ConserveNf=",true,
                              "ConserveNb=",false,
 			   "DiffMaxOcc=",true, "MaxOccVec=", v};


   auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"Normalize",false,"SVDMethod", "gesdd");



   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   
MPS psi2=psi;

   auto curr= makeCurr(sites, t0);
    auto Curr = toMPO(curr);
    auto am=makeHolstHam_disp(sites, t0, gamma, omega, omegap);
    auto H = toMPO(am);
    auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
      std::map<std::string, double> param;
  param.insert({"t0", t0});
  param.insert({"gamma", gamma});
   param.insert({"omega", omega});
    IHG GM(sites, gates, dt, param);
 GM.makeGates();


 TimeEvolveLbo< IHG, decltype(psi), false> C1(GM,  psi, N,  argsMPS, argsState);
  
  
    mpsName.insert(0, filename); 
 
  mpsName.insert(0, "tdmrglbo");
   std::string obsname="JJ";
         std::string DIR="JJprobe/"
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     //	      print(psi);
	      std::string filename_tot=DIR+"JJprobe"+mpsName;
	      boost::filesystem::path p(filename_tot);
	      std::cout << "path "<< p<<std::endl;
	      std::cout<< "start energy "<<innerC(psi2,H, psi)<<std::endl;
	      std::cout<< "start obs "<<innerC(psi2,Curr, psi)<<std::endl;
 auto y1 = applyMPO(Curr,psi,argsObs);
 y1.noPrime();
std::cout<< "start obs sqer "<<innerC(psi2,Curr, y1)<<std::endl;
	      int i=0;
	            TE1.makeLbo();
 	   while(n*dt<tot)
      {

 	  psi1.position(1);
 	  

	  auto psi1=TE1.getBareMps();

	  // generate new Ham and curr
	  double A0=1E-04;
	  double om_p=10;
	  double tau=0.05;
	  std::complex<double> factor=A0*std::cos(om_p*n*dt)*std::exp(Cplx_i);

 	  double n1=norm(psi1);
  	  std::complex<double> Oval= innerC(psi2x, psi1x);
 	std::vector<std::complex<double>> o;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
	std::vector<double> Afunc
 	    t.push_back(double(n*dt));
 	    avBd.push_back(averageLinkDim(psi1));
 	    maxBd.push_back(maxLinkDim(psi1));
 	    lbomx.push_back(TE1.maxLboDim());
 	    o.push_back(Oval);


       auto vecSize=t.size();
        std::string DIR=obsname+"noNormKarGF/";
       Many_Body::ToFile(o, DIR+obsname+"GF"+filename,vecSize);
       Many_Body::ToFile(t, DIR+"time"+obsname+"GF"+filename, vecSize);
        Many_Body::ToFile(lbomx, DIR+"lbomx"+obsname+"GF"+filename, vecSize);
       Many_Body::ToFile(avBd, DIR+"avBd"+obsname+"GF"+filename, vecSize);
       Many_Body::ToFile(maxBd, DIR+"maxBd"+obsname+"GF"+filename, vecSize);
	    	    
 	 

       std::cout<< n*dt<<" overlap"+obsname+"KarnoNorm "<< Oval<<"lbo max psi1 "<<TE1.maxLboDim()<< "   "<< " mx psi1  "<< maxLinkDim(psi1)<<" av psi1  "<< averageLinkDim(psi1)<<  " lbo max psi2 "<<TE2.maxLboDim()<<" mx psi2  "<< maxLinkDim(psi2)<<" av psi1  "<< averageLinkDim(psi2)<<" Norm 1.1= "<< norm(psi1)<<" norm 2.1= "<< norm(psi2)<<" Norm 1.2= "<<n1 <<" norm 2.2= "<< n2<<"\n";
 n++;



 	TE1.lbotDmrgStep();
 	TE2.lbotDmrgStep();
 
      }
  return 0;
}
