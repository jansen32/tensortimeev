#include "itensor/all.h"
#include"files.hpp"
#include"holstein_bath.hpp"
#include <iostream>
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include<iostream>

#include"../src/tdvp.hpp"

#ifdef ITENSOR_USE_TBB
#include "tbb/task_scheduler_init.h"	
#endif

using namespace std;

using namespace itensor;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{
#ifdef ITENSOR_USE_TBB
   tbb::task_scheduler_init init(12);
#endif


  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
std::string mpsName{};
 std::string siteSetName{};

    double cutoff{};
  double lbocutoff{};
  int M{};
    int Niter{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int LH{};
  bool saveNr=false;
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
    double eps0{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLH{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
   std::string sNiter{};
  std::string sV{};
    std::string seps0{};
  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("LH", boost::program_options::value(&LH)->default_value(4), "LH")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
          ("Niter", boost::program_options::value(&Niter)->default_value(10), "Niter")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
    ("saveNr", boost::program_options::value(&saveNr)->default_value(false), "saveNr");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("LH"))
      {      std::cout << "LH: " << LH << '\n';
      	sLH="LH"+std::to_string(vm["LH"].as<int>());
	filename+=sLH;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 5);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 5);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 5);
      		filename+=seps0;
      }

      		 if (vm.count("V"))
      {      std::cout << "V: " << V << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << tot << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << dt << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		       	     if (vm.count("Md"))
      {      std::cout << "Md: " << Md << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			     if (vm.count("Niter"))
      {      std::cout << "Niter: " << Niter << '\n';
      	sNiter="Niter"+std::to_string(vm["Niter"].as<int>());
      	filename+=sNiter;
      }

           		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << cutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }


      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  //	filename+=".bin";
  std::cout<<filename<<std::endl;
  auto sites = itensor::readFromFile<itensor::Holstein_bath>(siteSetName);
  auto psi = itensor::readFromFile<MPS>(mpsName);
  size_t N=length(psi);
  std::vector<int> v(N, 0);
  // is stored as vecto so start site-1
  for(size_t i=Llead1; i<v.size(); i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd,"CutoffLBO",lbocutoff  , "MaxDim=",Md,"Normalize",false);
  //    auto H2=itensor::makeIntHamV(sites, Llead1, LH, V/2, tl, tint, t0, gamma, omega, eps0);
  psi.position(1);
  psi.normalize();
  std::cout<< "stop 1"<< std::endl;
  auto H2=itensor::makeIntHamV_att_bath(sites, Llead1, LH, V/2, tl, tint, t0, gamma, omega, eps0, 0);

  auto Curr=MPO(itensor::jint(sites, Llead1, tint ));
    auto Neint=MPO(itensor::Neint(sites, Llead1 ));
     auto EHYB=MPO(itensor::EHYBint(sites, Llead1, tint));
     auto EL=MPO(itensor::ELint(sites, Llead1, LH, tl ));
      auto ER=MPO(itensor::ERint_bath(sites, Llead1, LH, t0,gamma, omega,  eps0 ));
      auto ERkin=MPO(itensor::ERkin_bath(sites, Llead1, LH, t0));
      auto RNph=MPO(itensor::Nphint_bath(sites, Llead1, LH, 1));





 

  int i=0;

 
     auto sweeps = Sweeps(1);
    sweeps.maxdim() = Md;
    sweeps.cutoff() = cutoff;
    sweeps.niter() =Niter;
    println(sweeps);
  TDVP<MPS> tdvp(psi,H2, -dt*Cplx_i);
 size_t mps_pos=mpsName.find("MPS");
 mpsName=mpsName.substr(mps_pos);
 
  mpsName.insert(0, filename);

  mpsName.insert(0, "int_bathtdvp");
  std::string dirName=mpsName;
 
  dirName.replace(dirName.end()-4, dirName.end(), "dir");
  boost::filesystem::path dir(dirName);
 std::cout<<"dir namme "<<dirName<<std::endl;
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success " << std::endl;
}
	     
	     else{
dir=boost::filesystem::path(dirName+"2");
 if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success second attempt" << std::endl;
}
 else{ std::cout<< "could not make dir error "<<std::endl;
return 0;}	     
}
	     bool set_time=true;
     while(dt*i<tot)
    {
      if(set_time)
	{
       if(std::abs(itensor::step_func(dt*i)-1.)>0.001)
     	{
     	  H2=itensor::makeIntHamV_att_bath(sites, Llead1, LH, V/2, tl, tint, t0, gamma, omega, eps0, dt*i);
       	  tdvp.H=H2;
       	  std::cout<< "curr time step func "<<itensor::step_func(dt*i)<<std::endl;
     	}
       else
     	{
     	 H2=itensor::makeIntHamV_att_bath(sites, Llead1, LH, V/2, tl, tint, t0, gamma, omega, eps0,40.);
       	  tdvp.H=H2;
	   std::cout<< "curr time step func "<<itensor::step_func(40.)<<std::endl;
	  set_time=false;}
	}



      psi.position(1);
    	 psi.normalize();
	 auto NORM=norm(psi);
    		 auto c = (itensor::innerC(psi, Curr, psi));
		 auto neintval = (itensor::innerC(psi, Neint, psi));
		 auto er = (itensor::innerC(psi, ER, psi));
		 auto el = (itensor::innerC(psi,EL, psi));
		 auto ehyb = (itensor::innerC(psi, EHYB, psi));
		 auto rkin = (itensor::innerC(psi,ERkin, psi));
		 auto rnph = (itensor::innerC(psi, RNph, psi));
		  
    		    double avl2=real(c);
		    double avl_el=real(el);
		    double avl_er=real(er);
		    double avl_ehyb=real(ehyb);
		    double avl_erkin=real(rkin);
		    double avl_rnph=real(rnph);
		    double neint=real(neintval);
		    std::vector<double> nevec2;
		    std::vector<double> nphvec;
		//     if(saveNr and (i%100==0))
	// 	      {
	// 	    for(int j=1; j<=length(psir2); j++)
	// 	      {
	// 		psir2.position(j);
	// 		  auto ket = psir2(j);
        // auto bra = dag(prime(ket,"Site"));
        // auto Neloc = op(sites,"n",j);
	// auto Nphloc = op(sites,"Nph",j);
	// auto nph = eltC(bra*Nphloc*ket);
	// auto ne = eltC(bra*Neloc*ket);
	// nevec2.push_back(real(ne));
	// nphvec.push_back(real(nph));
	// Many_Body::bin_write("Nphloct"+std::to_string(dt*i).substr(0, 6)+filename, nphvec);
	// Many_Body::bin_write("Neloct"+std::to_string(dt*i).substr(0, 6)+filename, nevec2);
	// 	      }
	// 	      }

		     double max_bond=maxLinkDim(psi);
		    std::vector<double> v1;
		    std::vector<double> v2;
		    std::vector<double> v_el;
		    std::vector<double> v_er;
		    std::vector<double> v_eh;
		    std::vector<double> v_rkin;	
		    std::vector<double> v_nph;
		    std::vector<double> v_max_bond;

		    v1.push_back(i*dt);
		    v2.push_back(avl2);
		    v_el.push_back(avl_el);
		    v_er.push_back(avl_er);
		    v_eh.push_back(avl_ehyb);
		    v_rkin.push_back(avl_erkin);
		    v_nph.push_back(avl_rnph);
		    v_max_bond.push_back(max_bond);
		    
		    auto vecSize=v1.size();
		    std::cout<< vecSize << "print "<<v1[0] <<std::endl;

       Many_Body::ToFile(v1,boost::filesystem::canonical(dir).string()+ "/time.dat",vecSize);
       Many_Body::ToFile(v2,boost::filesystem::canonical(dir).string()+ "/J.dat",vecSize);
       Many_Body::ToFile(v_el,boost::filesystem::canonical(dir).string()+ "/EL.dat",vecSize);
       Many_Body::ToFile(v_er,boost::filesystem::canonical(dir).string()+ "/ER.dat",vecSize);
       Many_Body::ToFile(v_eh,boost::filesystem::canonical(dir).string()+ "/Ehyb.dat",vecSize);
       Many_Body::ToFile(v_rkin,boost::filesystem::canonical(dir).string()+ "/ERkin.dat",vecSize);
       Many_Body::ToFile(v_nph,boost::filesystem::canonical(dir).string()+ "/RNph.dat",vecSize);
   Many_Body::ToFile(v_max_bond,boost::filesystem::canonical(dir).string()+ "/Bondmax.dat",vecSize);
       // Many_Body::ToFile<std::vector<double>>({neint}, "Neint"+mpsName,vecSize);

		  if(max_bond>=Md-100)
		    {

    std::cout<< "finish "<<std::endl;
return 0;
		    }
    		  std::cout<< std::setprecision(12)<<dt*i <<"  " << c << "  E "<< innerC(psi, H2, psi)<< " comp  "<< el+er+ehyb<<"  "<<c<< " n "<<real(neint)<<"   "<< " mx  "<< max_bond<<" av  "<< averageLinkDim(psi)<<"  NORM "<<NORM<<"\n";
		  std::cout<< "start TC"<<std::endl;
       tdvp.step(sweeps);
	   i++;
	       		 	  }
 

     return 0;
}
