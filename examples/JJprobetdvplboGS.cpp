
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/GFopt.hpp"
#include"tdvp_lbo.hpp"
#include <boost/filesystem.hpp>
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
     std::string somegap{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double omegap{};
  double tot{};

  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("j", boost::program_options::value(&j)->default_value(1), "j")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph)->default_value(M), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(200), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
               ("omgp", boost::program_options::value(&omegap)->default_value(0), "omgp")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-15), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omegap"+std::to_string(vm["omgp"].as<double>()).substr(0, 3);
      		filename+=somegap;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    //print(psi);
   using Gate = BondGate;
    auto gates = vector<Gate>();
  int N=L;
   


  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "MinDim=",10,"Truncate", true,"DoNormalize",false, "SVDMethod", "gesdd");



   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   
MPS psi2=psi;
    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Md;
    sweeps.cutoff() = cutoff;
    sweeps.niter() = 10;
    println(sweeps);
    	  double A0=1E-2;
	  double om_p=10;
	  double tau=0.1;
	  double tin=5.;
	  double probe=A0*std::cos(om_p*0*dt)*std::exp(-(0*dt-tin)*(0*dt-tin)/(tau*tau*2));
	  std::complex<double> factor=std::exp(-Cplx_i*probe);
	  auto curr= makeCurr_probe(sites, t0, factor);
    auto Curr = toMPO(curr);
 auto am=makeHolstHam_disp_probe(sites, t0, gamma, omega, omegap, factor);
 
 
    auto H = toMPO(am);
    auto argsObs= itensor::Args("Method=","DensityMatrix","MaxDim=",Md,"Cutoff=",cutoff);
        
    TDVP_lbo<MPS> tdvp1(psi,H, -dt*Cplx_i, sweeps,argsMPS);
  
    mpsName.insert(0, filename); 
 
  mpsName.insert(0, "tdvplbo");
   std::string obsname="Jprobe";
         std::string DIR=obsname+"/";
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");

	      std::string filename_tot=DIR+obsname+mpsName;
	      boost::filesystem::path p(filename_tot);
	      std::cout << "path "<< p<<std::endl;
 int n=0;
  	   
      tdvp1.make_lbo();
      bool check=true;
	   while(n*dt<tot)
      {

 	  psi.position(1);
 	  double n1=norm(psi);
  	  std::complex<double> Oval= innerC(psi, Curr,psi);
 	std::vector<std::complex<double>> o;
 	std::vector<double> t;
 	std::vector<double> avBd;
 	std::vector<double> maxBd;
 	std::vector<double> lbomx;
	std::vector<double> Avec;
 	    t.push_back(double(n*dt));
 	    avBd.push_back(averageLinkDim(psi));
 	    maxBd.push_back(maxLinkDim(psi));
 	    lbomx.push_back(tdvp1.maxLboDim());
	   Avec.push_back(probe);
 	    o.push_back(Oval);
	

       auto vecSize=t.size();
       std::cout<< " to "<< DIR+obsname+mpsName<<std::endl;
       Many_Body::ToFile(o, DIR+obsname+mpsName,vecSize);
       Many_Body::ToFile(t, DIR+"time"+obsname+mpsName, vecSize);
       Many_Body::ToFile(lbomx, DIR+"lbomx"+obsname+mpsName, vecSize);
       Many_Body::ToFile(avBd, DIR+"avBd"+obsname+mpsName, vecSize);
       Many_Body::ToFile(maxBd, DIR+"maxBd"+obsname+mpsName, vecSize);
       Many_Body::ToFile(Avec, DIR+"A"+obsname+mpsName, vecSize);
	    	    
 	 
 
       std::cout<< n*dt<<" overlap"+obsname+"KarnoNorm "<< Oval<<"lbo max psi1 "<< tdvp1.maxLboDim()<< " mx psi1  "<< maxLinkDim(psi)<<" av psi1  "<< averageLinkDim(psi)<<  " Norm 1.1= "<< norm(psi)<<" Norm 1.2= "<<n1 << " A= "<<probe << "fax "<< factor<< "\n";

       
        n++;
       // updating
	//	if(std::abs(probe)<1E-15 and check)
	  {
	probe=A0*std::cos(om_p*n*dt)*std::exp(-(n*dt-tin)*(n*dt-tin)/(tau*tau*2));
	   factor=std::exp(-Cplx_i*probe);
	  curr= makeCurr_probe(sites, t0, factor);
     Curr = toMPO(curr);
     am=makeHolstHam_disp_probe(sites, t0, gamma, omega, omegap, factor);
    tdvp1.H = toMPO(am);
    if(std::abs(n*dt-tin)<1E-10)
      {
	check=false;
      }
	  }


 	tdvp1.step();

      }
  return 0;
}
