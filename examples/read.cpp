#include <iostream>
#include <cmath>
#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include<iostream>
using namespace std;

int main(int argc, char *argv[])
{
  std::string fd{};
  std::string fmat{};
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("fmat", boost::program_options::value(&fmat)->default_value(""), "fmat")
      ("fd", boost::program_options::value(&fd)->default_value(""), "fd");
    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("fmat"))
      {    
  // std::cout << "Llead1: " << Llead1 << '\n';
  //     	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
  // 	filename+=sLlead1;
      }
            if (vm.count("fd"))
      {      
// std::cout << "Llead2: " << Llead2 << '\n';
//       	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
// 	filename+=sLlead2;
      }


      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  itensor::ITensor U, D;
  itensor::readFromFile(fmat, U);
  itensor::readFromFile(fd, D);
  Print(D);    
Print(U);
 U=dag(U);
 auto i =D.inds();
 auto j =U.inds();
 Print(i[0]);
 Print(i[1]);
 for(int l=1; l<=dim(i[0]); l++)
   {
 
     std::cout<< std::setprecision(4)<<itensor::elt(D, i[0](l),i[1](l) )<<std::endl ;    
 
   }
 std::cout<< "vector "<<std::endl;
 Print(j[0]);
 Print(j[1]);
 std::cout<< "BLOCK SIZE "<< itensor::blocksize( j[0], 1) << '\n';    
 std::cout<< "BLOCK SIZE "<< itensor::blocksize( j[0], 2) << '\n';    
 std::cout<< "BLOCK SIZE "<< itensor::blocksize( j[1], 1) << '\n';    
 std::cout<< "BLOCK SIZE "<< itensor::blocksize( j[1], 2) << '\n';    
 std::cout<< "COffs 1"<<'\n';
 for(int l=1; l<=int(dim(j[0])/2); l++)
   {

     std::cout<< std::setprecision(4)<<itensor::elt(U, j[0](l),j[1](1) )<< ' ';    

 std::cout<< '\n';
   }
 std::cout<< "COffs 2"<<'\n';
 for(int l=int(dim(j[0])/2)+1; l<=dim(j[0]); l++)
   {

     std::cout<< std::setprecision(4)<<itensor::elt(U, j[0](l), j[1](int(dim(j[0])/2)+1 ))<< ' ';    

 std::cout<< '\n';
   }
    return(0);
}
