#include "itensor/all.h"
#include "krylov.hpp"
#include "tridi.hpp"
#include"holstein.hpp"
using namespace itensor;
int main(int argc, char *argv[])
{
  double t0=1;
  double gamma=1;
    double omega=1;
    auto args = Args("Cutoff=",1E-13,"Maxm=",3000,"Normalize", true, "t0=", t0, "omega=", omega, "gamma", gamma);
  //  auto targs = args;
  double tau=0.001;
    auto ttotal = 0.1;
  int L=4;
  int N=2*L;
  int M=3;

    auto sites = Holstein(N,{"ConserveNf=",true,
                             "ConserveNb=",false,
	   "MaxOcc=", M});
  

    auto am=makeHolstHam(sites, t0, gamma, omega);
    auto H = toMPO(am);
              auto ampo = AutoMPO(sites);
	
    for(int j = 1; j <= N; j += 2)
        {
	   ampo += omega,"N",j+1;

        }
    auto Nph=toMPO(ampo);
    auto state = InitState(sites);
    state.setAll("Emp");
    state.set(1,"Occ");

    for(int i=2; i<=N; i+=2)
      {
	state.set(i,"1");
      }
    auto psi=MPS(state);
    psi.position(1);
     psi.normalize();
     psi.normalize();    
    int i=0;
 while(i*tau<ttotal)
   {
 krylov_timeStep(psi, H, -tau*(1._i));
 psi.normalize();
 auto en = innerC(psi,Nph,psi);
 printfln("\n N %.4f %.20f",ttotal, en);
 std::cout<<" "<< i*tau << std::endl;
 i++;
 //  std::cout<< "hei xxx 2"<< std::endl;
}
 

  return 0;
}


