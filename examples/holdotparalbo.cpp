#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"../src/timeevclass.hpp"
#include"../src/lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"../src/htham.hpp"
#include"../src/exapp.hpp"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include<iostream>
#ifdef ITENSOR_USE_TBB
#include "tbb/task_scheduler_init.h"		

#endif
using namespace std;

using namespace itensor;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{

#ifdef ITENSOR_USE_TBB
 tbb::task_scheduler_init init(10);
#endif

  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
std::string mpsName{};
 std::string siteSetName{};

 double cutoff{};
  double lbocutoff{};
  int M{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
   double dt{};
   double norm_change{};
  double tot{};
  double V{};
  double eps0{};
  bool theta{};
  std::string snorm_change{};
  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
  std::string seps0{};
  std::string filename="";
 bool saveNr=true;
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("Norm", boost::program_options::value(&norm_change)->default_value(0.0001), "Norm")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(200), "lboMd")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
("theta", boost::program_options::value(&theta)->default_value(0), "theta")
 ;

    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
    if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << Llead2 << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << Lchain << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
   {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 5);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 5);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 5);
      		filename+=seps0;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << V << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << tot << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 4);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << dt << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		       	     if (vm.count("Md"))
      {      std::cout << "Md: " << Md << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << lboMd << '\n';
      	slboMd="lboMd"+std::to_string(lboMd);
      	filename+=slboMd;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << cutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       		 if (vm.count("lbocut"))
      {      std::cout << "lbocutoff: " << lbocutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 scutoff="lbocut"+ss.str();
      	filename+=scutoff;
      }
		       		 if (vm.count("Norm"))
      {      std::cout << "NormChange: " << norm_change << '\n';
	 std::stringstream ss;
	 ss<<vm["Norm"].as<double>();
	 snorm_change="NC"+ss.str();
      	filename+=snorm_change;
      }
		       		 if (vm.count("theta"))
      { 

	 std::stringstream ss;
	 ss<<vm["theta"].as<bool>();
	 
	 filename+="theta"+ss.str();
      }
      
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  //	filename+=".bin";
  std::cout<<filename<<std::endl;
  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi0 = itensor::readFromFile<MPS>(mpsName);
  size_t N=length(psi0);
  std::vector<int> v(N, 0);
  // is stored as vecto so start site-1
for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
//"MinDim=",maxLinkDim(psi0)
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "CutoffLBO",lbocutoff  , "MaxDim=",Md, "Normalize",false, "SVDMethod", "ITensor");
  //    auto H2=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, gamma, omega, eps0);
  psi0.position(1);
  psi0.normalize();
  std::cout<< "stop 1"<< std::endl;
    auto H2=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, gamma, omega, eps0);
auto Curr=MPO(itensor::j(sites, Llead1,Llead2, Lchain, tint ));
  auto CurrE=MPO(itensor::jEdot(sites, Llead1,Llead2, Lchain, tl , V/2));
  auto EL=MPO(itensor::ELdot(sites, Llead1,Llead2, Lchain, tl, V/2 ));
  auto ER=MPO(itensor::ERdot(sites, Llead1,Llead2, Lchain, tl, V/2 ));
  auto EHYB=MPO(itensor::EHYBdot(sites, Llead1,Llead2, Lchain, tint ));
  auto CDWdisp=CDWDispstruct(sites, Llead1, Lchain);
  auto Nst=Nstruct(sites, Llead1, Lchain);
auto CDWO=CDWOstruct(sites, Llead1, Lchain);
 auto NPH=Ephstruct(sites, Llead1, Lchain, omega);
 auto EKIN=Ekinstruct(sites, Llead1, Lchain, t0);
 auto ESTRUCT=Estruct(sites, Llead1, Llead2, Lchain, t0, gamma, omega, eps0);
  std::map<std::string, double> param;
   std::cout<< "stop "<< std::endl;
 param.insert({"t0",t0});
 param.insert({"tl",tl});
 param.insert({"tint",tint});
 param.insert({"V",V}); // now using -V
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"eps0",eps0});
 //  param.insert({"lambda",lambda});


 

  int i=0;


  itensor::IHGdot GM(sites, gates, dt, param, Llead1,Llead2, Lchain);
  if(!theta)
    {     	
    	    GM.set_current_time(40.);
     	  std::cout<< "curr time step func "<<itensor::step_func(40.)<<std::endl;
     	  GM.makeGates();
    }
    	
  auto psiv=itensor::vMPS(psi0);
 
   itensor::TimeEvolveLbo< itensor::IHGdot, decltype(psiv), false> TE(GM,  psiv, N,  argsMPS, argsState);

   TE.makeLboStruct(Llead1, Lchain);
 size_t mps_pos=mpsName.find("MPS");
 mpsName=mpsName.substr(mps_pos);

  mpsName.insert(0, filename);

  mpsName.insert(0, "dottebdlbopara");
  std::string dirName=mpsName;
  dirName.replace(dirName.end()-4, dirName.end(), "dir");
  boost::filesystem::path dir(dirName);
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success " << std::endl;
}	     
	     else{
dir=boost::filesystem::path(dirName+"2");
 if(boost::filesystem::create_directories(dir)){
	       std::cout<< "made dir success second attempt" << std::endl;
}
 else{ std::cout<< "could not make dir error "<<std::endl;
return 0;}	     
}

   if(boost::filesystem::exists("time"+mpsName))
	     {
	     	 mpsName+="2";
	     }
   //	     	     print(psi0);
    bool set_time=true;
      while(dt*i<tot)
     {
       if(theta)
	 {
        if(set_time)
       	{
       if(std::abs(itensor::step_func(dt*i)-1.)>0.001)
       	{
       	  GM.set_current_time(dt*i);
       	  std::cout<< "curr time step func "<<itensor::step_func(dt*i)<<std::endl;
       	  GM.makeGates();
       	}

        else
       	{
       	    GM.set_current_time(40.);
       	  std::cout<< "curr time step func "<<itensor::step_func(40.)<<std::endl;
       	  GM.makeGates();
       	  set_time=false;}
       	}
     }
     	auto psir2=TE.getBareMpsStruct(Llead1, Lchain);

    	 psir2.position(Llead1+1);
       	  auto NORM=itensor::norm(psir2);
	  	  	 if(std::abs(NORM-1.)>norm_change)
	   {

	     std::cout<< "normalize!!!"<<std::endl;
	     std::cout<< "before "<<NORM<<std::endl;
	 TE.paraNormalize();
       psir2=TE.getBareMpsStruct(Llead1, Lchain);

    	 psir2.position(Llead1+1);
       	   NORM=itensor::norm(psir2);
	   std::cout<<"norm after "<< NORM<<std::endl;

}

		    		    dens_dens_correl( sites,  psir2,  Llead1, Lchain, boost::filesystem::canonical(dir).string());
   auto cdwdisp = (itensor::innerC(psir2, CDWdisp, psir2));
		    auto cdwo = (itensor::innerC(psir2, CDWO, psir2));
		    auto estruct = (itensor::innerC(psir2, ESTRUCT, psir2));
		    auto ekin = (itensor::innerC(psir2, EKIN, psir2));
		    auto Nph = (itensor::innerC(psir2, NPH, psir2));
		    auto Ne = (itensor::innerC(psir2, Nst, psir2));
		  auto c = (itensor::innerC(psir2, Curr, psir2));
		  auto el=(itensor::innerC(psir2, EL, psir2));
		  auto er=(itensor::innerC(psir2, ER, psir2));
		  auto curre= (itensor::innerC(psir2, CurrE, psir2));
		  // auto ehybe= (itensor::innerC(psir2, EHYB, psir2));
		  double ne_val=real(Ne);
		  double cdwdisp_val=real(cdwdisp);

		  double cdwo_val=real(cdwo);
		  
		  double nph_val=real(Nph);
		  double estruct_val=real(estruct);
		  double ekin_val=real(ekin);
    		    double avl2=real(c);
		     double avl3=real(curre);
		   
		    double elval=real(el);
		    double erval=real(er);
		    double max_bond=maxLinkDim(psi0);
		    double max_lbo=TE.maxLboDim();
       		    std::vector<double> v1;
       		    std::vector<double> v2;
       		    std::vector<double> v3;
       		    std::vector<double> v4;
       		    std::vector<double> v5;
       		    std::vector<double> v6;
       		    std::vector<double> v7;
       		    std::vector<double> v8;
       		    std::vector<double> v9;
       		    std::vector<double> v10;
		    std::vector<double> v11;
		    std::vector<double> v12;
		    std::vector<double> v13;
       		    v1.push_back(i*dt);
       		    v2.push_back(avl2);
       		    v3.push_back(cdwdisp_val);
       		    v4.push_back(cdwo_val);
       		    v5.push_back(estruct_val);
       		    v6.push_back(ne_val);
       		    v7.push_back(nph_val);
       		    v8.push_back(avl3);
       		    v9.push_back(erval);
       		    v10.push_back(elval);
		    v11.push_back(max_bond);
		    v12.push_back(max_lbo);
		    v13.push_back(ekin_val);
       		    auto vecSize=v1.size();
		    
       		    std::cout<< vecSize << "print "<<v1[0] <<std::endl;
       		    std::cout<<  "/time"+mpsName <<std::endl;
       		    Many_Body::ToFile(v1, boost::filesystem::canonical(dir).string()+"/time.dat",vecSize, true);
		    Many_Body::ToFile(v2, boost::filesystem::canonical(dir).string()+"/J.dat",vecSize, true);
       Many_Body::ToFile(v3, boost::filesystem::canonical(dir).string()+"/CDWdisp.dat",vecSize);
       Many_Body::ToFile(v4,boost::filesystem::canonical(dir).string()+ "/CDWO.dat",vecSize);
       Many_Body::ToFile(v5,boost::filesystem::canonical(dir).string()+ "/Estruct.dat",vecSize);
       Many_Body::ToFile(v6, boost::filesystem::canonical(dir).string()+"/Ne.dat",vecSize);
       Many_Body::ToFile(v7, boost::filesystem::canonical(dir).string()+"/Nph.dat",vecSize);
       Many_Body::ToFile(v8, boost::filesystem::canonical(dir).string()+"/Ecur.dat",vecSize);
       Many_Body::ToFile(v9, boost::filesystem::canonical(dir).string()+"/ER.dat",vecSize);
       Many_Body::ToFile(v10, boost::filesystem::canonical(dir).string()+"/EL.dat",vecSize);
       Many_Body::ToFile(v11, boost::filesystem::canonical(dir).string()+"/Bondmax.dat",vecSize);
       Many_Body::ToFile(v12, boost::filesystem::canonical(dir).string()+"/LBOmax.dat",vecSize);
       Many_Body::ToFile(v13, boost::filesystem::canonical(dir).string()+"/Ekinstruct.dat",vecSize);      

		  if(max_bond>=Md-100)
		    {

    std::cout<< "finish "<<std::endl;
return 0;
		    }
   
   		  std::cout<< std::setprecision(12)<<dt*i <<" Oder param "<<cdwo_val <<" ne "<< ne_val << " " <<c<< "  " << "  E "<< innerC(psir2, H2, psir2)<< " Order disp  "<< cdwdisp_val<<" " "lbo max "<< max_lbo<<"   "<< " mx  "<< max_bond<<" av  "<< averageLinkDim(psi0)<< "norm "<< NORM <<"\n";
		  std::cout<< "start TC"<<std::endl;
		  std::cout<< "start TC"<<std::endl;
 		  TE.lboTEBDStepO2paraStruct(Llead1, Lchain);		  

	   i++;
	       		 	  }


     return 0;
}
